const { db_port, db_user, db_password, db_name, db_server } = require('../config/environment');
const sql = require('mssql');
const logger = require('../utils/logger');
const { isEmptyObject } = require('../utils/utils');

const DB_CONFIG = { 
    user: db_user,
    password: db_password,
    server: db_server,
    database: db_name,
    port: +db_port,
    connectionTimeout: 60000,
    options: { 
        encrypt: true,
        enableArithAbort: true,
        abortTransactionOnError: true
    }
}

const DB_CONNECTION = {
    pool : '',
}

const createConnectionPool = () => { 
    return new Promise( async (resolve, reject) => { 
        if(isEmptyObject(DB_CONNECTION.pool) || !DB_CONNECTION.pool._connected) { 
            logger.debug('No existing connection.');
            logger.debug('Connecting to database...');
            const pool = new sql.ConnectionPool(DB_CONFIG, err => { 
                if(err) 
                    reject(err);

                DB_CONNECTION.pool = pool;
                resolve(pool);
            });

            pool.connect(err => { 
                if(err) { 
                    logger.error(`Failed to Create Connnection Pool!`);
                } else  { 
                    logger.info(`Database Connection established!`);
                    logger.debug(`Connection pool created successfuly!`);
                }
            });
          
        } else { 
            logger.debug('Using existing connection pool.');
            resolve(DB_CONNECTION.pool);
        }

        
    })
}

exports.createConnectionPool = createConnectionPool;

exports.getConnectionPool = () => { 
    return DB_CONNECTION.pool;
}

exports.restartConnection = async () => { 
    logger.info("Restarting Connection...")
    DB_CONNECTION.pool.close();
    DB_CONNECTION.pool = {};
    await createConnectionPool();

}

exports.getDatabaseConnection = async () => { 
    if(DB_CONNECTION.pool._connected) {
        return await DB_CONNECTION.pool.connect();
    }
    else {
        const message = `Could not create dbConnection. Possible pool connection failure.`;
        logger.error(message);
        throw new Error(message);
    }
}   

exports.getDatabaseTransaction = () => { 
    if (DB_CONNECTION.pool._connected) {
        return new sql.Transaction(DB_CONNECTION.pool);
    } else {
        const message = `Could not create transaction. Possible pool connection failure.`;
        logger.error(message);
        throw new Error(message);
    }
}