const dbHelper = require('./dbHelpers');

const { db_port, db_user, db_password, db_name, db_server } = require('../config/environment');
const logger = require('../utils/logger');


const DBCONFIG = { 
    user: db_user,
    password: db_password,
    server: db_server,
    database: db_name,
    port: +db_port,
    options: { 
        encrypt: true,
        enableArithAbort: true,
        abortTransactionOnError: true
    }
}

const DB_CON_STATE = {
    isConnected: false,
    db: null,
}

/**
 * Method to execute a procedure in the database.
 * @param {object} procedureData
 * @param {object} databaseConnection
 * @param {string} procedureName
 * @param {boolean} isTransaction
 */
exports.executeProc = async (procedureData, databaseConnection, procedureName, isTransaction = false) => {
    const {doesObjectExist} = require("../utils/utils");
    try {
        // Confirm if there is actually a connection & the data for the procedure & that we are providing a procedure name
        if (procedureData && databaseConnection && procedureName) {
            if (databaseConnection.connected) {
                const databaseRequest = await dbHelper.addParameters(procedureData, databaseConnection, procedureName); // Error is here
                if (doesObjectExist(databaseRequest)) {
                    if(!isTransaction) {
                        const result = await databaseRequest.execute(procedureName);
                        return result.recordset;
                    } else {
                        return  await databaseRequest.execute(procedureName);
                    }
                }
                throw `DatabaseRequest object its missing data: ${databaseRequest}`
            }
        } 
        throw `Data missing for the parameters\nProcedureData: ${procedureData};\nDatabaseConnection: ${databaseConnection};\nProcedureName: ${procedureName}`;
        
    } catch (error) {
        const { restartConnection } = require('./dbConnector');
        //:: TODO > MAKE THIS A DB CONNECTION ERROR
        logger.error(`Could not execute procedure : ${error.message}`);
        logger.error(`Values of: 
        - ProcedureData: ${JSON.stringify(procedureData)};
        - ProcedureName: ${procedureName}`)
        logger.error(`Object with error at executeProc:  ${JSON.stringify(error.stack)}`);
        const { writeToErrorLog } = require('../utils/utils');
        DB_CON_STATE.isConnected = false;
        writeToErrorLog(error, 'dbAccess', 'execProc');
        await restartConnection();
        throw error;
    }
}

exports.execProcedureInTransaction = async (procedureName, procedureData, transaction) => {
    const dbReq = await dbHelper.createDatabaseRequestForProcedure(procedureName, procedureData, transaction);
    return await dbReq.execute(procedureName);
}


exports.db_config = DBCONFIG;
exports.dbConState = DB_CON_STATE;
