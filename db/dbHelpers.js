const sql = require('mssql');
const logger = require('../utils/logger');

/**
 * Returns the datatype of sql by specifing a datatype in string format
 * @param {string} strDataType 
 */
const getMssqlDataType = (strDataType) => {
    const dType = strDataType.toLowerCase();

    switch(dType) { 
        case 'varchar': return sql.VarChar;break;
        case 'int': return sql.Int; break;
        case 'text': return sql.Text; break;
        case 'string': return sql.VarChar; break;
        case 'bit': return sql.Bit; break;
        case 'datetimeoffset': return sql.DateTimeOffset; break;
        case 'datetime': return sql.DateTime; break;
        case 'time': return sql.Time; break;
        default: return sql.VarChar;
    }
}

/**
 * helperFunction to build de object that will hold the sql parameter info ( dataType, key, value)
 * @param {object} inputData 
 * @param {string} procedureName 
 * @param {object} databaseConnection 
 */
const buildSqlParamObject = async (inputData, procedureName,  databaseConnection) => {
    const {doesArrayExist, getProcParamsBackupSrcipt } = require('../utils/utils');
    const { restartConnection } = require('./dbConnector');
    const databasRequest = databaseConnection.request();
    let params = [];
    // SOMETHING HERE MANY NOT BE OK
    try {
        databasRequest.input('p_proc_name', sql.VarChar, procedureName);
        let procedureParamenters = await databasRequest.execute('proc_get_proc_params');
        if (!doesArrayExist(procedureParamenters.recordset)) {
            procedureParamenters = await databaseConnection.query(getProcParamsBackupSrcipt(procedureName));
        }
        const procedureParamentersSqlTypeMap = matchParameterWithSqlType(procedureParamenters.recordset);
        Object.keys(inputData).forEach(key => {
            params = [...params, {key, dataType: procedureParamentersSqlTypeMap[key], value: inputData[key]}]
        })
        return params;
        
    } catch (error) {
        logger.error(`Could not get procedure parameters ${error.message}`);
        logger.error(`Object with error at dbHelper  ${JSON.stringify(error)}`);
        const {writeToErrorLog} = require('../utils/utils');
        writeToErrorLog(error, 'dbHelpers.js', 'buildSqlParamObject');
        await restartConnection();
        return {};
    }
    
}

/**
 *  Maps the parameter name with its sql data type.
 * @param {object[]} arrayOfParamenterObject 
 */
const matchParameterWithSqlType = (arrayOfParamenterObject) => {
    const parameterNameAndSqlTypeMap = {};
    let parameterObjectKeys, newParameterName, parameterName, parameterType;
    arrayOfParamenterObject.forEach(parameterObject => {
        parameterObjectKeys = Object.keys(parameterObject);
        parameterName = parameterObjectKeys[0];
        parameterType = parameterObjectKeys[1];
        newParameterName = parameterObject[parameterName].replace('@', '');
        parameterNameAndSqlTypeMap[newParameterName] = parameterObject[parameterType];
    })
    return parameterNameAndSqlTypeMap;
}

/**
 * Helper function to add parameters to a databaseRequest of mssql
 * @param {object} parameters 
 * @param {object} databaseConnection 
 * @param {string} procedureName 
 */
exports.addParameters = async (parameters, databaseConnection, procedureName) => {
    
    try {
        let parameterName, value, msssqlDataType;
        const databaseRequest = databaseConnection.request();
        const paramsObject = await buildSqlParamObject(parameters, procedureName, databaseConnection);
        paramsObject.forEach( parameterObject => {
            if(parameterObject.dataType === undefined) return;
            parameterName = parameterObject.key;
            value = (parameterObject.value || parameterObject.value === 0)? parameterObject.value : undefined;
            msssqlDataType = getMssqlDataType(parameterObject.dataType);
            databaseRequest.input(parameterName, msssqlDataType, value);
        });
        return databaseRequest;

    } catch (error) {
        logger.error(`Could not add parameters to query ${error.message}`);
        logger.error(`Object with error at dbHelper  ${JSON.stringify(error)}`);
        const {writeToErrorLog} = require('../utils/utils');
        writeToErrorLog(error, 'dbHelpers.js', 'addParameters');
        return {};
    }
   
}

//::>> New Methods For Transaction Processing
const getTransaction = () => { 
    const { getDatabaseTransaction } = require('./dbConnector');
    return getDatabaseTransaction();
}

exports.getAndBeginTransaction = async () => {
    const transaction = getTransaction();
    await transaction.begin();
    return transaction;
}

exports.createDatabaseRequestForProcedure = async (procedureName, procedureParameters, transaction) => { 
    let parameterName, value, msssqlDataType;
    const databaseRequest = this.getRequestFromTransaction(transaction);
    const paramsObject = await buildProcedureParameterObject(procedureName, procedureParameters, transaction);
    paramsObject.forEach( parameterObject => {
        if(parameterObject.dataType === undefined) return;
        parameterName = parameterObject.key;
        value = (parameterObject.value || parameterObject.value === 0)? parameterObject.value : undefined;
        msssqlDataType = getMssqlDataType(parameterObject.dataType);
        databaseRequest.input(parameterName, msssqlDataType, value);
    });
    return databaseRequest;
}

exports.getRequestFromTransaction = (transaction) => {
    return new sql.Request(transaction);
}

const buildProcedureParameterObject = async (procedureName, procedureParameters,  transaction) => {
    let params = [];
    const databasRequest = this.getRequestFromTransaction(transaction);
    databasRequest.input('p_proc_name', sql.VarChar, procedureName);
    let procedureParamenters = await databasRequest.execute('proc_get_proc_params');
    const procedureParamentersSqlTypeMap = matchParameterWithSqlType(procedureParamenters.recordset);
   
    Object.keys(procedureParameters).forEach(key => { 
        params = [...params, {key, dataType: procedureParamentersSqlTypeMap[key], value: procedureParameters[key]}]
    })

    return params;
}

exports.getDbObjectIdFromResult = (result) => { 
    const isEmptyValue = (value) => { 
        return (value === '') || (value === undefined) || (value === null);
      }
    // const {isEmptyValue} = require('../utils/utils');
    let objectId = result.recordset[0].svisit;
    if(isEmptyValue(objectId))
        objectId = result.recordset[0].int_id;
    return objectId;
}