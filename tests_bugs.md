# TESTS
### User Creation (Fail Cases)
- [x] Cannot create user with existing email
- [x] Cannot create user with existing phone-number-1
- [x] Cannot create user without token
- [x] Can add users with same phone_number_02