const { validationResult } = require("express-validator");
const { sendResponseError } = require("../utils/controllerHelper");

/**
 * Validadtes the request fields. 
 * @param {object} req 
 * @param {object} res 
 * @param {*} next 
 */
exports.validateRequestFields = async (req, res, next) => {
    try{ 
        const valRes = validationResult(req);	
		if (!valRes.isEmpty()) {
			return res.status(400).json({ errors: valRes.errors });
        }
        next();
    } catch (err){
        sendResponseError(res, err, 'globalMidddleware.js', 'validateRequestFieldss');
    }
}