const sql = require('mssql');
const logger = require('../utils/logger');
const { getDatabaseConnection, getConnectionPool, createConnectionPool, getDatabaseTransaction } = require('../db/dbConnector');

const retryConnection = async () => { 
    let retrySuccess = true;
    try {
        const pool = getConnectionPool();
        pool.close();
        await createConnectionPool();

        const secondConnection = getConnectionPool();
        retrySuccess = secondConnection._connected;
        logger.info(`Connection Retry Successful.`);
    } catch (error) {
        logger.error(`Connection retry failed : ${error.message}`);
        retrySuccess = false;
    }
    return retrySuccess;
}

/**
 * Checks if a connection to the database is established for any route that is necessary to have a connection.
 * @param {object} req 
 * @param {object} res 
 * @param {*} next 
 */
module.exports = async (req, res, next) => {
    try { 
        logger.debug(`URL: ${req.originalUrl}`);
        req.db = await getDatabaseConnection();
        return next();        
    } catch (err){
        logger.error('Could pass Database connection to request. Retrying connection...');
        const retriedSuccessfully = await retryConnection();
        if (retriedSuccessfully) {
            req.db = await getDatabaseConnection(); 
            return next();
        }
        logger.error(`${err.message} file@dbCon.js`);
        res.status(401).json({root_msg: err, error: 'Could not connect to database!'});
    }
}
