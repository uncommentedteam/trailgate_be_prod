const multer = require("multer")
const path = require('path');

const MIME_TYPE_MAP = {
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/jpg': 'jpg'
}
// Where will store the file
const storage = multer.diskStorage({
    // When multer trys to save a file
    destination: (req, file, cb) => {
        // Check if file is valid object
        const isValid = MIME_TYPE_MAP[file.mimetype]
        // Check for errors
        let error = new Error("Invalid file object")
        if (isValid){
            error = null
        }
        // Send to api
        const pathx = String(path.join('res','userDocumentPhotos'))
        cb(error, pathx) 
    },
    filename: (req, file, cb) => {
        const name = file.originalname
        const ext = MIME_TYPE_MAP[file.mimetype]
        cb(null, name+'.'+ext)
    }
})

module.exports = multer({storage:storage}).array("img",2)//.single("img") //Single is the param name