const randomString = require("randomstring");
const config = require("config");
const { extractFieldsFromObject, getLastIdOfTable, getToday, writeToErrorLog, doesVariableExist, doesArrayExist } = require("../utils/utils");
const { getDatabaseFieldsOfAllObjects, getColaboratorById, isUserThread, isUserCoreSec} = require("../utils/controllerHelper");
const BaseClass = require('./BaseClass');
const crypto = require('crypto');
const Visitor = require("./Visitor");
const User = require("./User");

class Visit extends BaseClass { 
    constructor(requestData, databaseConnection) {
        super(requestData, databaseConnection);
        this.createVisitAndVisitExtraObject();
    }

    /**
     * Creates an object only for the visit data and another for extras (materials and vehicles linked to the visit) from the request data.
     * Also keeps the this.visitExtras an array regardless of the number of extras.
     */
    createVisitAndVisitExtraObject() { 
        this.visit = extractFieldsFromObject(this.requestData, getDatabaseFieldsOfAllObjects().request);
        if(doesArrayExist(this.requestData.extraArray)) { 
            this.visitExtras = this.requestData.extraArray.map(extra => { 
                return {...extractFieldsFromObject(extra, getDatabaseFieldsOfAllObjects().requestExtra), 
                    p_is_active: 1, 
                    p_dtm: getToday().toISOString()
                }
            });
        } else { 
            this.visitExtras = [{...extractFieldsFromObject(this.requestData, getDatabaseFieldsOfAllObjects().requestExtra),
                p_is_active: 1, 
                p_dtm: getToday().toISOString() 
            }]
        }

        this.visitExtras = this.visitExtras.filter(vis => vis.p_oname);
    }

    /**
     * Creates the visit access code. This has a backup Method in case the first generation of the code fails.
     */
    _createVisitAccessCode() { 
        this.visit.p_code = randomString.generate(config.get("others.visitCodeSize"));
        this.visit.p_code = this.visit.p_code?  this.visit.p_code : crypto.createHash('md5').update(this.visit.p_date_time).digest('hex').substring(0, 7);
    }

    /**
     * Relatest the visit extras to the visit id and to the user id. (Database Id's)
     * @param {number} visitId 
     * @param {number} userId 
     */
    relateVisitExtrasToVisitAndUser (visitId, userId) { 
        this.visitExtras.forEach( visitExtra => { 
            visitExtra.p_svisit = visitId;
            visitExtra.p_cod_user = userId || visitExtra.p_cod_user;
        });
    }

    /**
     * Adds the visit data and the initial visit state to the database. #TODO : TO REMOVE
     */
    async saveVisitAndVisitState() { 
        this._createVisitAccessCode();
        let [isSuccessful, message] = await super.save('proc_add_svisit', this.visit);
        if(!isSuccessful) return [false, message] 

        const visitId = await getLastIdOfTable(this.databaseConnection, 'tbl_svisit');
        [isSuccessful, message] = await super.save('proc_add_ivstate', {p_id_svisit: visitId});
        if(!isSuccessful) { return [false, message]}

        return [true, visitId];
    }

    /**
     * Saves the visit extras (Materials and Vehicles data of the visit) to the database.
     */
    async saveVisitExtras() { 
        this.visitExtras.forEach(async visit => {
            try {
                await super.save('proc_add_svextra', visit);
            } catch (error) {
                
                writeToErrorLog(error, 'Visit.js', 'saveVisitExtras')
            }
        });
        return [true, 'Success']
    }

    /**
     * Relates the visit to the visitors.
     * @param {number} visitId 
     */
    async _realteUsersToVisit(visitId) { 
        if(!doesVariableExist(visitId)) return [false, 'Could not relate visit to visitors. Visit failed to register.']
        if (doesArrayExist(this.requestData.p_users)) {
            this.requestData.p_users.forEach(async user => { 
                if(!(user.missing || user.failed)) { 
                    const userObject = new User(user, this.databaseConnection);
                    let [_, __, userId] = await userObject.save();
                    await this._addVisitorEntry(userId, visitId);
                }
            });
            if(!this.requestData.p_internal) {
                this._addVisitorEntry(this.requestData.p_id_x, visitId);
            }
        } else { 
            return this._addVisitorEntry(this.requestData.p_id_x, visitId);
        }
        return [true, 'Success'];
    }

       /**
     * Adds the visitor entry to the database.
     * @param {number} userId 
     * @param {number} visitId 
     */
    async _addVisitorEntry(userId, visitId) { 
        const visitor = new Visitor({p_id_svisit: visitId, p_id_x: userId}, this.databaseConnection);
        const [isSuccessful, message]  = await visitor.save();
        if(this.visit.p_id_host) this.changeVisitStateByDestinationUser(this.visit.p_id_host, visitId);
        return [isSuccessful, message];
    }

    /**
     * Changes the state of aprovals of the visits by the destination user. 
     * @param {number} p_id_host 
     * @param {number} visitId 
     */
    async changeVisitStateByDestinationUser(p_id_host, visitId) { 
        if(!doesVariableExist(p_id_host)) return;
        const user = await getColaboratorById(this.databaseConnection, p_id_host);
        let stateUpdate = { 
            p_int: 0,
            p_id_svisit: visitId,
            p_id_htc: p_id_host,
            p_bool_htc: 1,
            p_dtm_htc: getToday().toISOString()
        }

        if(isUserThread(user)) { 
            await super.save('proc_edit_vstate_state_htc', stateUpdate);
        } else if (isUserCoreSec(user)){
            await super.save('proc_edit_vstate_state_htc', stateUpdate);
            await super.save('proc_edit_vstate_state_htc', {...stateUpdate, p_int: 1});
        }
    }

    /**
     * This method is ment to avoid the use of the save method of this class. Child classes can't use super.save() of this class, but will be able to use super.save() of the BaseClass
     * @param {string} procedureName 
     * @param {object} data 
     */
    async saveDataToDatabase (procedureName, data) { 
        await super.save(procedureName, data);
    }

    /**
     * Saves the Visit/Request to the database.
     */
    async save()  {
        const [isSuccessful, visitId] = await this.saveVisitAndVisitState('proc_add_svisit', this.visit);
        if(!isSuccessful) return [false, 'Visit Could not be Saved!'];

        this.relateVisitExtrasToVisitAndUser(visitId);

        const [isSuccessful2, _] = await this.saveVisitExtras();
        if(!isSuccessful2) return [false, 'Visit Extras could not be Saved!'];

        return [true, 'Success', visitId];
    }


}

module.exports = Visit 