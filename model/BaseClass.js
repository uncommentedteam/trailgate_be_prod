const { executeProc } = require("../db/dbAccess");
const { writeToErrorLog, getErrorMessageByErrorId } = require("../utils/utils");

class BaseClass {
    constructor(requestData, databaseConnection) { 
        this.requestData = requestData;
        this.databaseConnection = databaseConnection;
    }

    set (key, val) { this[key] = val; }

    get (key) { return this[key] || null }

    /**
     * Saves the data to the database executing a procedure specified in the procedureName
     * @param {string} procedureName 
     * @param {object} data 
     */
    async save(procedureName, data) { 
        try {
            await executeProc(data, this.databaseConnection, procedureName);
        } catch (error) {
            writeToErrorLog(error, 'Request.js', 'save');
            throw error;
            // throw {message: getErrorMessageByErrorId(error.number)};
        }
        return [true, "Success"];
    }
}

module.exports = BaseClass; 