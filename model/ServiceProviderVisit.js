const Visit = require("./Visit");
const { doesVariableExist } = require("../utils/utils");

class ServiceProviderVisit extends Visit {
    constructor(requestData, databaseConnection) { 
        super(requestData, databaseConnection);
    }

    /**
     * Saves the visit to the database. It realtes the visit to the participants aswell.
     */
    async save() { 

        try {

            this._createVisitAccessCode();

            let [isSuccessful, message, visitId] = await super.save();
            if(!isSuccessful)return [false, message];

            [isSuccessful, message] = await this._realteUsersToVisit(visitId);
            if(!isSuccessful) return [false, message];

            if(doesVariableExist(this.visit.p_id_host)) { 
                await this.changeVisitStateByDestinationUser(this.visit.p_id_host, visitId);
            }
                                              
        } catch (error) {
            writeToErrorLog(error, 'ServiceProviderVisit.js', 'save');
            return [false, "Could Not Save Request. Try again Later!"];
        }
       

        return [true, 'Success'];
    }

}

module.exports = ServiceProviderVisit;