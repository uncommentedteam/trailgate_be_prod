const { extractFieldsFromObject, doesObjectExist } = require("../utils/utils");
const { getDatabaseFieldsOfAllObjects, getUserbyDocumentId, getVisitorById, getUserByPhoneNumber1 } = require("../utils/controllerHelper");
const Visit = require('./Visit');
const User = require("./User");

class FastVisit extends Visit { 
    constructor(visData, db) { 
        super(visData, db);
        this.createUserOfVisitObject();
    }

    /**
     * Creates an object of the user data only and another for the user details only from the request data.
     */
    createUserOfVisitObject() { 
        const user = extractFieldsFromObject(this.requestData.p_user, getDatabaseFieldsOfAllObjects().user);
        const uDetails = extractFieldsFromObject(this.requestData.p_user, getDatabaseFieldsOfAllObjects().userDetails);

        this.user = new User(null, this.databaseConnection);
        this.user.set('user', user);
        this.user.set('userDetails', uDetails);
        this.user.set('requestData', {...user, ...uDetails});
    }

    /**
     * Verifies if the user can create a fast request. Only temporary visitors (visitors which do not have an account) can create fast requests.
     * @param {object} user 
     */
    async _isUserAllowedToCreateVisit(user) { 
        if(!doesObjectExist(user)) return false;
        if (user.isvisitor) {
            const moreCompleteUser = await getVisitorById(this.databaseConnection, user.cod);
            return moreCompleteUser.istemp? true:false;
        } else { 
            return false;
        }
    }

    /**
     * Verifies if the user exists in the database. And if it does, it returns its ID.
     */
    async _verifyUserExistence() { 
        let userId;
        let errorMessage = "User has been registered! Please Login to your account to make a request.";
        let user = await getUserbyDocumentId(this.databaseConnection, this.user.user.p_nid);
        user = user? user : await getUserByPhoneNumber1(this.databaseConnection, this.user.user.p_pnumber_1);
        if(doesObjectExist(user)) {
            const canCreateVisit = await this._isUserAllowedToCreateVisit(user);
            if(canCreateVisit) {
                userId = user.cod
             } else { 
                return [false, errorMessage, -1];
             }
        } else { 
            return [false, errorMessage, -1]
        }

        return [true, 'Success', userId]
    }

v
    /**
     * Saves the User and the Visit.
     */
    async save() { 
        let [isSuccessful, message] = await this.user.save();
        let userId, visitId;

        if(!isSuccessful) [isSuccessful, message, userId] = await this._verifyUserExistence();
        if(!isSuccessful) return [isSuccessful, message];
        
        super.createVisitAndVisitExtraObject();
        [isSuccessful, visitId] = await this.saveVisitAndVisitState('proc_add_svisit', this.visit);
        if(!isSuccessful) { return [false, 'Could not create Fast Request']};

        userId = userId? userId : await this.user.getUserId();
        super.relateVisitExtrasToVisitAndUser(visitId, userId);
        [isSuccessful, message] = await super.saveVisitExtras();
        if(!isSuccessful) { return [false, 'CouLd not create Fast Request']};

        [isSuccessful, message] = await this._addVisitorEntry(userId, visitId);
        if(!isSuccessful) { return [false, 'CouLd not create Fast Request']};

        return [true, 'Success'];

    }
}


module.exports = FastVisit;