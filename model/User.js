const { getDatabaseFieldsOfAllObjects, getUserbyDocumentId, getVisitorById } = require("../utils/controllerHelper");
const { getMomentFromString, extractFieldsFromObject, getLastIdOfTable, writeToErrorLog, removePropertyFromObject, doesObjectExist, doesArrayExist, isEmptyValue } = require("../utils/utils");
const { executeProc } = require("../db/dbAccess");
const BaseClass = require('./BaseClass')

const randomString = require('randomstring'); 
const { USER } = require("../utils/constants");
const { convertDatabaseFieldstoProcedureFields } = require("../utils/responseHandlers");
const Notification = require("../utils/Notification");

class User extends BaseClass {
    constructor(requestData, databaseConnection) {
        super(requestData, databaseConnection);
        if (doesObjectExist(requestData)) {this._createUserAndUserDetailsFields();}

    }

    /**
     * Converts some fields of the user object to fit to the procedure fields.
     * @param {object} user 
     * @param {object} databaseConnection 
     */
    convertToUserFromId(userObject) {
        if(!doesObjectExist(user) || !doesArrayExist(databaseConnection)) return [false, 'Could Create or Update User!']
        const convertedUser = convertDatabaseFieldstoProcedureFields(userObject, USER);
        Object.keys(convertedUser).forEach(key => this[key] = convertedUser[key]);
        this.requestData = convertedUser;
        this.databaseConnection = databaseConnection;
        this._createUserAndUserDetailsFields();
        this.user.p_expiration_date_id = getMomentFromString(this.user.p_expiration_date_id).toISOString();

    }

    /**
     * Creates the user object containing only the user necessary fields and a userDetails object containing only the user details fields.
     */
    _createUserAndUserDetailsFields() {
        this.user = extractFieldsFromObject(this.requestData, getDatabaseFieldsOfAllObjects().user);
        this.userDetails = extractFieldsFromObject(this.requestData, getDatabaseFieldsOfAllObjects().userDetails);
    }

    /**
     *  Registers a new provenance to the database and returns its id.
     * @param {string} newProvenace 
     */
    async _registerNewProvenace(newProvenace) { 
        await executeProc({ p_id_etype: "PVC", p_name: newProvenace, p_active: 1, }, this.databaseConnection, "proc_add_extra");
        const provenaceId = await getLastIdOfTable(this.databaseConnection, 'tbl_extra');
        return provenaceId;
    }

    /**
     * Check if the user has a provenace. If the provenace exists but it is not registered in the database it proceeds to its registration.
     */
    async _checkUserProvenace() {
        let pid; //::>> Provenance Database ID.
        const provenanceIdFromReqData = this.userDetails.p_id_provenance;
        const newProvenace = this.requestData.provenance;
        let existingProvenace;

        if (!provenanceIdFromReqData && !newProvenace) {
            return [true, 'Success', provenanceIdFromReqData];
        } else if (provenanceIdFromReqData) {
            return [true, 'Success', provenanceIdFromReqData];
        }

        try {

            existingProvenace = await this.databaseConnection.query(`SELECT * FROM tbl_extra WHERE str_name LIKE '${newProvenace}'`);
            pid = existingProvenace.recordset[0]? existingProvenace.recordset[0].int_id : false;
            if (pid) { return [true, 'Succcess', pid] }

            //::>> NEW PROVENANCE TO BE REGITERED
            pid = await this._registerNewProvenace(newProvenace);

        } catch (error) {
            writeToErrorLog(error, 'User.js', '_checkUserProvenance');
            return [false, error.message, undefined];
        }

        return [true, 'Success', pid];
    }

    /**
     * Generates the user token and a random password if it doesn't have one.
     */
    _generateTokenAndPassword() {
        this.userDetails.p_tokken = this.userDetails.p_tokken || randomString.generate();
        this.user.p_psw = this.user.p_psw || randomString.generate({length: 8});
    }

    /**
     * Gets and Returns the id of the user
     */
    async getUserId() {
        const user = await getUserbyDocumentId(this.databaseConnection, this.user.p_nid);
        if (doesObjectExist(user)) return user.cod;
        return -1;
    }

    /**
     * Generates a new token for the user object.
     */
    changeToken() {
        this.userDetails.p_tokken = randomString.generate();
    }

    /**
     * Updates the user data.
     */
    async updateUser() {

        let isSuccessful, message;
        [isSuccessful, message] = await super.save('proc_edit_user', {...this.user, p_id: this.requestData.p_id });
        if (!isSuccessful) { return [false, message] };

        this.changeToken();
        let userDetails = {...this.userDetails, int_id_user: this.requestData.p_id };
        userDetails = removePropertyFromObject(userDetails, 'p_id_user');
        [isSuccessful, message] = await super.save('proc_edit_user_detail', userDetails);
        if (!isSuccessful) { return [false, message] };

        return [true, 'Success'];
    }

    /**
     * Activates the User and Changes his token.
     */
    async activateChangeToken() {
        this.user.p_active = 1;
        this.changeToken();
        return await this.updateUser();
    }

    /**
     * Saves the user data to the database.
     */
    async _registerUser() { 
        try { 
            return await super.save('proc_add_user', this.user);
        } catch (error) { 
            return [false, error.message];
        }
       
    }

    /**
     * Saves the only the user details to the database
     */
    async _registerUserDetails() { 
        let dbUser = await getUserbyDocumentId(this.databaseConnection, this.user.p_nid);
        let userId = dbUser.cod;
        this.user.p_id = userId;
        const [isSuccessful, _] = await super.save('proc_add_user_detail', {...this.userDetails, p_id_user: userId });
        if (!isSuccessful) return [false, "User Registration Failed. Please Contact Support!"];
        return [true, 'success']
    }

    /**
     * Activates a temporary visitor and updates its data.
     * @param {object} userData 
     */
    async _activateTemporaryVisitor(userData) { 

        
        if(!doesObjectExist(userData)) return [false, 'Could not save user!']
        this.requestData.p_id = userData.cod;
        this.user.p_id = userData.cod
        let dbUserData = await getVisitorById(this.databaseConnection, userData.cod);

        if(isEmptyValue(dbUserData)) { 
            dbUserData = {...this.user, ...this.userDetails};
            dbUserData.istemp = dbUserData.p_istemp;
        }

        if(dbUserData.istemp) { 
            this.userDetails.p_istemp = 0
            const [isSuccessful, message] = await this.updateUser();
            if(!isSuccessful) return [false, message, userData.cod];
            return [true, 'Success', userData.cod]
        } else { 
            return [false, 'User Is Already Registered!', userData.cod];
        }
    }

    
    /**
     * Verifies if the user exists. If so it may activate the visitor (in case its a visitor) if its a temporary visitor.
     */
    async _verifyUserExistence() { 
        let user = await getUserbyDocumentId(this.databaseConnection, this.user.p_nid);
        if (!user) {
            return [false, "User may already exists. Please verify the data entered or contact support."];
        } else if (user.isvisitor) { 
            return await this._activateTemporaryVisitor(user);
        } else { 
            return [false, 'User Is Already Registered!', user.cod];
        }
    }

    /**
     * Saves the user into the database
     */
    async save() {
        try {

            this._generateTokenAndPassword();
            let isSuccessful, message, pid;
       
            [isSuccessful, message, pid] = await this._checkUserProvenace();
            this.userDetails.p_id_provenance = pid;

            [isSuccessful, message] = await this._registerUser();
            if (!isSuccessful) {
                const result = await this._verifyUserExistence();
                return result; 
            }

            // [isSuccessful, message] = await super.save('proc_add_user_detail', {...this.userDetails, p_id_user: userId });
            // if (!isSuccessful) return [false, "User Registration Failed. Please Contact Support!"];
            [isSuccessful, message] = await this._registerUserDetails();
            if(!isSuccessful) return [false, message];

            if(this.user.p_is_toEmail) { 
                const nt = new Notification();
                this.user.p_tokken = this.userDetails.p_tokken;
                await nt.sendVisitorAccountCredentialsEmail(this.user);
            }

            return [true, 'Success', this.user.p_id];

        } catch (error) {
            writeToErrorLog(error, "User.js", "save");
            return [false, error.message]
        }

    }
}

module.exports = User;