const { extractFieldsFromObject, getToday, getLastIdOfTable, writeToErrorLog } = require("../utils/utils");
const { getDatabaseFieldsOfAllObjects, getUserbyDocumentId } = require("../utils/controllerHelper");
const BaseClass = require("./BaseClass");
const randomString = require('randomstring');

class CompanyUser extends BaseClass { 
    constructor(requestData, databaseConnection) { 
        super(requestData, databaseConnection);
        this._createUserAndUserDetailsFields();
    }

    /**
     * Extracts only the company fields form the request data that recieves
     */
    _collectOnlyCompanyFields() { 
        this.company = extractFieldsFromObject(this.requestData, getDatabaseFieldsOfAllObjects().company);
    }

    /**
     * Creates the user object with the user fields extracted from the request data. The same happens to userDetails object
     */
    _createUserAndUserDetailsFields() { 
        this.user = extractFieldsFromObject(this.requestData, getDatabaseFieldsOfAllObjects().user);
        this.userDetails = extractFieldsFromObject(this.requestData, getDatabaseFieldsOfAllObjects().userDetails);
    }

    /**
     * Auxiliar method for getting the user Identification Number.
     */
    async _getUserId () { 
        const user = await getUserbyDocumentId(this.databaseConnection, this.user.p_nid);
        if(user) return user.cod;
        return -1;
    }

    /**
     * Saves the company or entity user data to the database. (Only its personal information).
     */
    async _saveUserOfCompanyData() { 
        await super.save('proc_add_user', this.user);
    }

    /**
     * Saves the user details data. (Only its details data)
     */
    async _saveUserOfCompanyDetailsData() { 
        const cid = await getLastIdOfTable(this.databaseConnection, 'tbl_company');
        this.userDetails.p_id_entity = cid;
        this.userDetails.p_id_user = this.company.p_id_cuser;
        this.userDetails.p_tokken = randomString.generate();
        await super.save('proc_add_user_detail', this.userDetails);
    }

    /**
     * Saves the data related to the company only.
     */
    async _saveCompanyData() { 
        const userId = await this._getUserId();
        this._collectOnlyCompanyFields();
        this.company.p_id_cuser = userId;
        this.company.p_dmt = getToday().toISOString();
        this.company.p_cod_core = this.requestData.p_cod_core;
        await super.save('proc_add_company', this.company);
    }

    /**
     * Adds the user and the company related to the user to the database.
     */
    async save() { 
        try {

            await this._saveUserOfCompanyData();
            await this._saveCompanyData();
            await this._saveUserOfCompanyDetailsData();

        } catch (error) {
            writeToErrorLog(error, 'CompanyUser.js', 'save')
            throw error;
        }
    }
}

module.exports = CompanyUser;