const Visit = require("./Visit");
const { doesVariableExist, writeToErrorLog } = require("../utils/utils");
const { executeProc } = require("../db/dbAccess");

class VisitorVisit extends Visit { 
    constructor(requestData, databaseConnection) { 
        super(requestData, databaseConnection);
    }

    /**
     * Saves the visit to the database. It realtes the visit to the participants aswell.
     */
    async save() { 

        try {

            this._createVisitAccessCode()

            let result = await executeProc(this.visit, this.databaseConnection, 'proc_add_svisit_visitors');
            result = result[0];
            if(result.status === 0) { 
                throw new Error('Failed To Create Request. Missing Data or Invalid Data Supplied')
            }

            const visitId = result.svisit;
            this.relateVisitExtrasToVisitAndUser(visitId);
            const [isSuccessful2, _] = await this.saveVisitExtras();

         
            return await this._realteUsersToVisit(visitId);

                                     
        } catch (error) {
            writeToErrorLog(error, 'VisitorVisit.js', 'save');
            return [false, "Could Not Save Request. Try again Later!"];
        }
       

        return [true, 'Success'];
    }

}

module.exports = VisitorVisit;