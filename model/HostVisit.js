const { getColaboratorById, isUserThread, isUserCoreSec } = require("../utils/controllerHelper");
const Visit = require("./Visit");
const { doesVariableExist, writeToErrorLog } = require("../utils/utils");
const { executeProc } = require("../db/dbAccess");

class HostVisit extends Visit { 
    constructor(requestData, databaseConnection) { 
        super(requestData, databaseConnection);
    }

    /**
     * Changes the state of aprovals of the visits by the destination user. 
     * @param {number} p_id_host 
     * @param {number} visitId 
     */
    async changeVisitStateByDestinationUser(p_id_host, visitId) {

        const user = await getColaboratorById(this.databaseConnection, p_id_host);
        await super.saveDataToDatabase('proc_edit_vstate_state_htc', {p_int: 0, p_dtm_htc: this.visit.p_date_time, p_id_svisit: visitId, p_id_htc: p_id_host, p_bool_htc: 1 });
        if (isUserThread(user)) {
            await super.saveDataToDatabase('proc_edit_vstate_state_htc', {p_int: 1, p_dtm_htc: this.visit.p_date_time, p_id_svisit: visitId, p_id_htc: p_id_host, p_bool_htc: 1 });
        } else if (isUserCoreSec(user)) {
            await super.saveDataToDatabase('proc_edit_vstate_state_htc', {p_int: 1, p_dtm_htc: this.visit.p_date_time, p_id_svisit: visitId, p_id_htc: p_id_host, p_bool_htc: 1 });
            await super.saveDataToDatabase('proc_edit_vstate_state_htc', {p_int: 2, p_dtm_htc: this.visit.p_date_time, p_id_svisit: visitId, p_id_htc: p_id_host, p_bool_htc: 1 });
        }

    }

    

    /**
     * Saves the visit to the database. It realtes the visit to the participants aswell.
     */
    async save() { 
        try {

            this._createVisitAccessCode();

            let result = await executeProc(this.visit, this.databaseConnection, 'proc_add_svisit_hosts');
            result = result[0];
            if(result.status === 0) { 
                throw new Error('Failed To Create Request. Missing Data or Invalid Data Supplied')
            }

            const visitId = result.svisit;
            this.relateVisitExtrasToVisitAndUser(visitId);
            const [isSuccessful2, _] = await this.saveVisitExtras();

         
            return await this._realteUsersToVisit(visitId);

        } catch (error) {
            writeToErrorLog(error, 'HostVisit.js', 'save');
            return [false, "Could Not Save Request. Please Try Again Later!"];
        }
       
        return [true, 'Success'];
    }
}

module.exports = HostVisit;