const BaseClass = require('./BaseClass');
const { extractFieldsFromObject, getToday } = require('../utils/utils');
const { getDatabaseFieldsOfAllObjects, getColaboratorById, isUserCoreSec, isUserThread } = require('../utils/controllerHelper')
const moment = require('moment');

class Visitor extends BaseClass { 
    constructor(visitor, database) {
        super(visitor, database);
        this._createVisitorObject();
    }

    /**
     * Creates And Object Of the visitor and its default states.
     */
    _createVisitorObject() { 
        this.visitor = extractFieldsFromObject(this.requestData, getDatabaseFieldsOfAllObjects().visitor);
        this.visitor.p_is_active = 1;
        this.visitor.p_induction = 1;
    }

    /**
     * Saves the visitor and a visit entry in the database relate to the visitor.
     */
    async save () { 
        let [isSuccessful, message] = await super.save('proc_add_visitor', this.visitor);
        if(!isSuccessful) return [false, 'Could not Create Visit. Visitor creation failed!'];

        [isSuccessful, message] = await super.save('proc_add_visit', {p_id_x: this.visitor.p_id_x, p_id_svisit: this.visitor.p_id_svisit});
        if(!isSuccessful) return [false, 'Could not save Visit. Visit creation failed!'];

        return [isSuccessful, 'Success'];
    }
}

module.exports = Visitor;