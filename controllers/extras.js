const { executeProc } = require("../db/dbAccess");
const { convertDatabaseFieldstoProcedureFields } = require("../utils/responseHandlers");
const { removePropertyFromObject, doesArrayExist} = require("../utils/utils");
const { COMPANIES, EXTRAS, USER_OF_TERMINAL, INDUCTION_TYPE } = require("../utils/constants");
const { sendResponseError } = require("../utils/controllerHelper");

/**
 * Controller to add alert messages.
 * @param {object} req 
 * @param {object} res 
 * @deprecated
 */
exports.addAlertMsg = async (req, res) => { 
    try {
        await executeProc(req.body, req.db, 'proc_add_alert_msg');
        res.json({msg: "Alert Message Added Successfully"});
    } catch (error) {
        sendResponseError(res, error, 'extras.js', 'addAlertMsg');
    }
}


/**
 * Controller to edit alert messages.
 * @param {object} req 
 * @param {object} res 
 * @deprecated
 */
exports.editAlertMsg = async (req, res) => { 
    try {
        await executeProc(req.body, req.db, 'proc_edit_alert_msg');
        res.json({msg: "Alert Message Changed Successfully"});
    } catch (error) {
        sendResponseError(res, error, 'extras.js', 'editAlertMsg');
    }
}


/**
 * Controller to get companies (active ones and non active ones).
 * @param {object} req 
 * @param {object} res 
 * @deprecated
 */
exports.getCompanies = async (req, res) => { 
    try {
        const {prod} = req.body;
        const body = removePropertyFromObject(req.body, 'prod');
        const result = await executeProc(body, req.db, 'proc_get_companies');
        res.json(prod? convertDatabaseFieldstoProcedureFields(result, COMPANIES): result);
    } catch (error) {
        sendResponseError(res, error, 'extras.js', 'getCompanies');
    }
}


/**
 * Controller to get colaborador by the company that is related to.
 * @param {object} req 
 * @param {object} res 
 */
exports.getFuncionarioByCompany = async (req, res) => { 
    try {
        let result = await executeProc(req.body, req.db, 'proc_get_funcionario_by_company');
        result = convertDatabaseFieldstoProcedureFields(result, USER_OF_TERMINAL);
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'extras.js', 'getFuncionarioByCompany');
    }
}


/**
 * Controller to get the extras in the database of a certain type.
 * @param {object} req 
 * @param {object} res 
 */
exports.getExtras = async (req, res) => {
    try {
        const {prod} = req.body;
        const body = removePropertyFromObject(req.body, 'prod');
        const result = await executeProc(body, req.db, 'proc_get_extras');
        const toSend = prod? convertDatabaseFieldstoProcedureFields(result, EXTRAS) : result;
        res.json(toSend);
    } catch (error) { 
        sendResponseError(res, error, 'extras.js', 'getExtras');
  }
};


/**
 * Controller to get all the extras in the database.
 * @param {object} req 
 * @param {object} res 
 */
exports.getAllExtras = async (req, res) => { 
    try {
        const {prod} = req.body;
        let typeExtra;

        const body = removePropertyFromObject(req.body, 'prod');
        let result = await executeProc(body, req.db, 'proc_get_extras');
        result = convertDatabaseFieldstoProcedureFields(result, EXTRAS);
        const mappedExtras = {};

        result.forEach(extra => { 
            typeExtra = extra.p_id_etype;
            if(!doesArrayExist(mappedExtras[typeExtra]))
                mappedExtras[typeExtra] = [extra]
            else
                mappedExtras[typeExtra].push(extra);
        })
        res.json(mappedExtras);
    } catch (error) { 
        sendResponseError(res, error, 'extras.js', 'getAllExtras');
  }
}

/**
 * Controller to edit the extra (departments, id_types, reasons to reject, etc).
 * @param {object} req 
 * @param {object} res 
 */
exports.editExtra = async (req, res) => {
    try {
        await executeProc(req.body, req.db, 'proc_edit_extra');
        if(req.body.p_id_etype === INDUCTION_TYPE) {
            const translatioEdit = {...req.body, p_id: req.body.p_id+1, p_name: `${req.body.p_name}_EN`}
            await executeProc(translatioEdit, req.db,'proc_edit_extra');
        }
        res.json({msg: 'Operation Successfull'});
    } catch (error) {
        sendResponseError(res, error, 'extras.js', 'editExtra');
    }
}

/**
 * Controller to add the extra (departments, id_types, reasons to reject, etc).
 * @param {object} req 
 * @param {object} res 
 */
exports.addExtras =  async (req, res) => {
    try {
        let body = removePropertyFromObject(req.body, 'code');
        body = removePropertyFromObject(body, 'name');
        body = removePropertyFromObject(body, 'active');

        await executeProc(body, req.db, 'proc_add_extra');
        res.json({msg: "Operation Successfull"});
    } catch (error) {
        sendResponseError(res, error, 'extras.js', 'addExtras');
    }
}