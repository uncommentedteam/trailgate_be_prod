const { validationResult } = require("express-validator");

const { getUserById, getVisitorByDocumentId, sendResponseError } = require("../utils/controllerHelper");
const { writeToErrorLog, removePropertyFromObject, doesObjectExist, hashString, isNotEmptyObject } = require("../utils/utils");
const { executeProc } = require("../db/dbAccess");
const User = require("../model/User");
const Notification = require("../utils/Notification");
const {sendAccountCredentialsToUser} = require("../utils/authUtils");
const { USER } = require("../utils/constants");
const { convertDatabaseFieldstoProcedureFields } = require("../utils/responseHandlers");
const { setParam } = require("../utils/auxPScontroller");
const { saveNewUser, saveNewHostUser, saveBulkNewHostUsers, saveBulkNewUsers } = require("./processes/auth");
const { resendAccountVerificationIfNecessary, isUserNotActivated, isUserBlocked, isUserAServiceProvider, addRepresentativeOfServiceFlag, hasNotFoundUser } = require("../utils/authUtils");
const logger = require("../utils/logger");


//::>> PREVIOSU VERSION 

/**
 * Controller to get a specific user by the id 
 * @param {object} req 
 * @param {object} res 
 */
exports.getUser = async (req, res) => { 
	try { 

		let result = await getUserById(req.db,req.body.p_id);
		result = convertDatabaseFieldstoProcedureFields(result, USER);
		logger.debug(`User requested: ${result.p_fname} ${result.p_lname}`);
		res.json(result);
	} catch (error) {
		sendResponseError(res, error, 'auth.js', 'getUser');
	}
}


/**
 * Controller to login.
 * @param {object} req 
 * @param {object} res 
 */
exports.login = async (req, res) => {

	try {
		const {prod} = req.body;
		const body = removePropertyFromObject(req.body, 'prod');
		body.p_psw = hashString(body.p_psw);
		let result = await executeProc(body,req.db, "proc_get_selected_by_email_or_pnumber_and_psw_users",);
		let user = result[0];

		if(user && !user.isvisitor) { 
			result = await executeProc({...body, p_isvisitor: 0},req.db, "proc_get_selected_by_email_or_pnumber_and_psw_users",);
			user = result[0];
		}

		if(prod) { 
			if(hasNotFoundUser(user)) 
				return res.status(401).json({ error: '', root_err: "Username or password not valid!", });
			
			user = convertDatabaseFieldstoProcedureFields(user, USER);
			resendAccountVerificationIfNecessary(user);

			if(isUserNotActivated(user)) 
				return res.status(401).json({error: '', root_err: "User is not active. Please verify your account with the email link or contact support.",});

			if(isUserBlocked(user)) 
				return res.status(401).json({error: '', root_err: "This account has been blocked! Please contact support.", });

			if(isUserAServiceProvider(user)) { 
				await addRepresentativeOfServiceFlag(user);
			}
		}

		if(isNotEmptyObject(user))
			logger.info(`Login(${user.p_id}) : ${user.p_fname} ${user.p_lname}`);

        res.json(user);
        
	} catch (error) {
		sendResponseError(res, error, 'auth.js', 'login');
	}
};

/**
 * Controller to get the user by token.
 * @param {object} req 
 * @param {object} res 
 */
exports.getUserByToken = async (req, res) => {
	try {
		const {prod} = req.body;
		const body = removePropertyFromObject(req.body, 'prod');

		let result = await executeProc(body,req.db,"proc_get_selected_by_tokken_users");

		let user = result[0];
		if(!doesObjectExist(user)) return res.status(401).json({error: '', root_err: "Invalid User Token! You Token does not exist or it has expired.",});
		
		if(!user.isvisitor) { 
			user = await executeProc({...body, p_isvisitor: 0}, req.db, 'proc_get_selected_by_tokken_users');
			user = user[0];
		}

		if(prod) { 
			result = convertDatabaseFieldstoProcedureFields(user, USER);
		} else { 
			result = user[0];
		}
		res.json(result);
	} catch (error) {
		sendResponseError(res, error, 'auth.js', 'getUserByToken');
	}
};

/**
 * Controller to register a new user (visitors or callaborators).
 * @param {object} req 
 * @param {object} res 
 */
exports.regUser = async (req, res) => {
    try {
		await saveNewUser(req.body);
		res.json({msg: 'User Registered!'});
} catch (error) {
		sendResponseError(res, error, 'auth.js', 'regUser');
    }
};

exports.regBulkUsers = async (req, res) => { 
	try {
		await saveBulkNewUsers(req.body.users);
		res.json({msg: 'Users Registered.'});
	} catch (error) {
		sendResponseError(res, error, 'auth.js', 'regUser');
	}
}

exports.grantAcccess = async (req, res) => { 
	try {
		await saveNewHostUser(req.body);
		res.json({msg: 'Success'});		
	} catch (error) {
		sendResponseError(res, error, 'auth.js', 'regUser');
	}
}

exports.grantAccessBulk = async (req, res) => { 
	try {
		await saveBulkNewHostUsers(req.body);
		res.json({msg: 'Success'});	
	} catch (error) {
		sendResponseError(res, error, 'auth.js', 'regUser');
	}
}

/**
 * Controller to resend the verification email for the visitor who is about to register.
 * @param {object} req 
 * @param {object} res 
 */
exports.resendVerificationEmail = async (req, res) => { 
	try {   
        let user = await getVisitorByDocumentId(req.db, req.body.p_nid);
		user = convertDatabaseFieldstoProcedureFields(user, USER);

		const notification = new Notification(req.db, user.p_id, user.p_isvisitor);
		notification.set('user_tokken', user.p_tokken);
		notification.set('p_is_toEmail', false);
		notification.senVerificationEmail();

        res.json({msg: 'Verification Sent'});
    } catch (error) {
		sendResponseError(res, error, 'auth.js', 'resendVerificationEmail');
    }
}

/**
 * Controller to edit the main information about the user.
 * @param {object} req 
 * @param {object} res 
 */
exports.editUser = async (req, res) => {
	try {

		const user = new User(req.body, req.db);
		if(req.body.hasUpdatedPassword)
			user.user.p_psw = hashString(user.user.p_psw);
		const [isSuccessful, message] = await user.updateUser();
		if(!isSuccessful) throw new Error(message);

		res.json({ msg: "User data edited Successfully" });

	} catch (error) {
        sendResponseError(res, error, 'auth.js', 'editUser');
	}
};

/**
 * Controller to edit the main information about the user.
 * @param {object} req 
 * @param {object} res 
 */
exports.editUser_v2 = async (req, res) => {
    try { 
        await executeProc(req.body , req.db, 'proc_edit_user');
        res.json({msg: 'User data edited Successfully'})
    } catch (err) {
		sendResponseError(res, error, 'auth.js', 'editUser_v2');
	}
}

/**
 * Controller to verify the user.
 * @param {object} req 
 * @param {object} res 
 */
exports.verification = async (req, res) => {
	try {
		const user  = new User(req.body, req.db);
		const [isSuc, message] = await user.activateChangeToken();
		if(!isSuc) { throw new Error(message)};

		res.json({msg: 'User verified!'});
	} catch (error) {
		sendResponseError(res, error, 'auth.js', 'verification');	}
};

/**
 * Controller to register user for public service.
 * @param {object} req 
 * @param {object} res 
 */
exports.create_PublicService = async (req, res) => {
	let parm = await setParam(req)
	try {
        const result = await executeProc(parm, req.db, 'proc_create_ps');
		sendAccountCredentialsToUser({...parm, p_psw: '12345678'});
        res.status(200).json(result);
    } catch (error) {
        sendResponseError(res, error, 'auth.js', 'create_PublicService');
    }
}
