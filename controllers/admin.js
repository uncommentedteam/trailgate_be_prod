const { validationResult } = require("express-validator");
const { executeProc } = require("../db/dbAccess");
const { writeToErrorLog, getError, removePropertyFromObject, sendUpdate, getToday, getStatsOfVisits, getTerminalStats, getLastIdOfTable, doesObjectExist, doesVariableExist, isEmptyObject } = require("../utils/utils");
const {unifyGroupedVisits, getVisitorById, getColaboratorById, getUserByEmail, sendResponseError } = require("../utils/controllerHelper");
const { convertDatabaseFieldstoProcedureFields, convertFullCompanyFieldsToProcedureFields } = require("../utils/responseHandlers");
const path = require('path');
const fs = require('fs');
const emailSender = require('../utils/mailSender');
const { COMPANIES, USER, INDUCTION, COMPANY, ALERT_MESSAGE, FULL_COMPANIES, SUB_COMPANIES } = require("../utils/constants");
const randomString = require('randomstring'); 
const logger = require("../utils/logger");


/**
 * Controller to get All the users in the database by filtering what kind of user it is required.
 * @param {object} req 
 * @param {object} res 
 */
exports.getAllUsrs = async(req, res) => {

try {
    const {prod} = req.body;
    const body = removePropertyFromObject(req.body, 'prod');
    let result = await executeProc(body, req.db, 'proc_get_users');
    if(prod) result = result.map(user => convertDatabaseFieldstoProcedureFields(user, USER));
    res.json(result);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getAllUsrs');
}
}

/**
 * Controller to block visitors.
 * @param {object} req 
 * @param {object} res 
 */
exports.addBlocked = async(req, res) => {
try {
    await executeProc(req.body, req.db, 'proc_add_blocked');
    sendUpdate(req);
    res.json({msg: 'Succesfull'});
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'addBlocked');    }
}

/**
 * Controller to edit blocked visitors.
 * @param {object} req 
 * @param {object} res 
 */
exports.editBlocked = async(req, res) => {
try {
    await executeProc(req.body, req.db, 'proc_edit_blocked');
    sendUpdate(req);
    res.json({msg: 'Succesfull'});
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'editBlocked');
}
}


/**
 * Controller to get Current Date Stats of a terminal.
 * @param {object} req 
 * @param {object} res 
 */
exports.getThreadStats = async(req, res) => {
try {
    const {prod} = req.body;
    const allVisits = await executeProc({ p_dtm_start: getToday().toISOString() , p_dtm_end: getToday().endOf('month').toISOString(), p_id_comp: req.body.p_id_thread }, req.db, 'proc_get_svisit_vstates');
    if(!prod) { 
        const calculatedStats = getStatsOfVisits(allVisits);
        res.json(calculatedStats)
    } else { 
        const visits = unifyGroupedVisits(allVisits);
        const stats = await getTerminalStats(visits, req.db);
        res.json(stats);
    }
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getThreadStats');
}
}


/**
 * Controller to get Current Date Stats.
 * @param {object} req 
 * @param {object} res 
 */
exports.getStats = async(req, res) => {
try {
    const {prod} = req.body;
    let allVisits = await executeProc({ p_dtm_start: getToday().format("YYYY-MM-DD"), p_dtm_end: getToday().endOf('month').toISOString() }, req.db, 'proc_get_svisit_vstates');
    const unfinishedVisits = await executeProc({ p_dtm_start: getToday().format("YYYY-MM-DD"), p_dtm_end: getToday().endOf('month').toISOString() }, req.db, 'proc_get_visits_in');
    allVisits = [...allVisits, ...unfinishedVisits];
    if(!prod) { 
        const calculatedStats = getStatsOfVisits(allVisits);
        res.json(calculatedStats)
    } else { 
        const visits = unifyGroupedVisits(allVisits);
        const stats = await getTerminalStats(visits, req.db);
        res.json(stats);
    }
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getStats');
}
}


/**
 * Controller to write the client app configurations.
 * @param {object} req 
 * @param {object} res 
 */
exports.writeClientConfigurations = async (req, res) => {
try {
    const configurationFile = path.join(__dirname, '..', 'config', 'config.json');
    if(isEmptyObject(req.body))
        throw new Error('Empty Configuration File!');
    fs.writeFileSync(configurationFile, JSON.stringify(req.body));
    res.json("Configurations Updated");
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'writeClientConfigurations');
}
}


/**
 * Controller to get the client configurations.
 * @param {object|null} req 
 * @param {object|null} res 
 * @returns {object|null}
 */
exports.getClientConfigurations = async(req, res) => {
    try {
        const configurationFile = path.join(__dirname, '..', 'config', 'config.json');
        const fileContent = fs.readFileSync(configurationFile, { encoding: 'utf-8' });
        const configurations =  JSON.parse(fileContent);
        logger.debug(`Configurations file requested and provided.`);
        if (doesVariableExist(req) || doesVariableExist(res)){
            res.json(JSON.parse(fileContent));
        } else {
            return JSON.parse(fileContent)
        }       
        
    } catch (error) {
        sendResponseError(res, error, 'admin.js', 'getClientConfigurations');
    }
}

/**
 * Controller to get request an induction to a user of a visit.
 * @param {object} req 
 * @param {object} res 
 */
exports.reqInduction = async(req, res) => {
try {
    const { sendInductionRequest, } = require("./processes/admin");
    await sendInductionRequest(req.body);
    sendUpdate(req);
    res.json({msg: "Success"});
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'reqInduction');   
}
}


/**
 * Controller to get request an induction to all users of a visit.
 * @param {object} req 
 * @param {object} res 
 */
exports.reqInductionAll = async(req, res) => {
try {
    const { sendInductionRequestForUserGroup } = require("./processes/admin");
    await sendInductionRequestForUserGroup(req.body);
    res.json({msg: "success"});
} catch (error) {
    writeToErrorLog(error, 'admin.js', 'reqInductionAll');
    return res.status(500).json({ root_msg: error.message, error: 'Server Error!'});   
}
}


const relateInductionsToTypeInduction = (indTypeToIndQuestion, inductionQuestions, p_id_type_induction) => { 
    let indType;
    let inductionQuestion;
    let indQuestExtra;
    if (doesVariableExist(p_id_type_induction)) { 
        inductionQuestions.forEach(inductionQuestion => {
            indType = indTypeToIndQuestion.find(indTypeToQuest => {
                const isOfType = indTypeToQuest.int_id_induction === inductionQuestion.p_cod;
                const isOfReqType = (indTypeToQuest.int_id_type_induction === p_id_type_induction) || (indTypeToQuest.int_id_type_induction === (p_id_type_induction + 1));
                return isOfType && isOfReqType;
            });
            inductionQuestion.p_cod_extra = [indType.int_id_type_induction];
        })
    } else { 
        indTypeToIndQuestion.forEach(indType => { 
            inductionQuestion = inductionQuestions.find(indQuest => indQuest.p_cod === indType.int_id_induction);
            indQuestExtra = inductionQuestion.p_cod_extra;
            inductionQuestion.p_cod_extra = indQuestExtra? [...indQuestExtra, indType.int_id_type_induction] : [indType.int_id_type_induction];
        });
    }
}

const addTranslatedQuestions = (inductionQuestions, allQuest, p_id_type_induction) => { 
    const translations = inductionQuestions.map(indQuest => { 
        const translation = allQuest.find(indQ => (indQ.p_answer === indQuest.p_answer) && (indQ.p_cod !== indQuest.p_cod));
        if(doesObjectExist(translation)) translation.p_cod_extra = [p_id_type_induction + 1];
        return translation;
    });
    return translations.filter(trans => doesObjectExist(trans));
}

/**
 * Controller to get the induction Questions.
 * @param {object} req 
 * @param {object} res 
 */
exports.getInductionQuest = async(req, res) => {
try {
    const body = removePropertyFromObject(req.body, 'prod');
    let result = await executeProc(body, req.db, 'proc_get_induction');
    result = result.map( res => convertDatabaseFieldstoProcedureFields(res, INDUCTION));
    const extrasRefResult = await executeProc({p_is_active: 1}, req.db, 'proc_get_ref_type_induction');
    relateInductionsToTypeInduction(extrasRefResult, result);
    res.json(result);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getInductionQuest');
}
}


/**
 * Controller submit induction results.
 * @param {object} req 
 * @param {object} res 
 */
exports.subInduction = async(req, res) => {
try {
    const { sendInductionResultMessage } = require("./processes/admin");
    const { inductionUserData , inductionQuestionsResult } = req.body;
    const {p_id_x, p_id_svisit, p_tokken, p_id_extra, minPointsToPass} = inductionUserData;
    

    let score = 0;
    inductionQuestionsResult.forEach(qst => { 
        if (qst.selectedOption && qst.selectedOption.isCorrect) score += qst.p_points; 
    })

    //::>> Verify Induction Result
    const p_isinduction = score >= minPointsToPass ? 1 : 0;

    const usr = await executeProc({p_isvisitor: 1, p_tokken},req.db,"proc_get_selected_by_tokken_users");
    await executeProc({ p_id_visitor: usr[0].cod, p_tokken: randomString.generate(), p_deep_induction: p_isinduction, p_dtm_deep_induction: getToday().toISOString(), p_id_tip_induc: p_id_extra, p_svisit: p_id_svisit }, req.db, 'proc_edit_induction_state');
    await sendInductionResultMessage(p_id_x, p_id_svisit, p_isinduction);
    sendUpdate(req);

    res.json({ msg: "Usuario notificado" });

} catch (error) {
    sendResponseError(res, error, 'admin.js', 'subInduction');
}
}

exports.forceInductionResult = async (req, res) => { 
    try {
        await executeProc(req.body, req.db, 'proc_edit_induction_state');
        sendUpdate(req);
        res.json({ msg: "Success" });
    } catch (error) {
        sendResponseError(res, error, 'admin.js', 'subInduction');
    }
}


/**
 * Controller to get submit issues to the support team.
 * @param {object} req 
 * @param {object} res 
 */
exports.contactUs = async(req, res) => {
try {
    const { p_email, p_detail } = req.body;
    emailSender.sendContactUsEmail(p_email, p_detail);
    res.json({ msg: 'Email Sent' });
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'contactUs');
}
}

/**
 *  Controller to get the last induction question id.
 * @param {object} req 
 * @param {object} res 
 */
exports.getInductioQuestionId = async(req, res) => {
try {
    const result = await getLastIdOfTable(req.db, 'tbl_induction');
    res.json(result);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getInductioQuestionId');    
}
}   


/**
 * Controller to get the induction question for the induction process.
 * @param {object} req 
 * @param {object} res 
 */
exports.getInduction = async(req, res) => {
try {
    const {prod, p_id_type_induction} = req.body;
    const body = removePropertyFromObject(req.body, 'prod')
    let inductionQuestions = await executeProc({...body, p_active: 1}, req.db, 'proc_get_induction');
    inductionQuestions = inductionQuestions.map(inductionQuestion => convertDatabaseFieldstoProcedureFields(inductionQuestion, INDUCTION));
    const copyOfIndQuest = [...inductionQuestions];
    const indTypeToIndQuest = await executeProc({p_is_active: 1, p_id_type_induction}, req.db, 'proc_get_ref_type_induction');
    

    if (p_id_type_induction) { 
        // let indTypeToIndQuest = await executeProc({p_is_active: 1, p_id_type_induction}, req.db, 'proc_get_ref_type_induction');
        // indTypeToIndQuest = [...indTypeToIndQuest];

        const indIdsArray = indTypeToIndQuest.map(typeToInd => typeToInd.int_id_induction);
        inductionQuestions = inductionQuestions.filter(indQuestion => { 
            const outcome = indIdsArray.includes(indQuestion.p_cod);
            return outcome;
        })
    }

    const configurationFile = path.join(__dirname, '..', 'config', 'config.json');
    const fileContent = fs.readFileSync(configurationFile, { encoding: 'utf-8' });
    const configurations = JSON.parse(fileContent);

    if (prod) {
        inductionQuestions = inductionQuestions.sort(() => .5 - Math.random());
        if (inductionQuestions.length >= configurations.QUESTIONS_PER_INDUCTION) { 
            inductionQuestions = inductionQuestions.slice(0, configurations.QUESTIONS_PER_INDUCTION);
        } 

        inductionQuestions = inductionQuestions.filter(indQuest => doesObjectExist(indQuest));
        relateInductionsToTypeInduction(indTypeToIndQuest, inductionQuestions, p_id_type_induction);
        const translations = addTranslatedQuestions(inductionQuestions, copyOfIndQuest, p_id_type_induction);
        inductionQuestions = [...inductionQuestions, ...translations];
    }

    res.json(inductionQuestions);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getInduction');
}
}


/**
 * Controller to add Induction Questions.
 * @param {object} req 
 * @param {object} res 
 */
exports.addInductionQuestion = async(req, res) => {
try {
    const inductionExtras = req.body.p_cod_extra;
    await executeProc(req.body, req.db, 'proc_add_induction');
    const inductionCode = await getLastIdOfTable(req.db, 'tbl_induction');
    const typeToQuestionRelation = {
        p_cod_induction: inductionCode,
        p_is_active: req.body.p_is_valid
    }
    for (let p_cod_extra of inductionExtras) { 
        await executeProc({...typeToQuestionRelation, p_cod_extra}, req.db, 'proc_add_ref_type_induction');
    }

    res.json({message: "Success."});
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'addInductionQuestion');
}
}


/**
 * Controller to edit induction questions.
 * @param {object} req 
 * @param {object} res 
 */
exports.editInductionQuestion = async(req, res) => {
try {
    const result = await executeProc(req.body, req.db, 'proc_edit_induction');
    const indTypesOfQuestion = await executeProc({p_id_induction: req.body.p_cod}, req.db, 'proc_get_ref_type_induction');
    let editedTypeOfInduction = [...req.body.p_cod_extra];
    let inductionTypeCode;
    for (let  indType of indTypesOfQuestion) { 
        inductionTypeCode = indType.int_id_type_induction;
        if (editedTypeOfInduction.includes(inductionTypeCode)) { 
            await executeProc({p_id_induction: req.body.p_cod, p_id_type_induction: inductionTypeCode, p_is_active: 1}, req.db, 'proc_edit_ref_type_induction');
        } else { 
            await executeProc({p_id_induction: req.body.p_cod, p_id_type_induction: inductionTypeCode, p_is_active: 0}, req.db, 'proc_edit_ref_type_induction');
        }
        editedTypeOfInduction =  editedTypeOfInduction.filter(extraOfInductionCode => extraOfInductionCode !== inductionTypeCode);
    };

    if ((editedTypeOfInduction.length > 0) && editedTypeOfInduction[0]) { 
        editedTypeOfInduction.forEach(extraOfInductionCode => { 
            executeProc({p_cod_induction: req.body.p_cod, p_cod_extra: extraOfInductionCode, p_is_active: 1}, req.db, 'proc_add_ref_type_induction');
        });
    }
    
    res.json(result);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'editInductionQuestion');  
}
}


/**
 * Controller to add an external company.
 * @param {object} req 
 * @param {object} res 
 */
exports.addExternalCompanyAndUser = async(req, res) => {
    const { saveNewEntity } = require("./processes/admin");
    try {
        await saveNewEntity(req.body);
        res.json({ msg: 'Successful' });
    } catch (error) {
        sendResponseError(res, error, 'admin.js', 'addExternalCompanyAndUser');    
    }
}


/**
 * Controller to visits by entity.
 * @param {object} req 
 * @param {object} res 
 */
exports.getUsrByEntity = async(req, res) => {
try {
    let result = await executeProc(req.body, req.db, 'proc_get_user_by_entity');
    result = result.map(resul => convertDatabaseFieldstoProcedureFields(resul, USER));
    res.json(result);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getAllUsrs');
}
}

/**
 * Controller to get all the saved companies.
 * @param {object} req 
 * @param {object} res 
 */
exports.getCompanies = async(req, res) => {
    try {
        const {prod} = req.body;
        const body = removePropertyFromObject(req.body, 'prod');
        let result = await executeProc(body, req.db, 'proc_get_all_companies_subcompanies');
        if (prod) {
            /* In here i convert the subcompanies of companies a standalone company by converting the company object to a subcompany object of name. 
            * When retriving from database, if a company has 4 subcompanies, i get 4 objects of this company in which str_cname is the same and str_scname is the name of each subcompany.
            * So i take the names from str_scomp and replace in str_comp for the 4 objects, but making a copy of the first one. After processing I add this copy to the result (from 4 to 5 now);
            */
            const companiesWithSubCompanies = [];
            const subCompCompIDMap = {};
            let compWithSubCompName = "";
            result = result.filter(comp => {
                if(doesVariableExist(comp.str_scname) && comp.str_scname !== comp.str_cname) {
                    if(comp.str_cname !== compWithSubCompName)
                        companiesWithSubCompanies.push({...comp, int_id: comp.int_id[0]});

                    // Add a mapping with subcompanyName --> companyID
                    subCompCompIDMap[comp.str_scname] = comp.int_id[0];

                    compWithSubCompName = comp.str_cname;
                    comp.str_cname = comp.str_scname;
                    comp.int_id = comp.int_id[1];
                    /* The id's come as arrays, the first is the id of the object and the second is the id of the related object. 
                    * for related objects, i use the second element,and for the main object i use the firs,
                    * as you can see in the else block.
                    */
                   
                } else { 
                    comp.int_id = comp.int_id[0];
                }
                return !comp.bool_is_ps;
            });
            result = [...result, ...companiesWithSubCompanies];
            result = convertDatabaseFieldstoProcedureFields(result, COMPANIES);
            result = [...result, subCompCompIDMap];
        }
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'admin.js', 'getCompanies');
    }
}

/**
 * Controller to get all the saved companies for edition.
 * @param {object} req 
 * @param {object} res 
 */
exports.getAllCompanies = async(req, res) => {
    try {
        const body = removePropertyFromObject(req.body, 'prod');
        let result = await executeProc(body, req.db, 'proc_get_all_companies_subcompanies');
        
        const unique_cids = [], newResult = [];
        let auxCompany = {}, auxSubComp = {};
        for (let company of result) { 
            const [cid, scid] = company.int_id;
            if(!unique_cids.includes(cid)) { 
                unique_cids.push(cid);
                auxCompany = convertDatabaseFieldstoProcedureFields([company], FULL_COMPANIES)[0];
                auxCompany.p_id = cid;
                auxCompany.sub_companies = [];
                newResult.push(auxCompany);
            } else { 
                auxCompany = newResult[unique_cids.indexOf(cid)];
            }

            if(scid !== null) { 
                auxSubComp = convertDatabaseFieldstoProcedureFields([company], SUB_COMPANIES)[0];
                auxSubComp.p_id = scid;
                auxSubComp.p_id_company = cid;
                auxCompany.sub_companies.push(auxSubComp);
            }
        }

        //if (prod) result = convertDatabaseFieldstoProcedureFields(result, FULL_COMPANIES);
        res.json(newResult);
    } catch (error) {
        sendResponseError(res, error, 'admin.js', 'getAllCompanies');
    }
}



/**
 * Controller to get subcompany of a company.
 * @param {object} req 
 * @param {object} res 
 */
exports.getSubCompanies = async(req, res) => {
try {
    const result = await executeProc(req.body, req.db, 'proc_get_sub_company_by_company_or_cod');
    res.json(result);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getSubCompanies');
}
}

/**
 * Controller to add and internal compnay or terminal.
 * @param {object} req 
 * @param {object} res 
 */
exports.addCompany = async(req, res) => {
try {
    const result = await executeProc(req.body, req.db, 'proc_add_company');
    res.json(result[0]);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'addCompany');
}
}

/**
 * Controller to add a subcompany to a company(terminal).
 * @param {object} req 
 * @param {object} res 
 */
exports.addSubComp = async(req, res) => {
try {
    await executeProc(req.body, req.db, 'proc_add_sub_company');
    res.json({ msg: 'Success' });
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'addSubComp');
}
}

/**
 * Controller to edit an internal company or terminal.
 * @param {object} req 
 * @param {object} res 
 */
exports.editCompany = async(req, res) => {
try {
    await executeProc(req.body, req.db, 'proc_edit_company');
    res.json({ msg: 'Company Edited!' });
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'editCompany');
}
}

/**
 * Controller to edit a subcompnay of a company(a terminal).
 * @param {object} req 
 * @param {object} res 
 */
exports.editSubCompany = async(req, res) => {
try {
    const valRes = validationResult(req);
    if (!valRes.isEmpty()) {
        return res.status(400).json({ error: valRes.array() });
    }

    await executeProc(req.body, req.db, 'proc_edit_sub_company');
    res.json({ msg: 'Company Edited!' });

} catch (error) {
    res.status(500).json({ errors: [{ root_err: error.message }, { err: 'Server Error. Something went wrong!' }] })
}
}

/**
 * Controller to add an Alert Message.
 * @param {object} req 
 * @param {object} res 
 */
exports.addAlertMsg = async(req, res) => { 
try {
    await executeProc(req.body, req.db, 'proc_add_alert_msg');
    res.json({msg: 'success'});
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'addAlertMsg');
}
}

/**
 * Controller to edit an Alert Message.
 * @param {object} req 
 * @param {object} res 
 */
exports.editAlertMsg = async(req, res) => { 
try {
    await executeProc(req.body, req.db, 'proc_edit_alert_msg');
    res.json({msg: 'success'});
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'editAlertMsg');  
}
}

/**
 * Controller to get Alert Messages that are active.
 * @param {object} req 
 * @param {object} res 
 */
exports.getAlertMsg = async(req, res) => { 
try {
    const {prod} = req.body;
    const body = removePropertyFromObject(req.body, 'prod');
    let result = await executeProc(body, req.db, 'proc_get_alert_msg');
    if(prod) result = result.map(res => convertDatabaseFieldstoProcedureFields(res, ALERT_MESSAGE));
    res.json(result);
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'getAlertMsg');
}
} 


/**
 * Controller to send a link for an external company to register.
 * @param {object} req 
 * @param {object} res 
 */
exports.sendCompRegLink = async(req, res) => {
try {
    const { p_email, p_cod_core } = req.body;
    const user = await getColaboratorById(req.db, p_cod_core);
    const token = user.tokken;
    emailSender.sendCompRegEmail(p_email, token);
    res.json({ msg: 'Successful' });
} catch (error) {
    sendResponseError(res, error, 'admin.js', 'sendCompRegLink'); 
}
}

/**
 * Controller for password recovery
 * @param {object} req 
 * @param {object} res 
 */
exports.recoverPassword = async(req, res) => {
try {
    const { performPasswordLinkNotificationProcess } = require("./processes/admin");
    await performPasswordLinkNotificationProcess(req.body);
    res.json({ msg: "Success!" });

} catch (error) {
    sendResponseError(res, error, 'admin.js', 'recoverPassword');
}
}



exports.report_ssrs_view = async(req, res) => {
    try {
        let result = await req.db.query("SELECT value FROM settings WHERE name='ROOT_REPORT_SERVER_URL';")
        if ( result.recordset.length > 0) {
            const reportsFile = path.join(__dirname, '..', 'config', 'reports_conf.json');
            var fileContent = JSON.parse(fs.readFileSync(reportsFile));
            var lstReports = []
            const report_root_url = result.recordset[0]["value"]
            for (const rname in fileContent) {
                var report = fileContent[rname]
                report["PATH"] = report_root_url + rname
                lstReports.push(report)
            }
            res.json(lstReports)
        } else {
            res.status(404).json()

        }
        
    } catch (error) {
        logger.error(error.message);
        res.status(500).json()
    }
}

/*
[ 
    { contact : {email: "", p_pnumber_1: ""} , subject: "", message: "" },
    { contact : {email: "", p_pnumber_1: ""} , subject: "", message: "" },
    ...
]
*/
exports.sendAdminNotifications = async(req, res) => { 
    const { sendAdminNotificationMessagesProcess, } = require("./processes/admin");
    try {
        await sendAdminNotificationMessagesProcess(req.body.notifications);
    } catch (error) {
        logger.error(error.message);
        sendResponseError(res, error, 'admin.js', 'recoverPassword');
    }
}

