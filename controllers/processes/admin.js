const { executeProc } = require("../../db/dbAccess");
const { getDatabaseConnection } = require("../../db/dbConnector");
const { getAndBeginTransaction } = require("../../db/dbHelpers");
const { saveCompanyUserMainData, prepareCompanyDataForSave, saveCompanyData, prepareUserDetaisDataForSave, saveUserDetailsData, sendNotificationToCompanyUser, sendPasswordRecoveryLinkToUser, sendInductionRequestNotificationToVisitor, sendInductionCompletitionMessageToVisitor, sendGeneralPurposeMessage } = require("../../utils/adminUtils");
const { getUserByDBId } = require("../../utils/authUtils");
const { getUserByEmail } = require("../../utils/controllerHelper");
const logger = require("../../utils/logger");
const { buildNotificationMessageOptions } = require("../../utils/NotificationMessageBuilder");
const { isNotEmptyObject } = require("../../utils/utils");
const { getVisitByDBId } = require("../../utils/visitUtils");

exports.saveNewEntity = async (entityReqData) => { 
    const transaction = await getAndBeginTransaction();
    try {
        const userDbId = await saveCompanyUserMainData(entityReqData, transaction);
        prepareCompanyDataForSave(userDbId, entityReqData);
        const compId = await saveCompanyData(entityReqData, transaction);
        prepareUserDetaisDataForSave(compId, entityReqData);
        await saveUserDetailsData(entityReqData, transaction);
        sendNotificationToCompanyUser(entityReqData);
        transaction.commit();
    } catch (error) {
        transaction.rollback();
        logger.error(error.message);
        throw error;
    }
}

exports.performPasswordLinkNotificationProcess = async (userData) => { 
    try {
        const dbCon = await getDatabaseConnection();
        let result = await executeProc(userData, dbCon, 'proc_get_user_by_one_of_property');
       
        if(isNotEmptyObject(result)) {
            const user = result[0];
            logger.debug(`Sending password recovery link for contact ${userData.p_contact}...`);
            sendPasswordRecoveryLinkToUser(user);
        } else { 
            logger.warn(`No password recovery link was sent to ${userData.p_contact}`);
        }
    } catch (error) {
        logger.error(`Failed to send password recovery email for contact ${userData.p_contact}`);
        throw error;
    }
}

exports.sendInductionRequest = async (reqData) => { 
    try {
        const {p_id_x, p_id_svisit} = reqData;
        const dbCon = await getDatabaseConnection();
        const visitor = await getUserByDBId(p_id_x);
        const visit =  await getVisitByDBId(p_id_svisit);
        await executeProc({ p_id: p_id_svisit, p_id_x: p_id_x }, dbCon, 'proc_edit_visitor_visit_induction');
        sendInductionRequestNotificationToVisitor(visitor, visit);
    } catch (error) {
        logger.error(error.message);
        throw error;
    }
}

exports.sendInductionRequestForUserGroup = async (reqData) => { 
    try {
        const visitId = reqData.p_id_svisit;
        const usersIdArray = reqData.users;
        const visit = await getVisitByDBId(visitId);
        usersIdArray.forEach(async userId => {
            const visitor = await getUserByDBId(userId);
            await executeProc({ p_id: visitId, p_id_x: userId }, dbCon, 'proc_edit_visitor_visit_induction');
            sendInductionRequestNotificationToVisitor(visitor, visit);
        });
    } catch (error) {
        logger.error(error.message);
        throw error;
    }
}

exports.sendInductionResultMessage = async (userId, visitId, inductionResult) => { 
    const visit = await getVisitByDBId(visitId);
    const user = await getUserByDBId(userId);
    sendInductionCompletitionMessageToVisitor(user ,visit, inductionResult);
}

exports.sendAdminNotificationMessagesProcess = async (notificationOptionsList) => { 
    try {
        for(let notificationOption of notificationOptionsList) { 
            await sendGeneralPurposeMessage(notificationOption.contact, notificationOption.subject, notificationOption.message)            
        }
    } catch (error) {
        logger.error(error.message);
        throw error;
    }
}