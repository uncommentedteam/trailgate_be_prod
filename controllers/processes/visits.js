const { getDatabaseConnection } = require("../../db/dbConnector");
const { getAndBeginTransaction } = require("../../db/dbHelpers");
const { saveUserFromVisitWhenNonExistent } = require("../../utils/authUtils");
const { getColaboratorById } = require("../../utils/controllerHelper");
const logger = require("../../utils/logger");
const { writeToErrorLog, addDataToObject } = require("../../utils/utils");
const { saveVisitorVisitDataOnly, saveVisitMaterialsOnly, saveVisitorsToVisitLink, saveHostVisitDataOnly, prepareVisitorFastVisitForSave, sendFastVisitorAccountCredentials, getVisitByDBId, getVisitorsFromVisit, sendNotificationToUsersByVisitState, addUserAndVisitIdToVisitReqData, updateVisitDataOnly, updateVisitMaterialsOnly, resetVisitState } = require("../../utils/visitUtils");

exports.saveVisitorVisit = async (visitRequestData) => {
    const transaction = await getAndBeginTransaction();
    try {
        const visitDbId = await saveVisitorVisitDataOnly(visitRequestData, transaction);
        await saveVisitMaterialsOnly(visitDbId, visitRequestData, transaction);
        await saveVisitorsToVisitLink(visitDbId, visitRequestData, transaction);
        transaction.commit();
    } catch (error) {
        transaction.rollback();
        throw error;
    } 
}

exports.saveHostsVisit = async (visitRequestData) => { 
    const transaction = await getAndBeginTransaction();
    try {
        const visitDbId = await saveHostVisitDataOnly(visitRequestData, transaction);
        await saveVisitMaterialsOnly(visitDbId, visitRequestData, transaction);
        await saveVisitorsToVisitLink(visitDbId, visitRequestData, transaction);
        transaction.commit();
    } catch (error) {
        logger.error(`Could not create visit of ${error.message}`);
        transaction.rollback();
        throw error
    }
}

exports.saveVisitorFastVisit = async (visitRequestData) => { 
    const transaction = await getAndBeginTransaction();
    try {
        const [userId, wasExistent] = await saveUserFromVisitWhenNonExistent(visitRequestData.p_user, transaction);
        prepareVisitorFastVisitForSave(userId, visitRequestData);
        const visitDbId = await saveVisitorVisitDataOnly(visitRequestData, transaction);
        addDataToObject({p_id_x: userId, p_id: visitDbId}, visitRequestData);
        await saveVisitMaterialsOnly(visitDbId, visitRequestData, transaction);
        await saveVisitorsToVisitLink(visitDbId, visitRequestData, transaction);
        if(!wasExistent)
            await sendFastVisitorAccountCredentials(visitRequestData.p_user);

        transaction.commit();
    } catch (error) {
        logger.error(`Could not create visit of ${error.message}`);
        transaction.rollback();
        throw error;
    }
}

exports.editScheduledVisit = async (visitReqData) => {
    let colaboratorOfVisit = {};
    try {
        const dbCon = await getDatabaseConnection();
        colaboratorOfVisit = await getColaboratorById(dbCon, visitReqData.p_id_host);
    } catch (error) {
        logger.error(`Could not get colaborator of visit (${visitReqData.p_id})`);
        throw new Error('Unable to updated visit!');
    }

    const transaction = await getAndBeginTransaction();
    try {
        await updateVisitDataOnly(visitReqData, transaction);
        await resetVisitState(visitReqData, colaboratorOfVisit, transaction);
        await updateVisitMaterialsOnly(visitReqData, transaction);
        transaction.commit();
        logger.debug(`Visit ${visitReqData.p_id} has been edited by ${visitReqData.p_cod_user}`)
        logger.info(`Visit ${visitReqData.p_id} has been updated!`);
    } catch (error) {
        logger.error(`Could not edit visit ${error.message}`);
        transaction.rollback();
        throw error;
    }
}

exports.sendVisitStateNotificationToUsers = async (visitDbId) => { 
    try {
        const visit = await getVisitByDBId(visitDbId);
        const visitorsOfVisit = await getVisitorsFromVisit(visit);
        sendNotificationToUsersByVisitState(visit, visitorsOfVisit);
        
    } catch (error) {
        writeToErrorLog(error, 'visits:processes', 'sendNotificationToUsers');
    }
}