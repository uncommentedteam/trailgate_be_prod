const { getDatabaseConnection } = require("../../db/dbConnector");
const { getAndBeginTransaction } = require("../../db/dbHelpers");
const { saveNewUserFromRegistration, saveNewUserFromGrantAccess, generateUserName, addDefaultPasswordOfInternalUsers } = require("../../utils/authUtils");
const logger = require("../../utils/logger");

exports.saveNewUser = async (userReqData) => { 
    try {
        const dbCon = await getDatabaseConnection();
        userReqData.p_uname = await generateUserName(userReqData, dbCon);    
        logger.debug(`Created username for ${userReqData.p_fname} ${userReqData.p_lname} : ${userReqData.p_uname}  [VISITOR].`);
    } catch (error) {
        logger.error(`Could not create username for ${userReqData.p_fname} ${userReqData.p_lname} [VISITOR] : ${error}.`);
        throw error;
    }
   
    const transaction = await getAndBeginTransaction();
    try {
        await saveNewUserFromRegistration(userReqData, transaction);
        transaction.commit();
        logger.info(`${userReqData.p_fname} ${userReqData.p_lname} has been registered [VISITOR].`);
    } catch (error) {
        transaction.rollback();
        logger.error(`${userReqData.p_fname} ${userReqData.p_lname} could not be registered [VISITOR] : ${error}.`);
        throw error;
    }
}

exports.saveBulkNewUsers = async (usersToRegister) => { 
    const dbCon = await getDatabaseConnection();
    const usersToSkipNid = [];
    for (let userReqData of usersToRegister) { 
        try {
            userReqData.p_uname = await generateUserName(userReqData, dbCon);
            addDefaultPasswordOfInternalUsers(userReqData);
            logger.debug(`Created username for ${userReqData.p_fname} ${userReqData.p_lname} : ${userReqData.p_uname}  [VISITOR].`);            
        } catch (error) {
            logger.error(`Could not create username for ${userReqData.p_fname} ${userReqData.p_lname} [VISITOR] : ${error}.`);
            usersToSkip.push(userReqData.p_nid);
        }
    }
    const filteredUsers = usersToRegister.filter(user => !usersToSkipNid.includes(user.p_nid));
    let transaction = await getAndBeginTransaction();
    let totalRegistered = 0;  
    try {
        for (let user of filteredUsers) { 
            try {
                await saveNewUserFromRegistration(user, transaction);
                transaction.commit();
                totalRegistered++;
                logger.info(`${user.p_fname} ${user.p_lname} (${filteredUsers.indexOf(user)}) has been registered [SERVICE PROVIDER].`);
                logger.debug(`Registered ${totalRegistered} of ${filteredUsers.length}`);
                transaction = await getAndBeginTransaction();
            } catch (error) {
                transaction.rollback();
                logger.error(`${user.p_fname} ${user.p_lname} (${filteredUsers.indexOf(user)}) could not be registered [SERVICE PROVIDER] : ${error}.`);  
            }
        }
    } catch (error) {
        transaction.rollback();
        logger.error(`Could not register Group of users(${filteredUsers.length}) [SERVICE PROVIDER] : ${error}.`);
        throw error;
    }
}

exports.saveNewHostUser = async (userReqData) => { 
    const transaction = await getAndBeginTransaction();
    try {
        await saveNewUserFromGrantAccess(userReqData, transaction);
        transaction.commit();
        logger.info(`${userReqData.p_fname} ${userReqData.p_lname} has been registered [HOST].`);
    } catch (error) {
        transaction.rollback();
        logger.error(`${userReqData.p_fname} ${userReqData.p_lname} could not be registered [HOST] : ${error}.`);
        throw error;
    }
}

exports.saveBulkNewHostUsers = async (users) => { 
    let transaction = await getAndBeginTransaction();
    try {
        let totalRegistered = 0;
        for (user of users)  { 
            try {
                await saveNewUserFromGrantAccess(user, transaction);
                transaction.commit();
                totalRegistered++;
                logger.info(`${user.p_fname} ${user.p_lname} (${users.indexOf(user)}) has been registered [HOST].`);
                logger.debug(`Registered ${totalRegistered} of ${users.length}`);
                transaction = await getAndBeginTransaction();
            } catch (error) {
                transaction.rollback();
                logger.error(`${user.p_fname} ${user.p_lname} (${users.indexOf(user)}) could not be registered [HOST] : ${error}.`);  
            }
        }
        
    } catch (error) {
        transaction.rollback();
        logger.error(`Could not register Group of users(${users.length}) [HOST] : ${error}.`);
        throw error;
    }
    
}