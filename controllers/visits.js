const { validationResult } = require("express-validator");
const { executeProc } = require("../db/dbAccess");
const { writeToErrorLog, removePropertyFromObject, sendUpdate, getToday, doesArrayExist, doesVariableExist, validateArrayParameter, isEmptyValue, isEmptyObject } = require("../utils/utils");
const { unifyGroupedVisits, getColaboratorById, isUserHost, isUserThread, sendResponseError, getVisitorOrHostById, getVisitorByDocumentId, hasVisitBeenRejected, hasVisitBeenCanceled } = require("../utils/controllerHelper");
const { convertVisitFieldsToProcedureFieldsAndGroups, getVisitsStateStats } = require("../utils/responseHandlers");
const VisitorVisit = require("../model/VisitorVisit");
const HostVisit = require("../model/HostVisit");
const Notification = require('../utils/Notification');
const FastVisit = require("../model/FastVisit");
const moment = require('moment');
const ServiceProviderVisit = require("../model/ServiceProviderVisit");
const { VISIT_EXTRAS, USER_TYPES_STORE_KEY } = require("../utils/constants");
const store = require('store2');
const { saveVisitorVisit, saveHostsVisit, saveVisitorFastVisit, sendVisitStateNotificationToUsers, editScheduledVisit } = require("./processes/visits");
const { sendNotificationToUsers } = require("../utils/visitUtils");
const logger = require("../utils/logger");
const {USER} = require("../utils/constants");
const {convertDatabaseFieldstoProcedureFields} = require("../utils/responseHandlers");
const {getUserById} = require("../utils/controllerHelper");
const {RequestNotificationHelper} = require("../utils/requestNotificationHelper");
/**
 * #TODO: Comment this.
 * @param {*} lstPedidos 
 */
const setPedidos = (lstPedidos) => {
    var newLst = []
    for (let i = 0; i < lstPedidos.length; i++) {
        var element = lstPedidos[i]
        element = setPedido(element)
        var isfound = false
        //Check if empty
        if (newLst.length > 0){
            // Check inside the list to see if there is one alreadt
            for (let j = 0; j < newLst.length; j++) {
                var nl_pedido = newLst[j];
                if (nl_pedido.length){
                    for (let k = 0; k < nl_pedido.length; k++) {
                        var item = nl_pedido[k];
                        if (item.cod === element.cod && item.x_cod !== element.x_cod){
                            nl_pedido.push(element)
                            newLst[j] = nl_pedido
                            isfound = true
                            break
                        } else if (item.cod === element.cod && item.x_cod === element.x_cod){
                            isfound = true
                            break
                        }
                    }
                }else{
                    if (nl_pedido.cod === element.cod && nl_pedido.x_cod !== element.x_cod){
                        newLst[j] = ([nl_pedido,element])
                        isfound = true
                        break
                    } else if (nl_pedido.cod === element.cod && nl_pedido.x_cod === element.x_cod){
                        isfound = true
                        break
                    }
                }
            }
            if (!isfound){
                newLst.push(element)
            }

        } else {
            newLst.push(element) 
        }
    }
    return newLst
}

/**
 * #TODO: Comment this.
 * @param {*} pedido 
 */
const setPedido = (pedido) => {
    let str_dateTime = pedido['sv_visit_date']
    let date = pedido["sv_visit_date"].toLocaleDateString();
    let time = pedido["sv_visit_date"].toLocaleTimeString();
    pedido['sv_visit_date_date'] = date
    pedido['sv_visit_date_time'] = time
    pedido['sv_visit_dateTime'] = convertStringToData(str_dateTime)
    pedido['st_state'] = setState(pedido)
    return pedido
}


/**
 * #TODO: Comment this.
 * @param {*} lstVisits 
 */
const set4MobileDashBoard = (lstVisits) => {
    var preList = []
    var joinDict = {}
    // Preparing
    for (let i = 0; i < lstVisits.length; i++) {
        const visit = lstVisits[i]
        var preDict = {}
        var cod_comp
        if(visit.length){
            cod_comp = visit[0]['sv_cod_comp']
        } else {
            cod_comp = visit['sv_cod_comp']
        } 
        preDict[cod_comp] = visit
        preList.push(preDict)
    }
    logger.info(preList)
    // JONING
    for (let i = 0; i < preList.length; i++) {
        const preDict = preList[i]

        for (var key in preDict) {
            if (joinDict[key] === undefined){
                joinDict[key] = [preDict[key]]
            }else{
                var tmpLst = joinDict[key]
                var data = preDict[key]
                if (data.length){
                    tmpLst.push.apply(tmpLst, [data])
                } else {
                    tmpLst.push(data)
                }
                joinDict[key] = tmpLst
            } 
        }
    }
    return joinDict

    
}

/**
 * #TODO: Comment Here.
 * @param {*} listVisitorLeaving 
 */
const setVisitorLeaving = (listVisitorLeaving) => {
    objVL = {}
    listVisitorL = []
    for (let vLeaving of listVisitorLeaving) {
        // Check if there is an object 
        let cod_svisit = vLeaving['cod_svisit']
        if (objVL[cod_svisit]) {
            // GET THE EXISTEM LIST
            // CHECK IF ITS A LIST
            var lLeaving = objVL[cod_svisit]
            if (lLeaving.length) {

                lLeaving.push(vLeaving)
            } else {
                lLeaving = [lLeaving, vLeaving]
            }
            objVL[cod_svisit] = lLeaving
        } else {
            objVL[cod_svisit] = vLeaving
        }
    }
    // JOINING NEED DATA
    for (let pox in objVL) {
        let visitorL = objVL[pox]
        //var visitorL = objVL[key]
        if (visitorL.length) {
            var sample = visitorL[0]
            for (eachVL of visitorL) {
                // if is list 
                if (sample['cod_x'].length) {
                    sample['cod'].push(eachVL['cod'])
                    sample['cod_gate'].push(eachVL['cod_gate'])
                    sample['gate_fname'].push(eachVL['gate_fname'])
                    sample['gate_lname'].push(eachVL['gate_lname'])
                    sample['isin'].push(eachVL['isin'])
                    sample['isout'].push(eachVL['isout'])
                    sample['dtm_in'].push(eachVL['dtm_in'])
                    sample['dtm_out'].push(eachVL['dtm_out'])
                    sample['cod_obs'].push(eachVL['cod_obs'])
                    sample['obs'].push(eachVL['obs'])
                    sample['cod_x'].push(eachVL['cod_x'])
                    sample['schedule_x_nid'].push(eachVL['schedule_x_nid'])
                    sample['schedule_x_fname'].push(eachVL['schedule_x_fname'])
                    sample['schedule_x_lname'].push(eachVL['schedule_x_lname'])
                } else {
                    sample['cod'] = [eachVL['cod']]
                    sample['cod_gate'] = [eachVL['cod_gate']]
                    sample['gate_fname'] = [eachVL['gate_fname']]
                    sample['gate_lname'] = [eachVL['gate_lname']]
                    sample['isin'] = [eachVL['isin']]
                    sample['isout'] = [eachVL['isout']]
                    sample['dtm_in'] = [eachVL['dtm_in']]
                    sample['dtm_out'] = [eachVL['dtm_out']]
                    sample['cod_obs'] = [eachVL['cod_obs']]
                    sample['obs'] = [eachVL['obs']]
                    sample['cod_x'] = [eachVL['cod_x']]
                    sample['schedule_x_nid'] = [eachVL['schedule_x_nid']]
                    sample['schedule_x_fname'] = [eachVL['schedule_x_fname']]
                    sample['schedule_x_lname'] = [eachVL['schedule_x_lname']]
                }
            }
            listVisitorL.push(sample)
        } else {
            listVisitorL.push(visitorL)
        }
    }
    return listVisitorL
}

/**
 * #TODO: Comment Here.
 * @param {*} visit 
 */
const setMultiAccess = (visit) => {
    // VISITAS COLECTIVAS
    if (visit['schedule_cod_x'].length){
        var allIn
        // VERIFICAR SE ALGUEM JA ENTROU PELOMENTOS 1 VEZ
        for (let idx_scodx in visit['schedule_cod_x']){
            let scodx = visit['schedule_cod_x'][idx_scodx]
            for (let idx_vcodx in visit['visit_cod_x']){
                let vcodx = visit['visit_cod_x'][idx_vcodx]
                if (scodx === vcodx){
                    // VERIFICAR SE JA ENTROU
                    if (visit['visit_bool_isin'][idx_vcodx]){
                        visit['listOut'][idx_scodx] = false 
                        break
                    }
                }
            }
        }
        for (let j in visit['schedule_cod_x']) {
            // CHECK IF ALL IN
            for (let isOut of visit['listOut']) {
                allIn = true
                if (isOut) {
                    allIn = false
                    break
                }
            }
        }
        visit['allIn'] = allIn
    } else {
        let scodx = visit['schedule_cod_x']
        for (let idx_vcodx in visit['visit_cod_x']){
            let vcodx = visit['visit_cod_x'][idx_vcodx]
            if (scodx === vcodx){
                // VERIFICAR SE JA ENTROU
                if (visit['visit_bool_isin'][idx_vcodx]){
                    visit['allIn'] = true
                    break
                }
            }
        }
    }
    return visit
}

/**
 * #TODO: Comment Here.
 * @param {*} visit 
 */
const setAccess = (visit) => {
    if (visit['schedule_cod_x'].length) {
        var listOut = []
        for (let j in visit['schedule_cod_x']) {
            let cod_x = visit['schedule_cod_x'][j]

            for (let vPos in visit['visit_cod_x']) {
                let vCodx = visit['visit_cod_x'][vPos]
                if (vCodx === cod_x) {
                    // Verifica se esta dentro e ainda nao saiu
                    if (visit['visit_bool_isin'][vPos] && !visit['visit_bool_isout'][vPos]) {
                        // SE ESTIVER DENTRO COLOCAR NA LIST QUE ESTA DENTRO
                        listOut[j] = false
                        break
                    }
                }
                listOut[j] = true
            }
            // CHECK IF ALL IN
            for (let isOut of listOut) {
                allIn = true
                if (isOut) {
                    allIn = false
                    break
                }
            }
        }
        visit['listOut'] = listOut
        visit['allIn'] = allIn
    } else {
        let cod_x = visit['schedule_cod_x']
        for (let vPos in visit['visit_cod_x']) {
            let vCodx = visit['visit_cod_x'][vPos]
            if (vCodx === cod_x) {
                // VERIFICAR SE ESTA DENTRO E AINDA NAO SAIU
                if (visit['visit_bool_isin'][vPos] && !visit['visit_bool_isout'][vPos]) {
                    // SE ESTIVER DENTRO COLOCAR NA LIST QUE ESTA DENTRO
                    allIn = true
                    break
                }
            }
            allIn = false
        }
        visit['allIn'] = allIn
    }
    return visit
}

/**
 * #TODO: Comment Here.
 * @param {*} listVisits 
 */
const setVisitas = (listVisits) => {
    var newListVisits = []
    for (let i in listVisits) {
        let visit = listVisits[i]
        var allIn = false
        //SET GROUPED
        visit = setGroupLogic(visit)
        // SET ACCESS
        visit = setAccess(visit)
        //SET MULTI ACCESS
        if (!visit['schedule_multi_entry']){
            visit = setMultiAccess(visit)
        }
        listVisits[i] = visit
    }
    for (let visit of listVisits) {
        if (!visit['allIn']) {
            newListVisits.push(visit)
        }
    }
    logger.info('finalProduct: ', newListVisits)
    return newListVisits
}

// PEDIDOS FILTER
/**
 * #TODO: Comment Here.
 * @param {*} dataTimeString 
 */
const convertStringToData = (dataTimeString) => {
    moment.locale("PT")
    let newDate;
    newDate = moment(dataTimeString, 'YYYY-MM-DDThh:mm:ss').format('DD/MM/YYYY HH:mm')

    let dayToday = moment().format('DD/MM/YYYY').split('/')[0]
    let dayPedido = newDate.split('/')[0]
    let numDias = dayPedido - dayToday
    if (numDias >= -5 && numDias <= 6) {
        newDate = moment(newDate, 'DD/MM/YYYY hh:mm').calendar()
    } else {
        newDate = moment(newDate, 'DD/MM/YYYY hh:mm').format('LLL')
    }
    return newDate
}

/**
 * #TODO: Comment Here.
 * @param {*} listPedidos 
 */
const setListPedidos = (listPedidos) => {
    let newListPedidos = []

    //Reconhecimento
    let pox = {}
    for (let i in listPedidos) {
        let pedido = listPedidos[i]
        let codPedido = pedido['schedule_cod']

        if (pox[codPedido]) {
            var element = pox[codPedido]
            let pedido_cod_x = pedido['schedule_cod_x']
            let pedido_nid = pedido['schedule_x_nid']
            let pedido_x_fname = pedido['schedule_x_fname']
            let pedido_x_lname = pedido['schedule_x_lname']
            let pedido_deep_induction = pedido['schedule_deep_induction']
            let pedido_date_deep_induction = pedido['schedule_date_deep_induction']
            let pedido_date_deep_induction_converted = convertStringToData(pedido['schedule_date_deep_induction'])
            let pedido_induction = pedido['schedule_induction']

            if (element['schedule_cod_x'].length) {
                element['schedule_cod_x'].push(pedido_cod_x)
                element['schedule_x_fname'].push(pedido_x_fname)
                element['schedule_x_lname'].push(pedido_x_lname)
                element['schedule_x_nid'].push(pedido_nid)
                element['schedule_deep_induction'].push(pedido_deep_induction)
                element['schedule_date_deep_induction'].push(pedido_date_deep_induction)
                element['schedule_date_deep_induction_converted'].push(pedido_date_deep_induction_converted)
                element['schedule_induction'].push(pedido_induction)
            } else {
                element['schedule_cod_x'] = [element['schedule_cod_x'], pedido_cod_x]
                element['schedule_x_fname'] = [element['schedule_x_fname'], pedido_x_fname]
                element['schedule_x_lname'] = [element['schedule_x_lname'], pedido_x_lname]
                element['schedule_x_nid'] = [element['schedule_x_nid'], pedido_nid]
                element['schedule_deep_induction'] = [element['schedule_deep_induction'], pedido_deep_induction]
                element['schedule_date_deep_induction'] = [element['schedule_date_deep_induction'], pedido_date_deep_induction]
                element['schedule_date_deep_induction_converted'] = [convertStringToData(pedido['schedule_date_deep_induction']), pedido_date_deep_induction_converted]
                element['schedule_induction'] = [element['schedule_induction'], pedido_induction]
            }
            pox[codPedido] = element
        } else {
            pedido['schedule_date_deep_induction_converted'] = convertStringToData(pedido['schedule_date_deep_induction'])
            pox[codPedido] = pedido
        }
    }

    //Ajuste dos pedidos 
    for (let key in pox) {
        var pedido = pox[key]

        //Setting DateTime
        let str_dateTime = pedido['schedule_date_time']

        let date = pedido["schedule_date_time"].toLocaleDateString();
        let time = pedido["schedule_date_time"].toLocaleTimeString();
        pedido['schedule_date'] = date
        pedido['schedule_time'] = time
        pedido['schedule_dateTime'] = convertStringToData(str_dateTime)

        // SET MATERIAL
        pedido['schedule_material'] = getMaterial(pedido)

        // SET LPLATE
        pedido['schedule_lplate'] = getLPlate(pedido)

        // SET STATE
        pedido['state_state'] = setState(pedido)

        //Add to final List
        newListPedidos.push(pedido)
    }

    return newListPedidos
}

// PEDIDO FORMATER
/**
 * #TODO: Comment Here.
 * @param {*} pedido 
 */
const setState = (pedido) => {
    let host = pedido['st_host_is']
    let thread = pedido['st_threa_is']
    let core = pedido['st_core_is']
    let canceled = (pedido['st_host_is_cancel'] ||
        pedido['st_threa_is_cancel'] ||
        pedido['st_core_is_cancel'])
    // CHECK ITS REJECTED 
    if ((host === false || thread === false || core === false) && !canceled) {
        return -1
    }
    // CHECK IF ITS PENDENTE
    else if ((host === null || thread === null || core === null) && !canceled) {
        return 0
    }
    // CHECK IF IT ACCEPT
    else if ((host === true || thread === true || core === true) && !canceled) {
        return 1
    }
    // CHECK IF ITS CANCELLED 
    else {
        return 2
    }
}

/**
 * #TODO: Comment Here.
 * @param {*} pedido 
 */
const getMaterial = (pedido) => {
    if (pedido['schedule_material']) {
        let material = pedido['schedule_material']
        return material.split(',')
    }
}

/**
 * #TODO: Comment Here.
 * @param {*} pedido 
 */
const getLPlate = (pedido) => {
    if (pedido['schedule_lplate']) {
        let lplate = pedido['schedule_lplate']
        return lplate.split(',')
    }
}

/**
 * #TODO: Comment Here.
 * @param {*} visit 
 */
const setGroupLogic = (visit) => {
    var grouped
    // Check se tem grouped
    // Se tiver pode ou nao ser colectivo
    // Mas se nao tem ja e singular
    if (visit['grouped']) {
        // tem grouped
        grouped = visit['grouped']

        // verificar se e colectivo ou singular
        visit['schedule_cod_x'] = [visit['schedule_cod_x']]
        visit['schedule_x_fname'] = [visit['schedule_x_fname']]
        visit['schedule_x_lname'] = [visit['schedule_x_lname']]
        visit['schedule_x_nid'] = [visit['schedule_x_nid']]
        visit['schedule_date_deep_induction'] = [visit['schedule_date_deep_induction']]
        visit['schedule_deep_induction'] = [visit['schedule_deep_induction']]
        visit['schedule_induction'] = [visit['schedule_induction']]
        // Para contar as visitas
        visit['visit_cod_x'] = [visit['schedule_cod_x'][0]]
        visit['visit_bool_isin'] = [visit['visit_bool_isin']]
        visit['visit_bool_isout'] = [visit['visit_bool_isout']]

        for (let vInGroup of grouped) {
            if (!(visit['schedule_cod_x'].includes(vInGroup['schedule_cod_x']))) {
                visit['schedule_cod_x'].push(vInGroup['schedule_cod_x'])
                visit['schedule_x_fname'].push(vInGroup['schedule_x_fname'])
                visit['schedule_x_lname'].push(vInGroup['schedule_x_lname'])
                visit['schedule_x_nid'].push(vInGroup['schedule_x_nid'])
                visit['schedule_date_deep_induction'].push(vInGroup['schedule_date_deep_induction'])
                visit['schedule_deep_induction'].push(vInGroup['schedule_deep_induction'])
                visit['schedule_induction'].push(vInGroup['schedule_induction'])
            }
            visit['visit_cod_x'].push(vInGroup['schedule_cod_x'])
            visit['visit_bool_isin'].push(vInGroup['visit_bool_isin'])
            visit['visit_bool_isout'].push(vInGroup['visit_bool_isout'])
        }
        // Verificar se e singular ou colectiva
        if (!visit['schedule_cod_x'].length || visit['schedule_cod_x'].length < 2) { //Singular
            visit['schedule_cod_x'] = visit['schedule_cod_x'][0]
            visit['schedule_x_fname'] = visit['schedule_x_fname'][0]
            visit['schedule_x_lname'] = visit['schedule_x_lname'][0]
            visit['schedule_x_nid'] = visit['schedule_x_nid'][0]
            visit['schedule_date_deep_induction'] = visit['schedule_date_deep_induction'][0]
            visit['schedule_deep_induction'] = visit['schedule_deep_induction'][0]
            visit['schedule_induction'] = visit['schedule_induction'][0]
        }
    } else {
        // Visita singular
        //visit['visit_cod_x'] = visit['schedule_cod_x'][0]
        visit['visit_cod_x'] = [visit['schedule_cod_x']]
        visit['visit_bool_isin'] = [visit['visit_bool_isin']]
        visit['visit_bool_isout'] = [visit['visit_bool_isout']]
    }
    return visit
}



// TESTE III
/**
 * Controller to get status of the schedule visit.
 * @param {object} req 
 * @param {object} res 
 */
exports.getScheduleVisitStatusThread = async (req, res) => {
    try {
        let result = await executeProc(req.body, req.db, 'proc_get_schedule_visit_status_thread');
        var resultFormat;
        resultFormat = setListPedidos(result)
        res.json(resultFormat)
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getScheduleVisitStatusThread');
    }
}

/**
 * Controller to change the material in schedule visit.
 * @param {object} req 
 * @param {object} res 
 */
exports.changeSVisitMaterial = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_change_schedule_visit_material');
        if (req.vSocket) {
            req.vSocket.emit('update');
            req.vSocket.broadcast.emit('update')
        }
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'changeSVisitMaterial');
    }
}

// TESTE IX
/**
 * Controller to get status of the schedule visit.
 * @param {object} req 
 * @param {object} res 
 */
exports.getDepByTer = async (req, res) => {
    try {
        let result = await executeProc(req.body, req.db, 'proc_get_department_by_terminl');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getDepByTer');    
    }
}

// TESTE X
/**
 * Controller to get status of the schedule visit.
 * @param {object} req 
 * @param {object} res 
 */
exports.getUserByDepAndTer = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_user_by_department_terminal');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'regUser');
    }
}

/**
 * Controller to add visits by the host.
 * @param {object} req 
 * @param {object} res
 * @deprecated
 */
exports.submitVisitHost = async (req, res) => {
    try {
        const {
            p_id_x,
            p_id_host,
            p_id_department,
            p_id_terminal,
            p_date_time,
            p_multi_entry,
            p_material,
            p_lplate,
            p_detail,
            p_period,
            p_submited,
            p_rule,

            p_why
        } = req.body

        let visitData = {
            p_id_host,
            p_id_department,
            p_id_terminal,
            p_date_time,
            p_multi_entry,
            p_material,
            p_lplate,
            p_detail,
            p_period,
            p_submited,
            p_rule
        };
        visitData = ch.addVisitCode(visitData);
        await executeProc(visitData, req.db, 'proc_add_schedule_visit');


        //::>> Add the visit to the visit state

        let vid = await req.db.query(`SELECT IDENT_CURRENT('tbl_svisit')`); // request the last id entered
        vid = vid.recordset[0][""];
        await executeProc({ p_id_svisit: vid }, req.db, 'proc_add_visit_state');
        await executeProc({
            p_id_svisit: vid,
            p_id_host,
            p_bool_host: 1,
            p_dtm_host: p_submited,
            p_why
        }, req.db, 'proc_change_visit_state_host');
        await executeProc({ p_id_x, p_id_svisit: vid }, req.db, 'proc_add_visitor');
        
        if (req.vSocket) {
            req.vSocket.broadcast.emit('update')
            req.vSocket.emit('update');
        }
        res.json({ msg: 'Visit Submited Successfully' });

    } catch (err) {
        sendResponseError(res, error, 'visits.js', 'submitVisitHost');    }

}

/**
 * Controller to add visits by the Thread.
 * @param {object} req 
 * @param {object} res 
 * @deprecated
 */
exports.submitVisitThread = async (req, res) => {

    try {


        const {
            p_id_x,
            p_id_thread,
            p_id_department,
            p_id_terminal,
            p_date_time,
            p_multi_entry,
            p_material,
            p_lplate,
            p_detail,
            p_period,
            p_submited,
            p_rule        } = req.body;

        let visitData = {
            p_id_host: p_id_thread,
            p_id_department,
            p_id_terminal,
            p_date_time,
            p_multi_entry,
            p_material,
            p_lplate,
            p_detail,
            p_period,
            p_submited,
            p_rule
        }

        visitData = ch.addVisitCode(visitData);
        await executeProc(visitData, req.db, 'proc_add_schedule_visit');

        let vid = await req.db.query(`SELECT IDENT_CURRENT('tbl_svisit')`); // request the last id entered
        vid = vid.recordset[0][""];
        await executeProc({ p_id_svisit: vid }, req.db, 'proc_add_visit_state');
        await executeProc({ p_dtm_accept: p_submited, p_id_svisit: vid, p_dtm_thread: p_submited, p_bool_thread: 1 }, req.db, 'proc_add_visit_state_thread')
        await executeProc({ p_id_x, p_id_svisit: vid }, req.db, 'proc_add_visitor');
        res.json({ msg: 'Visit Submited Successfully' });

    } catch (err) {
        sendResponseError(res, error, 'visits.js', 'submitVisitThread');
    }

}

/**
 * Controller to add visits by the Core Security.
 * @param {object} req 
 * @param {object} res 
 * @deprecated
 */
exports.submitVisitCoreSecurity = async (req, res) => {
    try {
        const {
            p_id_x,
            p_id_coresecurity,
            p_id_department,
            p_id_terminal,
            p_date_time,
            p_material,
            p_multi_entry,
            p_lplate,
            p_detail,
            p_period,
            p_submited,
            p_rule        } = req.body

        let visitData = {
            p_id_host: p_id_coresecurity,
            p_id_department,
            p_id_terminal,
            p_multi_entry,
            p_date_time,
            p_material,
            p_lplate,
            p_detail,
            p_period,
            p_submited,
            p_rule
        };
        visitData = ch.addVisitCode(visitData)
        await executeProc(visitData, req.db, 'proc_add_schedule_visit');

        let vid = await req.db.query(`SELECT IDENT_CURRENT('tbl_svisit')`); // request the last id entered
        vid = vid.recordset[0][""];
        await executeProc({ p_id_svisit: vid }, req.db, 'proc_add_visit_state');
        await executeProc({ p_dtm_accept: p_submited, p_id_svisit: vid, p_dtm_core: p_submited, p_bool_core: 1 }, req.db, 'proc_add_visit_state_core');
        await executeProc({ p_id_x, p_id_svisit: vid }, req.db, 'proc_add_visitor');
        if (req.vSocket) {
            req.vSocket.broadcast.emit('update')
            req.vSocket.emit('update');
        }
        res.json({ msg: 'Visit Submited Successfully' });
    } catch (err) {
        sendResponseError(res, error, 'visits.js', 'submitVisitCoreSecurity');    }
}

//GET COUNT VISITS
/**
 * Endpoint to count the amout of visit that started.
 * @param {object} req 
 * @param {object} res 
 */
exports.getCountVisitIn = async (req, res) => {
    try {
        let result = await executeProc(req.body, req.db, 'proc_get_count_visit');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getCountVisitIn');
    }
}

//Mr1 Get svisit State
/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getVisitsState = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_svisit_vstates');
        const newResult = [];
        result.forEach(element => {
            newResult.push(setPedido(element))
        })
        res.json(newResult);

    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getVisitsState');    }
}

//Mr1 Get svisit State User Mini
/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getVisitsStateUserMini = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_svisit_vstate_user_mini');
        const newResult = setPedidos(result)
        res.json(newResult);

    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getVisitsStateUserMini');
    }
}

/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getVisitsStateUserMiniFiltrar = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_svisit_vstate_user_filter');
        const newResult = setPedidos(result)
        res.json(newResult);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getVisitsStateUserMiniFiltrar');
    }
}

//Mr1 Get Mobile Dashboard
/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getMobileDashboard = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_visits_in');
        var newResult = setPedidos(result)
        newResult = set4MobileDashBoard(newResult)
        res.json(newResult);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getMobileDashboard');    }
}


// Mr1 Get Data Pedido
/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getVisitInfo = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_sv_visit_info');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getVisitInfo');    }
}

/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getVisitVisitor = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_sv_visit_visitor');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getVisitVisitor');    }
}

/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getVisitExtra = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_sv_visit_extra');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getVisitExtra');    }
}

/**
 * Controller to get extras by the extra code type.
 * @param {object} req 
 * @param {object} res 
 * @deprecated
 */
exports.getExtras = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_selected_extras');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getExtras');
    }
}

/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.editVstate_state_htc= async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_edit_vstate_state_htc');
        res.json(result);

        const {p_id_svisit, p_bool_htc} = req.body
        const notification = new Notification(req.db, -0, 1)
        notification.set('visitId', p_id_svisit)
        notification.sendNotificationsOnStatusChangeMobile()
        if (p_bool_htc){
            notification.notifyUserVisitorAtGate(p_id_svisit)
        }

    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'editVstate_state_htc');    }
}

/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.editVstate_state_htc_cancel= async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_edit_vstate_state_htc_cancel');
        res.json(result);

        const {p_id_svisit} = req.body
        const notification = new Notification(req.db, -0, 1)
        notification.set('visitId', p_id_svisit)
        notification.sendNotificationsOnStatusChangeMobile()
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'editVstate_state_htc_cancel');
    }
}

/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getVisitEntrace = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_visit_entrace');
        const newResult = setPedidos(result)
        res.json(newResult);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getVisitEntrace');
    }
}


/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.getVisitOut = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_visit_out');
        const newResult = setPedidos(result)
        res.json(newResult);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getVisitOut');
    }
}


/**
 * #TODO: Comment this. [You can use the extra endpoint for this.]
 * @param {object} req 
 * @param {object} res 
 */
exports.getTerminalPort = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_companies');
        res.json(result);

    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getTerminalPort');
    }
}


/**
 * #TODO: Comment this. [THis should be moved to extras controlelrs]
 * @param {object} req 
 * @param {object} res 
 */
exports.getDepByTerm = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_dep_by_ter');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getDepByTerm');
    }
}


/**
 * #TODO: Comment this. [THis should be moved to Admin controlelrs]
 * @param {object} req 
 * @param {object} res 
 */
exports.getUsersByDepAndTerm = async (req, res) => {
    try {
        const result = await executeProc(req.body, req.db, 'proc_get_user_by_ter_and_dep');
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getUsersByDepAndTerm');
    }
}


/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.checkVisitStarted = async (req, res) => {
    try {
        const valRes = validationResult(req);
        if (!valRes.isEmpty()) {
            return res.status(400).json({ errors: valRes.errors });
        }

        const result = await executeProc(req.body, req.db, 'proc_check_is_visit_started');
        res.json(result[0]['']);

    } catch (error) {
        return res.status(500).json({ errors: [{ msg: 'Server Error' }, { root_msg: error.message }] })
    }
}


/**
 * old editPeriodoOnly => editVisitSpec
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 * @deprecated
 */
exports.editVisitSpec = async (req, res) => { 
    try {
        await executeProc(req.body, req.db, 'proc_edit_visit_periodo_only');
        sendUpdate(req);
        res.json({msg: 'Visit edited period successfully'});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'editVisitSpec');
    }
}

//::>> Cherno
/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 * @deprecated
 */
exports.addVisit = async (req, res) => {
    try {
        await executeProc(req.body, req.db, 'proc_add_visit');
        sendUpdate(req);
        res.json({msg: 'Visit added successfully'});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'addVisit');
    }
}

//::>> Cherno
/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res 
 */
exports.editVisit = async (req, res) => { 
    
    try {
        let body = removePropertyFromObject(req.body, 'p_is_entrance')
        body = removePropertyFromObject(body, 'p_period')
       
        await executeProc(body, req.db, 'proc_edit_visit');
        const { p_id_x, p_id_svisit, p_id_obs } = req.body;
        
        let visit = await executeProc({p_id_svisit,p_id_x }, req.db, 'proc_get_visit');
        visit = visit[0];
        if (body.p_id_obs) {
            const notification = new Notification(req.db, p_id_x, 1);
            notification.set('visitId', p_id_svisit);
            notification.set('userVisit', visit);
            notification.set('p_id_obs', p_id_obs);
            await notification.sendEntranceExitRejectionEmail();
        }
       
        // sendUpdate(req);

        res.json({msg: 'Visit edited successfully'});

    } catch (error) {
        return res.status(500).json({ errors: [{ msg: 'Server Error' }, { root_msg: error.message }] })
    }
}
//::>>



//::>> NOZOTROX STUFF
/**
 * Controller that searches the visit in the dababase.
 * @param {object} req 
 * @param {object} res 
 */
exports.getScheduleVisit = async (req, res) => {

    try {
        const {prod,p_dtm_start, p_dtm_end, p_is_core} = req.body;
        let body = removePropertyFromObject(req.body, "prod");
        body = removePropertyFromObject(body, "p_id_user");
        let result = await executeProc(body, req.db, 'proc_get_svisit_vstates');
        if(p_is_core) { 
            const unfinishedVisits = await executeProc({p_dtm_start, p_dtm_end}, req.db, 'proc_get_visits_in');
            result = [...result, ...unfinishedVisits];
        }
        result = unifyGroupedVisits(result);
       
        if (prod) { 
            result = convertVisitFieldsToProcedureFieldsAndGroups(result);
            result = [getVisitsStateStats(result), ...result];
        }

        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getScheduleVisit');    }
}

/**
 * Controller to save a simple or collective visit.
 * @param {object} req 
 * @param {object} res 
 */
exports.addScheVisit = async (req, res) => {
    try {

        const {type_visit} = req.body;
        
        if(type_visit === 0) { 
            await saveVisitorVisit(req.body);
        } else if (type_visit === 1) {
            await saveHostsVisit(req.body);
        } else if (type_visit === 2) { 
            await saveVisitorVisit(req.body);
        }

        sendNotificationToUsers(req.db, req.body);

        sendUpdate(req);
        res.json({msg: 'Success'});
       
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'addScheVisit');    }
}

/**
 * Controller to add a fast request made by a non registered user.
 * @param {object} req 
 * @param {object} res 
 */
exports.addVisitorFastRequest = async (req, res) => { 
    try { 

        await saveVisitorFastVisit(req.body);
       
        sendUpdate(req);
        res.json({msg: 'Success'});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'addVisitorFastRequest');
    }
}

/**
 * Controller to get the visit extras.
 * @param {object} req 
 * @param {object} res 
 */
exports.getSvisitExtra = async (req, res) => { 
    try {
        const {prod, p_ucode} = req.body;
        let body = removePropertyFromObject(req.body, "prod");
        body = removePropertyFromObject(body, "p_ucode");
        let result = await executeProc(body, req.db, 'proc_get_svisit_extras');

        if(prod) result = convertDatabaseFieldstoProcedureFields([result, p_ucode], VISIT_EXTRAS);
        res.json(result);
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getSvisitExtra');    }
}

 
/**
 * Controller to edit only the visit data (not the materials or vehicles).
 * @param {object} req 
 * @param {object} res  
 */
exports.editScheVisit = async (req, res) => { 
    try {
        if(hasVisitBeenRejected(req.body) || hasVisitBeenCanceled(req.body))
            throw new Error('Cannot edit rejected/canceled visits!');
        await editScheduledVisit(req.body);
        // await executeProc(req.body, req.db, 'proc_edit_svisit');
        sendUpdate(req);
        res.json({msg: "Visit edited!"})
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'editScheVisit');
    }
}


/**
 * Controller to edit only the visit  the materials and/or vehicles.
 * @param {object} req 
 * @param {object} res  
 */
exports.editSVisitExtra = async (req, res) => { 
    try {
        const {p_svisit, p_cod_user} = req.body;
        const today = getToday().toISOString();
        if(doesArrayExist(req.body.extraArray) && req.body.p_oname) {
            const toSend = removePropertyFromObject(req.body, 'extraArray');
            if(toSend.p_id){ 
                await executeProc({...toSend, p_dtm:today }, req.db, 'proc_edit_svextra');
            } else { 
                await executeProc({...toSend, p_svisit, p_dtm: today, p_is_active: 1 }, req.db, 'proc_add_svextra');
            }
        } else { 
            req.body.extraArray.forEach(async extra => {
                let toSend;
                if(extra.p_id || extra.p_id_e){
                    toSend = {...extra, p_id: extra.p_id_e? extra.p_id_e: extra.p_id}
                    toSend = removePropertyFromObject(toSend, 'p_id_e')
                    await executeProc({...toSend, p_svisit, p_dtm: today, p_cod_user }, req.db, 'proc_edit_svextra');
                } else { 
                    await executeProc({...extra, p_svisit, p_dtm: today, p_is_active: 1 }, req.db, 'proc_add_svextra');
                }
            });
        }
        sendUpdate(req);
        res.json({msg: 'Extra edited successfully'});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'editSVisitExtra');
    }
}

/**
 * Controller to add vist extras (material and vehicles).
 * @param {object} req 
 * @param {object} res  
 */
exports.addSvisitExtra = async (req,res) => {
    try {
        await executeProc(req.body, req.db, 'proc_add_svextra');
        sendUpdate(req);
        res.json({msg: 'Extra added successfully'});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'addSvisitExtra');
    }
}

/**
 * Controller to edit the visit extras.
 * @param {object} req 
 * @param {object} res  
 * @deprecated
 */
exports.editSvisitExtra_v2 = async (req,res) => {
    try {
        await executeProc(req.body, req.db, 'proc_edit_svextra');
        sendUpdate(req);
        res.json({msg: 'Extra added successfully'});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'editSvisitExtra_v2');
    }
}

/**
 * 
 * @param {object} req 
 * @param {object} res  
 * @deprecated
 */
exports.edit_visit_host = async (req, res) => {
    try {
        await executeProc(req.body, req.db, 'proc_edit_svisit_host');
        sendUpdate(req);
        res.json({msg: "Updated Successfully!"});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'edit_visit_host');
    }
}
    
/**
 * Controller to reset the visit aproval to  its initial state of aproval.
 * @param {object} req 
 * @param {object} res  
 */
exports.resetVisitState = async (req, res) => { 
    try {
        const {p_id_svisit, p_id_host, p_submited} = req.body;
        const user = await getColaboratorById(req.db, p_id_host);
        const today = getToday().toISOString();
        let init = 0; let stateReq; let toEqual = -1;
        if(isUserHost(user)){
            init = 0; toEqual = 0;
        } else if (isUserThread(user)){ 
            init = 1; toEqual = 1;
        } else { init = 2; toEqual = 2 }
        
        for(let i = init; i < 3; i++){
            stateReq = {
                		p_int: i,
                		p_id_svisit: p_id_svisit,
                		p_id_htc: null,
                		p_bool_htc: null,
                		p_dtm_htc: i === toEqual? p_submited :  today,
                    };   
            await executeProc(stateReq, req.db, 'proc_edit_vstate_state_htc');
        }

        sendUpdate(req);
        res.json({msg: 'Visit Reseted Successfully'});

    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'resetVisitState');}
}

/**
 * Controller to cancel the visit.
 * @param {object} req 
 * @param {object} res  
 */
exports.editVstateCanc = async (req, res) => { 
    try {
        const {p_id_x,p_id_svisit } = req.body;
        const body = removePropertyFromObject(req.body, "p_id_x");
        const visit = await executeProc({p_id_x, p_id_svisit}, req.db, 'proc_get_visit');
        if(visit[0].bool_isin) { 
            return res.status(500).json({root_msg: "Cannot Cancel Request, Visit has already started!", error: "Cannot Cancel Request, Visit has already started!"})
        }

        await executeProc(body, req.db, 'proc_edit_vstate_state_htc_cancel');
        sendVisitStateNotificationToUsers(req.body.p_id_svisit);        
        sendUpdate(req);
        res.json({msg: 'Visit edited successfully'});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'editVstateCanc');    }
}

/**
 * Controller to change the visit state of aproval.
 * @param {object} req 
 * @param {object} res  
 */
exports.editVisitState = async (req, res) => { 
    try {
        await executeProc(req.body, req.db, 'proc_edit_vstate_state_htc');
        sendVisitStateNotificationToUsers(req.body.p_id_svisit);
        sendUpdate(req);
        res.json({msg: 'Visit edited successfully'});
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'editVisitState');
    }
}

/**
 * Controller to get all visitors of a visit.
 * @param {object} req 
 * @param {object} res  
 */
exports.getSelectedVisitors = async (req, res) => { 
    try {
        const {prod} = req.body;
        const body = removePropertyFromObject(req.body, 'prod');
        const result = await executeProc(body, req.db, 'proc_get_selected_visitors');
        let toSend = [];
        if(result && result.length > 0) result.forEach(obj => toSend = [...toSend,obj.int_id_x]);
        res.json(toSend);

    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'getSelectedVisitors');
    }
}


/**
 * Controller to add visitors to a visit.
 * @param {object} req 
 * @param {object} res  
 */
exports.addRequestVisitors = async (req, res) => { 
    try {
        const { p_users, p_id_svisit } = req.body;
        let ids = await executeProc({p_svisit: p_id_svisit}, req.db, 'proc_get_selected_visitors');
        const result = await executeProc({p_svisit: p_id_svisit, p_all: 1}, req.db, 'proc_get_selected_visitors');
        let idsAll = [];
        if(result && result.length > 0) { 
            result.forEach(obj => idsAll = [...idsAll,obj.int_id_x]);
        }

        ids.forEach(async usr => { 
            if(!p_users.includes(usr.int_id_x)) { 
                //::>> DEACTIVATE THE VISITOR
                const result = await executeProc({p_id_svisit, p_id_x: usr.int_id_x}, req.db, 'proc_get_visit');
                const {int_id,int_id_x,int_id_gk,int_id_svisit,dtm,int_id_obs,bool_isin,int_id_gate,bool_is_active, ind_id_obs} = result[0];
                try {
                    await executeProc({p_id: p_id_svisit, p_id_x: usr.int_id_x, p_is_active: 0}, req.db, 'proc_edit_visitor_visit');
                    await executeProc({p_id: int_id , p_id_gk: int_id_gk ,p_id_x:usr.int_id_x , p_id_svisit,p_dtm: moment().toISOString() ,p_id_obs:ind_id_obs ,p_isin:bool_isin ,p_id_gate:int_id_gate ,p_is_active:0}, req.db, 'proc_edit_visit');
                } catch (error) {
                    writeToErrorLog(error, 'Visits.js', 'addRequestVisitors')
                }
                
            }
        })

        p_users.forEach(async id => { 
            if(!idsAll.includes(id)){
                //::>> CREATE HIM
                await executeProc({p_id_svisit, p_id_x: id, p_is_active: 1, p_induction: 1}, req.db, 'proc_add_visitor');
                await executeProc({p_id_x: id, p_id_svisit}, req.db, 'proc_add_visit');
            } else { 
                //::>> ACTIVATE HIM
                const result = await executeProc({p_id_svisit, p_id_x: id}, req.db, 'proc_get_visit');
                const {int_id,int_id_x,int_id_gk,int_id_svisit,dtm,int_id_obs,bool_isin,int_id_gate,bool_is_active, ind_id_obs} = result[0];
                await executeProc({p_id: p_id_svisit, p_id_x: id, p_is_active: 1}, req.db, 'proc_edit_visitor_visit');
                await executeProc({p_id: int_id , p_id_gk: int_id_gk ,p_id_x: id , p_id_svisit,p_dtm: moment().toISOString() ,p_id_obs:ind_id_obs ,p_isin:bool_isin ,p_id_gate:int_id_gate ,p_is_active:1}, req.db, 'proc_edit_visit');
            }
        })

        sendUpdate(req);
        res.json({msg: 'Visit added successfully'});

    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'addRequestVisitors');
    }
}

/**
 * Controller to edit the host of a visit.
 * @param {object} req 
 * @param {object} res  
 */
exports.editVisitCodHost = async (req, res) => { 
    try {
        await executeProc(req.body, req.db, 'proc_edit_svisit_cod_host');
        sendUpdate(req);
        res.json({msg: "Visit edited!"})
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'regUser');
    }
}

/**
 * #TODO: Comment this.
 * @param {object} req 
 * @param {object} res  
 */
exports.setIsDone = async (req, res) => { 
    try {
        // await executeProc(req.body, req.db, 'proc_set_visit_done');
        res.json({msg: "Visita terminada!"}) 
    } catch (error) {
        sendResponseError(res, error, 'visits.js', 'setIsDone');
    }
}

// ADD FOR PUBLIC SERVICES
exports.getTerminalWithSpecs = async (req, res) => {
    try {
        const valRes = validationResult(req);
        if (!valRes.isEmpty()) {
            return res.status(400).json({ errors: valRes.errors });
        }

        const result = await executeProc(req.body, req.db, 'proc_get_all_companies');
        res.json(result);

    } catch (error) {
        return res.status(500).json({ errors: [{ msg: 'Server Error' }, { root_msg: error.message }] })
    }
}


/* Method to notify user*/
exports.notifyUser = async (req, res) => {
    try {
        const { p_id_svisit, p_id_from } = req.body 
        const notification = new Notification(req.db, p_id_from, false) 
        let {status, users} = await notification.notifyUserVisitorAtGate(p_id_svisit)
        if (status){
            for (id of users) {
                const body = {
                    "p_id_from":p_id_from,
                    "p_id_to": id,
                    "p_id_svisit": p_id_svisit,
                    "p_dtm_when": moment().format()
                }
                await executeProc(body, req.db, 'proc_log_notification');
            }
            res.status(200).json({});
        } else {
            res.status(500).json({"msg": "Nao foi possivel notificar os usuarios"})
        }
    } catch (error) {
        logger.error(error.message);
        res.status(500).json({})
    }
}