const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file'); 

const jsonLoggerFormats = winston.format.combine(
    winston.format.timestamp({
        format: 'ddd MMM DD h:mm:ss',
    }),
    winston.format.json()
);

const jsonLogger = winston.createLogger({
    level: 'info',
    format: jsonLoggerFormats,
    transports: [
        new DailyRotateFile({
            filename: `${__dirname}/../logs/messages-json-%DATE%.log`, 
            level:'info',
            datePattern: 'YYYY-MM-DD',
            maxSize: '20m',
            maxFiles: '90d',
        }),
    ]
    }
);

jsonLogger.add(new winston.transports.Console({
    silent: false,
    level: 'debug',
    format: jsonLoggerFormats
}));

module.exports = jsonLogger; 