const sql = require('mssql');
const fs = require('fs');
const path = require('path');
const moment = require('moment')
const {writeToErrorLog, getServerFilesContent, exists, writeContentToServerFile, doesVariableExist} = require('./utils');
const { executeProc, db_config } = require('../db/dbAccess');
const store = require('store2');
const { COLABORATOR_TYPE_STR, THREAD_TYPE_STR, GATE_KEEPER_TYPE_STR, CORE_SECURITY_TYPE_STR, USER_TYPES_STORE_KEY } = require('./constants');
const { dbConState } = require('../db/dbAccess')
const Notification = require('./Notification');
const logger = require('./logger');
const visitLogger = require('./visitLogger')
/**
 * Gets the database connection.
 */
const getDb = async () => { 
    try{ 
        return await sql.connect(db_config);
    } catch (err){
        writeToErrorLog(err, "tasks.js", "getDb");
        return null
    }
}



/**
 * Gets the content of a file.
 * @param {string} fileName 
 */
const getFileContent = async (fileName) => { 
    let content = await getServerFilesContent(fileName);
    if(doesVariableExist(content)){
        return JSON.parse(content);
    } 
    return {};
}
/**
 * Writes json content to a file.
 * @param {string} fileName 
 * @param {*} content 
 */
const writeToFile = async (fileName, content) => { 
    writeContentToServerFile(fileName, JSON.stringify(content));
}

const checkVisits = async () => {
    // This was made because i dont trust fully the system to finish request, because sometimes the system can be a dick
    const past = moment().subtract(30, 'days').toISOString()
    const future = moment().add(30, 'days') .toISOString()
    
    const raw_query = `SELECT
       DISTINCT
       tsv.int_id as cod_svisit,
       tsv.dtm_date_time  as dtm_svisit,
       tsv.bool_done  as is_done,
       tsv.int_period as period,
       tv.dtm  as dtm_visit,
       tv.bool_isin as visit_isin,
       (
           CASE
               WHEN
                   (SELECT TOP 1 dtm
                    FROM tbl_log_visit tlv
                    WHERE bool_isin = 1 AND int_id = tv.int_id
                    ORDER BY dtm ASC) IS NOT NULL
                   THEN
                   (SELECT TOP 1 dtm
                    FROM tbl_log_visit tlv
                    WHERE bool_isin = 1 AND int_id = tv.int_id
                    ORDER BY dtm ASC)
               ELSE
                   NULL
               END
           ) as dtm_first_entrance
   FROM tbl_svisit tsv
            LEFT OUTER JOIN tbl_visit tv
                            ON tsv.int_id = tv.int_id_svisit AND tv.bool_is_active = 1
            INNER JOIN tbl_log_visit tlv
                       ON tlv.int_id = tv.int_id
   WHERE tsv.bool_is_ps = 0 AND tsv.dtm_date_time BETWEEN '${past}' AND '${future}'
   ORDER BY dtm_first_entrance ASC`
    
    try {
        const dbCon = await getDb();
        const dbRequest = dbCon.request();
        const result = await dbRequest.query(raw_query);
        const recordSet = result.recordset
        console.log('Checking for unfinished business!')
        for (const record of recordSet) {
            const isDone = record["is_done"] || false 
            const periodo = record["period"] 
            const id_svisit = record["cod_svisit"]
            const first_entrance = record["dtm_first_entrance"]
            const now = moment()
            const firstDtmInWithPeriod = moment(first_entrance).add(periodo + 10, 'hours')
            
            if (!isDone && periodo !== null && first_entrance !== null) {
                if (now > firstDtmInWithPeriod) {
                    const procName = 'proc_set_visit_done';
                    const procBody = {p_cod: id_svisit};
                    await executeProc(procBody , dbCon, procName)
                    const msg = `
                    #
                    Visit with id: ${record['cod_svisit']} was due
                    Period of: ${periodo}
                    Entered at: ${first_entrance}
                    Should end at: ${firstDtmInWithPeriod}
                    Ended at: ${now}
                    # 
                    `
                    visitLogger.info(msg)
                } 
            } else if (isDone && periodo !== null && first_entrance !== null) {
                if (now < firstDtmInWithPeriod) {
                    const procName = 'proc_set_visit_started';
                    const procBody = {p_cod: id_svisit};
                    await executeProc(procBody , dbCon, procName)
                    const msg = `
                    #
                    Visit with id: ${record['cod_svisit']} was due incorrectly
                    Period of: ${periodo}
                    Entered at: ${first_entrance}
                    Should end at: ${firstDtmInWithPeriod}
                    # 
                    `
                    visitLogger.info(msg)
                }
            }
        }
    } catch (error) {
        logger.error(error.message);
    }
}



/**
 *  #TODO: Comment this.
 */
exports.performVisitCheck = () => {
    let interval = 10 * 60 * 1000;
    checkVisits();
    setInterval(function() {
        console.log("I am doing my 10 minutes check");
        checkVisits();
    }, interval);
}

/**
 * Creates paths and files that do not exist but are very necessary.
 */
exports.performPathCheck = () => { 
    const multipleUserRequestFiles = path.join(__dirname,'..', 'res', 'multipleUserRequestFiles');
    const userDocumentPhotos = path.join(__dirname,'..', 'res', 'userDocumentPhotos');
    const logFile = path.join(__dirname,'..', 'logs', 'error.log');
    const publicFile = path.join(__dirname, '..', 'public', 'induction');
    const inductionFile = path.join(__dirname, '..', 'res', 'induction');

    if(!fs.existsSync(multipleUserRequestFiles)) fs.mkdirSync(multipleUserRequestFiles);
    if(!fs.existsSync(userDocumentPhotos)) fs.mkdirSync(userDocumentPhotos);
    if(!fs.existsSync(logFile)) fs.writeFileSync(logFile, '');

    const files = fs.readdirSync(publicFile);

    files.forEach(file => { 
        fs.copyFileSync(path.join(publicFile, file), path.join(inductionFile, file));
    })

}


/**
 * Gets the type of user codes from the database
 */
exports.getUserCodes = async () => { 
    try {
        
        const db = await getDb();
        const funTypes = await executeProc({p_cod_etype: 'FUN'}, db, 'proc_get_extras');
        const userTypes = {};
        let typeStr, typeCode;
        funTypes.forEach(funType => { 
            typeStr = funType.extra;
            typeCode = funType.cod;
            if (typeStr.toLowerCase() === COLABORATOR_TYPE_STR.toLowerCase()) userTypes.COLABORATOR_CODE = typeCode;
            if (typeStr.toLowerCase() === THREAD_TYPE_STR.toLowerCase()) userTypes.THREAD_CODE = typeCode;
            if (typeStr.toLowerCase() === GATE_KEEPER_TYPE_STR.toLowerCase()) userTypes.GATEKEEPER_CODE = typeCode;
            if (typeStr.toLowerCase() === CORE_SECURITY_TYPE_STR.toLowerCase()) userTypes.CORE_SECURITY_CODE = typeCode;
        })        
        store.set(USER_TYPES_STORE_KEY, userTypes);

    } catch (error) {
        writeToErrorLog(error, 'tasks.js', 'getUserCodes');
    }
    

}
