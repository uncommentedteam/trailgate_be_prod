const { execProcedureInTransaction } = require("../db/dbAccess");
const { getDbObjectIdFromResult } = require("../db/dbHelpers");
const { getUserMainData, fillDefaultUserDataIfNotExistent, getUserDetailsData, sendAccountCredentialsToUser, generateUserName } = require("./authUtils");
const { getDatabaseFieldsOfAllObjects } = require("./controllerHelper");
const { getUserPasswordRecoveryMessage, buildNotificationMessageOptions, getInductionNotificationMessage, getInductionResultNotificationMessage } = require("./NotificationMessageBuilder");
const { getToday, extractFieldsFromObject, hashString } = require("./utils");

exports.saveCompanyUserMainData = async (entityReqData, transaction) => { 
    await fillDefaultUserDataIfNotExistent(entityReqData);
    const mainUserData = getUserMainData(entityReqData);
    mainUserData.p_psw = hashString(mainUserData.p_psw);
    mainUserData.p_uname = await generateUserName(mainUserData);
    const result = await execProcedureInTransaction('proc_add_user', mainUserData, transaction);
    const userId = getDbObjectIdFromResult(result);
    return userId;
}

exports.prepareCompanyDataForSave = (companyUserDbId, entityReqData) => { 
    entityReqData.p_id_cuser = companyUserDbId;
    entityReqData.p_dmt = getToday().toISOString();
    entityReqData.p_cpsw = hashString(entityReqData.p_cpsw)
}

exports.saveCompanyData = async (entityReqData, transaction) => { 
    const companyData = getCompanyDataFromReqData(entityReqData);
    const result = await execProcedureInTransaction('proc_add_company', companyData, transaction);
    const companyId = getDbObjectIdFromResult(result);
    return companyId;
}

exports.prepareUserDetaisDataForSave = (companyId, entityReqData) => { 
    entityReqData.p_id_entity = companyId;
    entityReqData.p_id_user = entityReqData.p_id_cuser;
}

exports.saveUserDetailsData = async (entityReqData, transaction) => { 
    const userDetailsData = getUserDetailsData(entityReqData);
    await execProcedureInTransaction('proc_add_user_detail', userDetailsData, transaction);
}

exports.sendNotificationToCompanyUser = (entityReqData) => { 
    const mainUserData = getUserMainData(entityReqData);
    sendAccountCredentialsToUser(mainUserData);
}

exports.sendPasswordRecoveryLinkToUser = (user) => { 
    const notificationSender = getNotificationSender();
    const emailNdTextMessageToSend = getUserPasswordRecoveryMessage(user);
    const notificationOptions = buildNotificationMessageOptions(user, emailNdTextMessageToSend);
    notificationSender.sendNotificationToAllChannels(notificationOptions);
}

exports.sendInductionRequestNotificationToVisitor = (visitor, visit) => { 
    const notificationSender = getNotificationSender();
    const emailNdTextMessageToSend = getInductionNotificationMessage(visitor, visit);
    const notificationOptions = buildNotificationMessageOptions(visitor, emailNdTextMessageToSend);
    notificationSender.sendNotificationToAvailableChannel(notificationOptions);
}

exports.sendInductionCompletitionMessageToVisitor = (visitor, visit, inducitonResult) => { 
    const notificationSender = getNotificationSender();
    const emailNdTextMessageToSend = getInductionResultNotificationMessage(visitor, visit, inducitonResult);
    const notificationOptions = buildNotificationMessageOptions(visitor, emailNdTextMessageToSend);
    notificationSender.sendNotificationToAvailableChannel(notificationOptions);
}

exports.sendGeneralPurposeMessage = async (contactInfo, subject, message) => { 
    const notificationSender = getNotificationSender();
    const notificationOptions = buildNotificationMessageOptions(contactInfo, {emailMessage: message, textMessage: message, subject});
    await notificationSender.sendNotificationToAllChannels(notificationOptions);
} 

const getNotificationSender = () => { 
    const Notification = require("./Notification");
    return new Notification();
}


const getCompanyDataFromReqData = (entityReqData) => { 
    const companyData = extractFieldsFromObject(entityReqData, getDatabaseFieldsOfAllObjects().company);
    return companyData;
}

