const winston = require('winston');
const DailyRotateFile = require('winston-daily-rotate-file'); 

const myLoggerFormat = winston.format.combine(
    winston.format.timestamp({
        format: 'ddd MMM DD h:mm:ss',
    }),
    winston.format.printf(info => `[${info.timestamp}] ${info.level.toUpperCase()} : ${info.message} `)
);

const logger = winston.createLogger({
    level: 'info',
    format: myLoggerFormat,
    transports: [
        new DailyRotateFile({
            filename: `${__dirname}/../logs/errors-%DATE%.log`, 
            level:'error',
            datePattern: 'YYYY-MM-DD',
            maxSize: '20m',
            maxFiles: '90d',
        }),
        new DailyRotateFile({
            filename: `${__dirname}/../logs/messages-%DATE%.log`,
            datePattern: 'YYYY-MM-DD',
            maxSize: '20m',
            maxFiles: '90d',
        })
    ],
    exceptionHandlers: [
        new DailyRotateFile({
            filename: `${__dirname}/../logs/exceptions-%DATE%.log`,
            datePattern: 'YYYY-MM-DD',
            maxSize: '20m',
            maxFiles: '90d',
        })
    ]
});


if (process.env.NODE_ENV !== 'production') { 
    logger.add(new winston.transports.Console({
        silent: false,
        level: 'debug',
        format: 
            winston.format.combine(
                winston.format.colorize({level: true, colors : {info: 'bold cyan', debug: 'bold magenta', error: 'bold red'}}),
                winston.format.timestamp({
                    format: 'ddd MMM DD h:mm:ss'
                }),
                winston.format.printf(info => `[${info.timestamp}] ${info.level} : ${info.message} `)
            )
        ,
    }));
}


module.exports = logger;