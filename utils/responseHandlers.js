const { isVisitToToday } = require("./utils");
const { CANCELED, PENDING, REJECTED, ACCEPTED, CORE_SEC, HOST, THREAD, HOST_ACT, THREAD_ACT, CORE_SEC_ACT, INDUCTION, ACCEPTED_TODAY, REJECTED_TODAY, PENDING_TODAY, CANCELED_TODAY, HAS_INDUCTION, VISIT_EXTRAS, COMPANIES, EXTRAS, USER, USER_FIELDS_PROCEDURE_FIELDS_MAP, VISIT_FIELDS_PROCEDURE_FIELDS_MAP, VISIT, INDUCTION_FIELDS_PROCEDURE_FIELDS_MAP, COMPANY_FIELDS_PROCEDURE_FIELDS_MAP, COMPANY, ALERT_MESSAGE_FIELDS_PROCEDURE_FIELDS_MAP, ALERT_MESSAGE, USER_OF_TERMINAL, FULL_COMPANIES, SUBCOMPANY_FIELDS_PROCEDURE_FIELDS_MAP, SUB_COMPANIES } = require("./constants");

/**
 * Converts the Extra  field names from the database (materials and vehicles) to field names used in procedures. 
 * @param {object[]} arrayToConvert 
 */
const convertExtraFieldsToProcedureFields = arrayToConvert => { 
    const extras = arrayToConvert.map(ext => { 
        return {code: ext.cod, name: ext.extra, active: ext.active?1:0,
            p_id: ext.cod,
            p_id_etype: ext.cod_etype,
            p_name: ext.extra,
            p_active: ext.active,
        }
    }).sort((a, b) => a.name - b.name);
    return extras;
}

/**
 * Converts the companies  field names from the database (materials and vehicles) to field names used in procedures. 
 * @param {object[]} arrayToConvert
 */
const convertCompaniesFieldsToProcedureFields = (arrayToConvert) => {
    let convertedCompany = {cod: 1, name: 'Failed to load...', active: 1};
    if(arrayToConvert){
        convertedCompany = arrayToConvert.map( item => {
            if (item.cod) return {code: item.cod, name: item.extra, active: item.active };
            return {code: item.int_id, name: item.str_cname, active: item.bool_active }
        })
    }
    return convertedCompany;
}

/**
 * Converts the user object with the direct database fields to a user object with the procedure fields
 * @param {object} userWithDbFields 
 */
const convertUserFieldsToProcedureFields = (userWithDbFields) => {
	const {doesObjectExist} = require('../utils/utils');
	if(!doesObjectExist(userWithDbFields)) return;
	let convertedUserFields = {};
	const boolsToConvertToIntegers = ["istemp", "isvisitor", "isactive"];

	Object.keys(USER_FIELDS_PROCEDURE_FIELDS_MAP).forEach((key) => {
		if (boolsToConvertToIntegers.includes(key)) {
			convertedUserFields = { ...convertedUserFields, [USER_FIELDS_PROCEDURE_FIELDS_MAP[key]]: userWithDbFields[key] ? 1 : 0 };
		} else {
			convertedUserFields = { ...convertedUserFields, [USER_FIELDS_PROCEDURE_FIELDS_MAP[key]]: userWithDbFields[key] };
		}
	});

	return convertedUserFields;
};

/**
 * Converts an Colaborator of terminal (User but specific to this use) field names of database to Procedure field names.
 * @param {object[]} arrayToConvert 
 */
const convertColaboratorOfTerminalFieldsToProcedureFields = (arrayToConvert) => {
    const fRes = arrayToConvert.map( item => {
        const {cod, fname, lname}  = item;
        return {code: cod, fname, lname }
    })
    return fRes;
}

/**
 * Converts the visit object with the direct database fields to a visit object with the procedure fields
 * @param {object} visitToConvert 
 */
const convertVisitFieldsToProcedureFields = (visitToConvert) => { 
    let convertedVisit = {};
    const boolsToConvertToIntegers = ["sv_is_company", "sv_is_rule", "sv_is_multi_entry", "sv_is_fast", "sv_is_access", "sv_is_internal", "bool_done", "is_induction"];
    const boolsToMantainValue = ["sv_is_done"];
    const normalFieldsToMantainValue = ["sv_cod_comp"]
    Object.keys(VISIT_FIELDS_PROCEDURE_FIELDS_MAP).forEach(key => {
      if(boolsToConvertToIntegers.includes(key)){
        convertedVisit = {...convertedVisit, [VISIT_FIELDS_PROCEDURE_FIELDS_MAP[key]]: visitToConvert[key]? 1:0}
      } else if (boolsToMantainValue.includes(key)){
        convertedVisit = {...convertedVisit, [VISIT_FIELDS_PROCEDURE_FIELDS_MAP[key]]: visitToConvert[key] === null? null : visitToConvert[key]? 1:0}
      } else if (normalFieldsToMantainValue.includes(key)) {
        convertedVisit = {...convertedVisit, [VISIT_FIELDS_PROCEDURE_FIELDS_MAP[key]]: visitToConvert[key] }
      } else if (key === "sv_cod_rrp") { 
        convertedVisit = {...convertedVisit, [VISIT_FIELDS_PROCEDURE_FIELDS_MAP[key]]: visitToConvert[key]? visitToConvert[key][0] || visitToConvert[key] : 1}
      } else if (key === "grouped"){
        convertedVisit = {...convertedVisit, [VISIT_FIELDS_PROCEDURE_FIELDS_MAP[key]]: visitToConvert[key] || []}
      } else { 
        convertedVisit = {...convertedVisit, [VISIT_FIELDS_PROCEDURE_FIELDS_MAP[key]]: visitToConvert[key] || ""}
      }   
    }); 
    convertedVisit = {...convertedVisit, state: getVisitState(visitToConvert), states: getHostsState(visitToConvert), v_name: visitToConvert.x_fname + ' ' + visitToConvert.x_lname}
    return convertedVisit;
  }

  
/**
 * Converts the visit company object with the direct database fields to a company object with the procedure fields.
 * @param {object} companies 
 */
const convertFullCompanyFieldsToProcedureFields = (companies) => { 
  
  const boolsToConvertToInteger = ["bool_port", "bool_active", "bool_is_ps"];
  let convertedCompanies = [];
  companies.forEach(company => { 
    let convertedCompany = {};
    Object.keys(COMPANY_FIELDS_PROCEDURE_FIELDS_MAP).forEach(key => {
      if(boolsToConvertToInteger.includes(key)){
        convertedCompany = {...convertedCompany, [COMPANY_FIELDS_PROCEDURE_FIELDS_MAP[key]]: company[key]? 1:0}
      } else { 
        convertedCompany = {...convertedCompany, [COMPANY_FIELDS_PROCEDURE_FIELDS_MAP[key]]: company[key]}
      }   
    });
    convertedCompanies = [...convertedCompanies, convertedCompany];
  })

   
  return convertedCompanies;
}


/**
 * Converts the Subcompany object with the direct database fields to a company object with the procedure fields.
 * @param {object} companies 
 */
 const convertSubCompanyFieldsToProcedureFields = (subCompanies) => { 
  
  const boolsToConvertToInteger = ["bool_is_active"];
  let convertedSubCompanies = [];
  subCompanies.forEach(subComp => { 
    let convertedSubCompany = {};
    Object.keys(SUBCOMPANY_FIELDS_PROCEDURE_FIELDS_MAP).forEach(key => {
      if (boolsToConvertToInteger.includes(key)) {
        convertedSubCompany = {...convertedSubCompany, [SUBCOMPANY_FIELDS_PROCEDURE_FIELDS_MAP[key]]: subComp[key]? 1:0}
      } else { 
        convertedSubCompany = {...convertedSubCompany, [SUBCOMPANY_FIELDS_PROCEDURE_FIELDS_MAP[key]]: subComp[key]}
      }   
    });
    convertedSubCompanies = [...convertedSubCompanies, convertedSubCompany];
  })

   
  return convertedSubCompanies;
}


/**
 * Converts the visit extra object with the direct database fields to a visit extra object with the procedure fields.
 * @param {object[]} param0 
 */
const convertVisitExtraFieldsToProcedureFields = ([extras, p_ucode]) => { 
  let materials;
  let convertedVisitExtras;
  materials = extras.map( ex => { 
    convertedVisitExtras = {p_oname: ex.str_oname, p_qnt: ex.int_qnt, p_ismtrl: ex.bool_ismtrl? 1:0 , p_id: ex.int_id, p_is_active: ex.bool_is_active? 1: 0,  p_cod_user: p_ucode};
    return convertedVisitExtras;
  })
  return materials;
}

/**
* Converts the visit induciton question object with the direct database fields to a induciton question object with the procedure fields.
* @param {object} inductionQuestToConvert 
*/
const convertInductionFieldsToProcedureFields = (inductionQuestToConvert) => { 
  let newObj = {};
  const boolsToConvertToInteger = ["bool_valid"];
 
   Object.keys(INDUCTION_FIELDS_PROCEDURE_FIELDS_MAP).forEach(key => {
    if(boolsToConvertToInteger.includes(key)){
      newObj = {...newObj, [INDUCTION_FIELDS_PROCEDURE_FIELDS_MAP[key]]: inductionQuestToConvert[key]? 1:0}
    } else { 
      newObj = {...newObj, [INDUCTION_FIELDS_PROCEDURE_FIELDS_MAP[key]]: inductionQuestToConvert[key]}
    }   
  });
  
  return newObj;
}

/**
* Converts the visit alert message object with the direct database fields to a alert message object with the procedure fields.
* @param {object} alertMessageToConvert 
*/
const convertAlertMessageFieldsToProcedureFields = (alertMessageToConvert) => { 

  let convertedAlertMessage = {};
  const boolsToConvertToInteger = ['bool_active'];

  Object.keys(ALERT_MESSAGE_FIELDS_PROCEDURE_FIELDS_MAP).forEach(key => {
    if(boolsToConvertToInteger.includes(key)){
      convertedAlertMessage = {...convertedAlertMessage, [ALERT_MESSAGE_FIELDS_PROCEDURE_FIELDS_MAP[key]]: alertMessageToConvert[key]? 1:0}
    } else { 
      convertedAlertMessage = {...convertedAlertMessage, [ALERT_MESSAGE_FIELDS_PROCEDURE_FIELDS_MAP[key]]: alertMessageToConvert[key]}
    }   
  });

  return convertedAlertMessage;
}

  /**
 * Converts the object field names of the database to field names that are used in the procedures.
 * @param {object} object 
 * @param {string} entityToConvert 
 */
const convertDatabaseFieldstoProcedureFields = (object, entityToConvert) => { 
  if(entityToConvert === EXTRAS) return convertExtraFieldsToProcedureFields(object);
  if(entityToConvert === COMPANIES) return convertCompaniesFieldsToProcedureFields(object);
  if(entityToConvert === USER) return convertUserFieldsToProcedureFields(object);
  if(entityToConvert === VISIT) return convertVisitFieldsToProcedureFields(object);
  if(entityToConvert === VISIT_EXTRAS) return convertVisitExtraFieldsToProcedureFields(object);
  if(entityToConvert === INDUCTION) return convertInductionFieldsToProcedureFields(object);
  if(entityToConvert === FULL_COMPANIES) return convertFullCompanyFieldsToProcedureFields(object);
  if(entityToConvert === SUB_COMPANIES) return convertSubCompanyFieldsToProcedureFields(object);
  if(entityToConvert === ALERT_MESSAGE) return convertAlertMessageFieldsToProcedureFields(object);
  if(entityToConvert === USER_OF_TERMINAL) return convertColaboratorOfTerminalFieldsToProcedureFields(object);
  return {};
}

/**
 * Converts each visit object, and the visits that are joined with it, with the direct database fields to a visit object with the procedure fields
 * @param {object[]} visitToConvert 
 */
exports.convertVisitFieldsToProcedureFieldsAndGroups = visitsToConvert => { 
    const {doesArrayExist} = require('../utils/utils');
    let convertedVisit;
    const convertedVisits = visitsToConvert.map(visit => { 
        convertedVisit = convertDatabaseFieldstoProcedureFields(visit, VISIT);
        if(doesArrayExist(visit.grouped)) convertedVisit.grouped = visit.grouped.map(gVisit => convertDatabaseFieldstoProcedureFields(gVisit, VISIT));
        return convertedVisit;
    });
    return convertedVisits;
}

/**
 * Gets the absolute visit state (Accepted, Rejected or Pending).
 * @param {object} visit 
 */
const getVisitState = (visit) => {
    let state;
    const { st_host_is, st_host_is_cancel, st_threa_is, st_threa_is_cancel, st_core_is, st_core_is_cancel} = visit;

    if(st_core_is_cancel || st_threa_is_cancel || st_host_is_cancel){
        state = CANCELED;
    } else if (st_core_is && st_host_is && st_threa_is){
        state = ACCEPTED;
    } else if ( st_core_is === null || st_host_is === null || st_threa_is === null){ 
        state = PENDING;
    } else { 
        state = REJECTED;
    }

    if (st_core_is === false || st_host_is === false || st_threa_is === false) state = REJECTED;
    return state;
}

/**
 * Gets the visit state of each person who recives the visit. (Colaborator, Security of Terminal, Homeland Security).
 * @param {object} visit 
 */
const getHostsState = (visit) => { 
    const state = {}
    const { st_host_is, st_host_is_cancel, st_threa_is, st_threa_is_cancel, st_core_is, is_induction, dtm_deep_induction, st_core_is_cancel} = visit;

        if(st_core_is_cancel ){
            state[CORE_SEC] = CANCELED;
        } else if (st_core_is) { 
            state[CORE_SEC] = ACCEPTED;
        } else if (st_core_is === null){
            state[CORE_SEC] = PENDING;
        } else { 
            state[CORE_SEC] = REJECTED;
        }
        
        if(st_host_is_cancel ){
            state[HOST] = CANCELED;
        } else if (st_host_is) { 
            state[HOST] = ACCEPTED;
        } else if (st_host_is === null){
            state[HOST] = PENDING;
        } else { 
            state[HOST] = REJECTED;
        }

        if(st_threa_is_cancel ){
            state[THREAD] = CANCELED;
        } else if (st_threa_is) { 
            state[THREAD] = ACCEPTED;
        } else if (st_threa_is === null){
            state[THREAD] = PENDING;
        } else { 
            state[THREAD] = REJECTED;
        }

        state[HOST_ACT] = st_host_is !== null;
        state[THREAD_ACT] = st_threa_is !== null;
        state[CORE_SEC_ACT] = st_core_is !== null;
        state[INDUCTION] = is_induction === null;
        state[HAS_INDUCTION] = dtm_deep_induction? true:false
    
        return state;

}

/**
 * Gets the overall state (absolute and by users that approve the vistis) of all the visits.
 * @param {object[]} visits 
 */
exports.getVisitsStateStats = (visits) => {
    const overallState = { ALL: 0, [CANCELED]: 0, [PENDING]: 0, [REJECTED]: 0, [ACCEPTED]: 0, [ACCEPTED_TODAY]: 0, [REJECTED_TODAY]: 0, [PENDING_TODAY]: 0, [CANCELED_TODAY]: 0}
    let state;
    for(let visit of visits){
        state = visit.state;
        overallState[state] = overallState[state] + 1;
        overallState.ALL = overallState.ALL + 1;
        if(isVisitToToday(visit)){
            overallState[state + '_TODAY'] = overallState[state + '_TODAY']  + 1;
        }
    }
    return overallState;
}



exports.convertDatabaseFieldstoProcedureFields = convertDatabaseFieldstoProcedureFields;
