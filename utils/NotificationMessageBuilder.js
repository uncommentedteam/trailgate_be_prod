const { fe_server_address, nodemailer_user, twilio_sender_number, email_env, support_email } = require('../config/environment');
const { APPROVED_IND_MESSAGE, REJECTED_IND_MESSAGE } = require('./constants');
const { isUserHost } = require('./controllerHelper');
const { getMomentFromString } = require('./utils');

const extractUserNames = (user) => { 
    const firstName = extractFirstName(user);
    const lastName = extractLastName(user);
    return {firstName, lastName};
}

const extractUserAccountNameNdPassword = (user) => { 
    let accountUName = extractUserEmail(user);
    if(!user.p_isvisitor)
        accountUName = extractUserName(user);

    const pnumber = extractUserNumber1(user);
    const password = extractUserPassword(user);
    return {accountName: accountUName || pnumber, password}
}

exports.getNewVisitorAccountVerificationMessage = (newVisitor) => { 
    //const verificationLink = `${fe_server_address}/accountVer/${newVisitor.p_tokken}`;
    const {accountName, _} = extractUserAccountNameNdPassword(newVisitor);
    const emailMessage =
        `<h2>Conta de Accesso a Port Gateway Criada</h2>
        <h4>Credenciais: </h4>
        <p><b>Username: ${accountName}</b></p>
        <b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b>
`;

    const textMessage =
        `PORT GATEWAY: CONTA CRIADA.\n
        Credenciais de acesso:\n 
        Username: ${accountName}\n
        PORT GATEWAY: ${fe_server_address}`;

    return {emailMessage, textMessage, subject: "PORT GATEWAY: Conta de Acesso"}
}

exports.getVisitorGuesAccountCredentialsNotificationMessage = (visitorGuest) => { 
    const {accountName, password} = extractUserAccountNameNdPassword(visitorGuest);
    const emailMessage =
     `<h2>Conta de Accesso a Port Gateway Criada</h2>
        <h4>Credenciais: </h4>
        <p>Accesse ao aplicativo usando este credencial. Posteriormente pode alterar os seus dados no menu de usuario.</p>
        <p><b>Username: ${accountName}</b></p>
        <p><b>Password: ${password}</b></p>
        
        <b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b>`

    const textMessage =  `PORT GATEWAY: CONTA CRIADA.\n
        Credenciais de acesso:\n 
        Username: ${accountName}\n
        Password: ${password}\n
        PORT GATEWAY: ${fe_server_address}`;

    return {emailMessage, textMessage, subject:"PORT GATEWAY: Conta de Acesso"}
}


exports.getInductionNotificationMessage = (visitor, visit) => { 
    const userTokken = extractUserTokken(visitor);
    const visitId = extractVisitId(visit);
    const emailMessage = 
    ` <body>     
        <h1>Pedido de Inducao</h1>
        <br>
        ${composeVisitData(visit)}
        <p>Accesse o link para fazer a inducao ${fe_server_address + "/" + "induction" + "/" + userTokken + "_" + visitId}</p>
     </body>
    `

    const textMessage = `PEDIDO DE INDUÇÃO
        Para a aprovacao da visita, por favor dirija-se ao porto para a inducao.
    `

    return {emailMessage, textMessage, subject: 'Pedido de Indução'};
}

exports.getInductionResultNotificationMessage = (visitor, visit, inductionState) => { 
    const state = inductionState? APPROVED_IND_MESSAGE:REJECTED_IND_MESSAGE;
    const stateColor = inductionState? '#218838' : '#DC3545';
    const emailMessage = 
    ` <body>     
        <h1>Resultado da Indução</h1>
        <br>
        ${composeVisitData(visit)}
        <p>Resutado: <span style='color:${stateColor}'>${state}<span></p>
     </body>
     <br>
     <p><a href="${fe_server_address}">Clique aqui para acessar o Port Gateway.</a></p>
    `

    const textMessage = `RESULTADO DA INDUÇÃO
        Resultado da indução: ${state}\n
        PORT GATEWAY: ${fe_server_address}.
    `
    return {emailMessage, textMessage, subject: 'Resultado da Indução'};
}

exports.getUserPasswordRecoveryMessage = (user) => { 
    const userTokken = extractUserTokken(user);
    const emailMessage = 
     `<body>     
        <h1>Recuperação da Palavra-Passe</h1>
        <p>Accesse o link: ${fe_server_address}/recoverPassword/${userTokken} </p>
    </body>
    `

    const textMessage = `RECUPERAÇÃO DE PASSOWRD\n
        Accesse o link: ${fe_server_address}/recoverPassword/${userTokken} para recuperar o password.
    `
    return {emailMessage, textMessage, subject: "Recuperação de Password"}
}


exports.getVisitorGuestNotificationMessage = (hostVisitor, guestVisistor) => { 
    const {firstName, lastName} = extractUserNames(hostVisitor);
    const gName = extractUserNames(guestVisistor);
    const gFirstName = gName.firstName;
    const gLastName = gName.lastName;
    const emailMessage = `
        <body>     
            <h1>Nova Requisição Para Acompanhante</h1>
            <p>Requisição criada por ${firstName + ' ' + lastName}.</p>
            <p>${gFirstName} ${gLastName} foi selecionado como acompanhante.</p>
            <a href="${fe_server_address}">Clique aqui para ver.</a>
        </body>
    `;
    const textMessage = `Nova requisição criada por ${firstName + ' ' + lastName}.\n
    ${gFirstName} ${gLastName} foi selecionado como acompanhante.\n
    Verifique o aplicativo. \n
    PORT GATEWAY: ${fe_server_address}`;
    return {emailMessage, textMessage, subject: 'Nova Requisição'};
}

exports.getNewVisitForHostNotificationMessage = (visitor) => {
    const {firstName, lastName} = extractUserNames(visitor);
    const emailMessage = ` 
        <body>     
            <h1>Nova Requisicao</h1>
            <p>${firstName + ' ' + lastName} enviou uma nova requisicao</p>
            <a href="${fe_server_address}">Clique aqui para ver.</a>
        </body>`;
    
    const textMessage = `
        Recebeu uma nova requisição de ${firstName + ' ' + lastName}.
        Verifique o aplicativo.\n
        PORT GATEWAY: ${fe_server_address}`
    
    return {emailMessage, textMessage, subject: 'Nova Requisição'};
}

exports.getVisitCanceledNotification = (visit) => { 
    const emailMessage = `
        <h1>A sua visita foi <b>cancelada</b></h1>
        <p><b>Razão:</b> ${visit.st_why_cencel}</p>

        ${composeVisitData(visit)}
        
        <br>
        <p><a href="${fe_server_address}">Clique aqui para acessar o Port Gateway.</a></p>
        `;

    const textMessage = `
        A sua visita foi cancelada.\n 
        Motivo: ${visit.st_why_cencel}\n
        Anfitriao: ${visit.host_fname + " " + visit.host_lname}\n
        PORT GATEWAY: ${fe_server_address}
    `;

    return {emailMessage, textMessage, subject: 'Requisição Cancelada'};
}


exports.getVisitRejectedNotification = (visit) => { 
    const emailMessage = `
        <h1>A sua visita foi <b>rejeitada</b></h1>
        <p><b>Razão:</b> ${visit.st_why}</p>    
        ${composeVisitData(visit)}
        
        <br>
        <p><a href="${fe_server_address}">Clique aqui para acessar o Port Gateway.</a></p>
        `;

    const textMessage = `
        A sua visita foi rejeitada.\n 
        Motivo: ${visit.st_whyl}\n
        Anfitriao: ${visit.host_fname + " " + visit.host_lname}\n
        PORT GATEWAY: ${fe_server_address}
    `;

    return {emailMessage, textMessage, subject: 'Requisição Rejeitada'};
}

exports.getVisitAcceptedNotification = (visit) => { 
    const visitAccessCode = extractVisitAccessCode(visit);

    const emailMessage = ` 
        <h1>A sua visita foi <b>aprovada</b>.</h1>
        <h2><b>CODIGO DA VISITA</b>: <u>${visitAccessCode}</u></h2>
        <small>Forneca este codigo ao se fazer ao portao do Porto de Maputo</small>

        ${composeVisitData(visit)}
        
        <br>
        <p><a href="${fe_server_address}">Clique aqui para acessar o Port Gateway.</a></p>
        `;
    
    const textMessage = `A sua visita foi aprovada.\n 
        Codigo de Acesso da visita: ${visitAccessCode}.\n
        Data: ${getMomentFromString(visit.p_date_time).format('ddd, MMM Do HH:mm')}\n
        PORT GATEWAY: ${fe_server_address}`;

    
    return {emailMessage, textMessage, subject: 'Requisição Aprovada'}
}




exports.buildNotificationMessageOptions = (destinationUser, notificationOptions) => { 
    const {textMessage, emailMessage, subject} = notificationOptions
    const destinationNumber = extractUserNumber1(destinationUser);
    const destinationEmail = extractUserEmail(destinationUser);

    const messageOptions = {
        body: textMessage,
        from: twilio_sender_number,
        to: destinationNumber
    }

    const emailOptions = {
        from: nodemailer_user,
        to: +email_env? [destinationEmail] : [support_email],
        subject: subject,
        html: emailMessage,
    }

    return {messageOptions, emailOptions};
}


const composeVisitData = (visit) => { 
    const hostName = extractHostNameInVisit(visit);
    const visitDateTime = extractVisitDateTImeInVisit(visit);
    const visitDetails = extractVisitDetailsInVisit(visit);
    const visitDataMessage = `
    =====
    <h3><b>Dados da visita:</b></h3>
    <p>     <b>Anfitriao:</b> ${hostName}</p>
    <p>     <b>Data:</b> ${getMomentFromString(visitDateTime).format('ddd, MMM Do hh:mm')}</p>
    <p>     <b>Motivo:</b> ${visitDetails}</p>
    <p><b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b></p>
    ====
    `
    return visitDataMessage;
}

const extractUserPassword = (userData) => { 
    return userData.p_psw;
}

const extractUserEmail = (userData) => { 
    return userData.p_email || userData.email;
}

const extractUserName = (userData) => { 
    return userData.p_uname || userData.uname;
}

const extractUserTokken = (userData) => { 
    return userData.p_tokken || userData.tokken;
}

const extractUserNumber1 = (userData) => { 
    return userData.p_pnumber_1 || userData.pnumber_1;
}

const extractUserID = (userData) => { 
    return userData.p_nid || userData.nid;
}

const extractFirstName = (userData) => { 
    return userData.p_fname || userData.fname;
}

const extractLastName = (userData) => {
    return userData.p_lname || userData.lname;
}

const extractVisitId = (visit) => { 
    return visit.p_id || visit.cod || visit.code;
}

const extractHostNameInVisit = visit => { 
    const fname = visit.p_fname || visit.host_fname || visit.st_host_fname;
    const lname = visit.p_lname || visit.host_lname || visit.st_host_lname;
    return `${fname} ${lname}`
}

const extractVisitDateTImeInVisit = visit => { 
    const dateTime = visit.p_date_time || visit.sv_visit_date;
    return dateTime;
}

const extractVisitDetailsInVisit = visit => { 
    const visitDetails = visit.p_detail || visit.sv_detail || '-';
    return visitDetails;
}

const extractVisitAccessCode = visit => { 
    return visit.p_code || visit.sv_code;
}


exports.extractFirstName = extractFirstName;
exports.extractLastName = extractLastName;
exports.extractUserEmail = extractUserEmail;
exports.extractUserNumber1 = extractUserNumber1;
exports.extractUserPassword = extractUserPassword;
exports.extractUserID = extractUserID;
exports.extractUserTokken = extractUserTokken;