exports.CANCELED = 'CANCELED';
exports.PENDING = 'PENDING';
exports.REJECTED = 'REJECTED';
exports.ACCEPTED = 'ACCEPTED';
exports.ALL = 'ALL';
exports.HOST = 'HOST';
exports.THREAD = 'TRHEAD';
exports.CORE_SEC = 'CORE_SEC';
exports.HOST_ACT = 'HOST_ACT';
exports.THREAD_ACT = 'THREAD_ACT';
exports.CORE_SEC_ACT = 'CORE_SEC_ACT';
exports.INDUCTION = 'INDUCTION';
exports.ACCEPTED_TODAY = 'ACCEPTED_TODAY';
exports.REJECTED_TODAY = 'REJECTED_TODAY';
exports.PENDING_TODAY = 'PENDING_TODAY';
exports.CANCELED_TODAY = 'CANCELED_TODAY';
exports.HAPPENING = 'HAPPENING';
exports.VISITORS = 'VISITORS';
exports.HAS_INDUCTION = 'HAS_INDUCTION';
exports.MAX_ERR_LOG_SIZE_IN_MB = 500;
exports.VISIT_EXTRAS = 'VISIT_EXTRAS';
exports.EXTRAS = 'EXTRAS';
exports.COMPANIES = 'COMPANIES';
exports.FULL_COMPANIES = 'FULL_COMPANIES';
exports.SUB_COMPANIES = 'SUB_COMPANIES';
exports.USER = 'USER';
exports.USER_OF_TERMINAL = 'USER_OF_TERMINAL';
exports.VISIT = 'VISIT';
exports.COMPANY = 'COMPANY';
exports.ALERT_MESSAGE = 'ALERT_MESSAGE';
exports.DEFAULT_CONTRY = 'Mozambique';
exports.DEFAULT_DOCUMENT_PHOTO_FILE = 'new_file.jpg';
exports.DEFAULT_INDUCTION_PHOTO_FILE = 'new_file.jpg';
exports.DEFAULT_STATIC_PUBLIC_DIR = 'public';
exports.COLABORATOR_TYPE_STR = 'Colaborador';
exports.THREAD_TYPE_STR = 'Oficial de segurança';
exports.CORE_SECURITY_TYPE_STR = 'Gestor do departamento de segurança interna';
exports.GATE_KEEPER_TYPE_STR = 'Agente de segurança do portão';
exports.USER_TYPES_STORE_KEY = 'USER_TYPES_STORE_KEY';
exports.INDUCTION_TYPE = 'TDI';
exports.PROVENANCE_TYPE = "PVC";
exports.RPP_TYPE = 'RPP';
exports.GATE_TYPE = 'GAT';
exports.DEPARTMENT_TYPE = 'DPT';
exports.TERMINAL_TYPE = 'TRL';
exports.ID_TYPE = 'TID';
exports.MRE_TYPE = 'MRE';
exports.MRP_TYPE = 'MRP';
exports.MRS_TYPE = 'MRS';
exports.TIN_TYPE = 'TDI';
exports.APPROVED_IND_MESSAGE = 'APROVADO';
exports.REJECTED_IND_MESSAGE = 'REPROVADO';
exports.WINDOWS_UA_STRING = 'Windows';
exports.MACOS_UA_STRING = 'Macintosh'


exports.USER_FIELDS_PROCEDURE_FIELDS_MAP = {
    cod: "p_id",
    fname: "p_fname",
    lname: "p_lname",
    uname: "p_uname",
    nid: "p_nid",
    psw: "p_psw",
    cod_provenance: "p_id_provenance",
    email: "p_email",
    pnumber_1: "p_pnumber_1",
    pnumber_2: "p_pnumber_2",
    nationality: "p_nationality",
    cod_type_id: "p_id_type_id",
    exp_date_id: "p_expiration_date_id",
    company: "company",
    department: "department",
    entity: "entity",
    cod_role: "p_id_role",
    cod_department: "p_id_department",
    cod_company: "p_id_company",
    istemp: "p_istemp",
    isactive: "p_active",
    isvisitor: "p_isvisitor",
    dtm_registry: "p_registry",
    tokken: "p_tokken",
    cod_entity:"p_id_entity",
    is_blocked:'p_isblocked',
    blocked_why: 'p_why',
    blocked_when:'p_when',
    int_id_who_blocked: 'p_id_who',
    int_id_obs: 'int_id_obs',
    str_name: "observation",
    int_id_svisit: 'int_id_svisit',
    sv_is_ps: 'sv_is_ps',
    isnotification: 'p_isnotification'
};


exports.VISIT_FIELDS_PROCEDURE_FIELDS_MAP = {cod: "p_id",
    st_host_cod: "p_id_host",
    sv_cod_dep: "p_id_department",
    sv_visit_date: "p_date_time",
    sv_cod_comp: "p_id_company",
    nid: "p_nid",
    sv_detail: "p_detail",
    sv_submited_date: "p_submited",
    sv_is_rule: "p_rule",
    sv_period: "p_period",
    sv_is_multi_entry: "p_multi_entry",
    sv_is_access: "p_access",
    sv_code: "p_code",
    sv_is_company: "p_tocompany",
    sv_is_fast: "p_fast",
    sv_cod_gate: "p_id_gate",
    sv_cod_rrp: "p_id_rpp",
    to_edit: "to_edit",
    x_cod: "p_id_x",
    sv_company: "terminal",
    sv_sub_company: "subTerminal",
    sv_department: "department",
    sv_period: "p_period",
    sv_is_internal: "p_internal",
    sv_is_multi_entry: "p_multi_entry",
    sv_gate: "gate",
    st_host_fname: "host_fname",
    st_host_lname: "host_lname",
    provenance: "provenance",
    grouped: 'grouped',
    st_host_dtm: 'st_host_dtm',
    st_threa_dtm: 'st_threa_dtm',
    st_core_dtm: 'st_core_dtm',
    st_why_cencel: 'st_why_cencel',
    is_induction: 'is_induction',
    dtm_deep_induction: 'dtm_deep_induction',
    st_why: 'st_why',
    x_fname: 'x_fname', 
    x_lname: 'x_lname',
    int_id_why: 'p_cod_why',
    sv_code: 'p_code',
    sv_is_done: 'sv_is_done',
    sv_is_ps: 'sv_is_ps',
   
};

exports.INDUCTION_FIELDS_PROCEDURE_FIELDS_MAP = {
    int_id: "p_cod",
    str_question: "p_question",
    str_answer: "p_answer",
    str_options: "p_options",
    id_extra: "p_cod_extra",
    bool_valid: "p_is_valid",
    dtm_created: "p_created",
    points: "p_points",
   }

exports.COMPANY_FIELDS_PROCEDURE_FIELDS_MAP = {
    int_id: "p_id",
    str_cname: "p_cname",
    str_email: "p_cemail",
    str_pnumber: "p_cpnumber_1",
    str_pnumber_2: "p_cpnumber_2",
    str_psw: "p_cpsw",
    bool_port:"p_cport",
    bool_active:"p_cactive",
    int_id_user: "p_id_user",
    bool_is_ps: "p_is_ps"
   }

exports.SUBCOMPANY_FIELDS_PROCEDURE_FIELDS_MAP = { 
    str_scname: "p_scname",
    int_id: "p_id",
    int_id_company: "p_id_company",
    bool_is_active: "p_is_active"
}

exports.ALERT_MESSAGE_FIELDS_PROCEDURE_FIELDS_MAP = {
    int_id: "p_id",
    str_msg: "p_msg",
    str_title: "p_title",
    dtm_start: "p_start",
    dtm_end: "p_end",
    bool_active: "p_active",
  }