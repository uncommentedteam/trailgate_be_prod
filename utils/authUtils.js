const { getDatabaseFieldsOfAllObjects, getUserById } = require("./controllerHelper");
const { extractFieldsFromObject, isEmptyObject, isEmptyValue, isNotEmptyValue, isNotEmptyObject, hashString } = require("./utils");
const randomString = require("randomstring");
const config = require("config");
const {isNotEmptyArray, convertExcelSerialDateToDateTimeString} = require("./utils");
const { execProcedureInTransaction, executeProc } = require("../db/dbAccess");
const { getDbObjectIdFromResult } = require("../db/dbHelpers");
const { PROVENANCE_TYPE } = require("./constants");
const { extractUserEmail,extractUserNumber1, extractUserID, getNewVisitorAccountVerificationMessage, buildNotificationMessageOptions, getVisitorGuesAccountCredentialsNotificationMessage } = require("./NotificationMessageBuilder");
const { getDatabaseConnection } = require("../db/dbConnector");
const moment = require("moment");
exports.saveUserFromVisitWhenNonExistent = async (userReqData, transaction) => {
    let userDbId = -1;
    let wasExistent = false;
    const userExists = await isUserExistent(userReqData, transaction);
    if (userExists) { 
        userDbId = await getExistentUserDbIdByUniqueData(userReqData, transaction);
        userDbId = userDbId.int_id;
        wasExistent = true;
    } else { 
        // generateVisitorPassword(userReqData); :: Temporary Modification 19.10.2021
        addDefaultPasswordOfInternalUsers(userReqData);
        userDbId = await saveUserData({...userReqData, p_psw: getUserHashedPassword(userReqData)}, transaction);
    }
    return [userDbId, wasExistent];
}

exports.saveNewUserFromRegistration = async (userReqData, transaction) => { 
    await verifyUserProvenance(userReqData, transaction);
    userReqData.p_psw = getUserHashedPassword(userReqData)
    userReqData.p_active = 1
    await saveUserData(userReqData, transaction);
    if(process.env.NODE_ENV !== 'test')
        sendAccountVerificationEmail(userReqData);
}

const generateUserName = async (userData, databaseCon={}) => { 
    // if its visitor username =  fname +'_'+ lname 
    // if its not visitor username = fname +'.'+ lname 
    // visitor Duplicated username =  fname +'_'+ lname +'_'+ numb
    // not visitor Duplicated username =  fname +'_'+ lname +'_'+ numb
    dbCon = databaseCon;
    if(isEmptyObject(databaseCon)) { 
        dbCon = await getDatabaseConnection();
    }
    let userName = ''
    if (userData.p_isvisitor) {
        userName = userData.p_fname.toLowerCase() + '_' + userData.p_lname.toLowerCase()
    } else {
        userName = userData.p_fname.toLowerCase() + '.' + userData.p_lname.toLowerCase()
    }
    let result = await executeProc({p_uname: userName}, dbCon,'proc_get_user_by_uname');
    if (isNotEmptyArray(result)) {
        if (userData.p_isvisitor) {
            userName +=  '_' + result.length
        } else {
            userName +=  '_' + result.length
        }
    }
    return userName.toLowerCase();
}

const generateVisitorPassword = (visitor) => { 
    visitor.p_psw = randomString.generate({length: 8});
}

getUserHashedPassword = (userReqData) => { 
    return hashString(userReqData.p_psw);
}

exports.saveNewUserFromGrantAccess = async (userReqData, transaction) => { 
    addDefaultPasswordOfInternalUsers(userReqData);
    const unhashedPassword = userReqData.p_psw;
    userReqData.p_psw = getUserHashedPassword(userReqData);
    await saveUserData(userReqData, transaction);
    if(process.env.NODE_ENV !== 'test')
        sendAccountCredentialsToUser({...userReqData, p_psw: unhashedPassword});
}

const addDefaultPasswordOfInternalUsers = (userReqData) => { 
    userReqData.p_psw = config.get("auth.internalUsersDefPassword");
}

const sendAccountVerificationEmail = (userReqData) => { 
    const notificationSender = getNotificationSender();
    const emailNdTextMessageToSend = getNewVisitorAccountVerificationMessage(userReqData);
    const notificationOptions = buildNotificationMessageOptions(userReqData, emailNdTextMessageToSend);
    notificationSender.sendNotificationToAllChannels(notificationOptions);
}

const verifyUserProvenance = async (userReqData, transaction) => { 
    const provenanceId = await getUserProvenanceId(userReqData, transaction);
    userReqData.p_id_provenance = provenanceId;
}

const getUserProvenanceId = async (userReqData, transaction) => { 
    let provenanceId = userReqData.p_id_provenance;
    const newProvenance = userReqData.provenance;
    if(isNotEmptyValue(newProvenance))  
        provenanceId = await getProvenanceIdByName(newProvenance);

    if(isEmptyValue(provenanceId) && isNotEmptyValue(newProvenance))
        provenanceId = await saveNewProvenance(newProvenance, transaction);
    
    return provenanceId;
}

const getProvenanceIdByName = async (provenanceName) => { 
    const provenance = await getProvenanceByName(provenanceName);
    let provId;
    if(isNotEmptyObject(provenance))
        provId = provenance.int_id;

    return provId;
}

const getProvenanceByName = async (provenanceName) => {
    const dbCon = await getDatabaseConnection(); 
    const result = await dbCon.query(`SELECT * FROM tbl_extra WHERE str_name LIKE '${provenanceName}'`);
    return result.recordset[0];
}

const saveNewProvenance = async (newProvenance, transaction) => { 
    const provenanceData = {p_id_etype: PROVENANCE_TYPE, p_name: newProvenance, p_active: 1}
    let result =  await execProcedureInTransaction('proc_add_extra', provenanceData, transaction);
    provenanceId = result.recordset[0].int_id;
    return provenanceId;
}



const saveUserData = async (userReqData, transaction) => {
    try {
        await fillDefaultUserDataIfNotExistent(userReqData);
        const mainUserData = getUserMainData(userReqData);
        const detailsUserData = getUserDetailsData(userReqData);
        
        if ((typeof mainUserData.p_expiration_date_id) === 'number')
            mainUserData.p_expiration_date_id = convertExcelSerialDateToDateTimeString(userOSType, colValue);
        else
            mainUserData.p_expiration_date_id = mainUserData.p_expiration_date_id.split('/').length > 1 ?
                moment(mainUserData.p_expiration_date_id, "DD/MM/YYYY").format() : "01/01/2100"
    
        const result = await execProcedureInTransaction('proc_add_user', mainUserData, transaction);
        const userDbId = getDbObjectIdFromResult(result);

        linkUserDetailsDataToUser(userDbId, detailsUserData);
        await execProcedureInTransaction('proc_add_user_detail', detailsUserData, transaction);
        return userDbId;
    } catch (error) {
        console.log(error);
        throw new Error(error.message);
    } 
}


exports.getUserByUniqueData = async (userReqData, dbConnection) => { 
    let result;
    const p_email = extractUserEmail(userReqData);
    const p_pnumber_1 = extractUserNumber1(userReqData);
    const p_nid = extractUserID(userReqData);
    const procedureBody = {p_email, p_pnumber_1, p_nid}
    result = await executeProc(procedureBody, dbConnection, 'proc_get_user_existence');
    if(isNotEmptyValue(result[0])) { 
        const userId = result[0].int_id;
        result = await getUserById(dbConnection, userId);
    } else { 
        result = {}
    }
    return result;
}

const isUserExistent = async (userReqData, transaction) => { 
    const dbUser = await getExistentUserDbIdByUniqueData(userReqData, transaction);
    return isNotEmptyValue(dbUser);
}

const fillDefaultUserDataIfNotExistent = async (userData, dbCon={}) => {
    userData.p_active = 1; 
    userData.p_tokken = userData.p_tokken || generateUserTokken();
    userData.p_psw = userData.p_psw || hashString(randomString.generate({length: +config.get("others.visitCodeSize")}));
    userData.p_uname = userData.p_uname || await generateUserName(userData, dbCon);
}

const generateUserTokken = () => { 
    return randomString.generate();
}

const getUserMainData = (userData) => {
    return extractFieldsFromObject(userData, getDatabaseFieldsOfAllObjects().user);
}

const getUserDetailsData = (userData) => { 
    return extractFieldsFromObject(userData, getDatabaseFieldsOfAllObjects().userDetails);
}

const linkUserDetailsDataToUser = (userDbId, userDetailsData) => { 
    userDetailsData.p_id_user = userDbId;
}

const getUserDbIdFromRequestData = async (userReqData, transaction) => { 
    const user = await getUserByDocumentId(userReqData.p_nid, transaction);
    return user.cod;
}

const getExistentUserDbIdByUniqueData = async (userReqData, transaction) => { 
    const p_email = extractUserEmail(userReqData);
    const p_pnumber_1 = extractUserNumber1(userReqData);
    const p_nid = extractUserID(userReqData);
    const procedureBody = {p_email, p_pnumber_1, p_nid}
    const result = await execProcedureInTransaction('proc_get_user_existence', procedureBody, transaction);
    return result.recordset[0];
}

const getUserByDocumentId = async (userDocumentNumber, transaction) => { 
    const reqData = {p_nid: userDocumentNumber, p_isvisitor: 1};
    let result = [];
    result = await execProcedureInTransaction('proc_get_user_by_nid', reqData, transaction);
    if(isEmptyObject(result.recordset[0])) {
        reqData.p_isvisitor = 0;
        result = await execProcedureInTransaction('proc_get_user_by_nid', reqData, transaction);
    }
    return result.recordset[0];
}

const sendAccountCredentialsToUser = (userData) => { 
    const notificationSender = getNotificationSender();
    const emailNdTextMessageToSend = getVisitorGuesAccountCredentialsNotificationMessage(userData);
    const notificationOptions = buildNotificationMessageOptions(userData, emailNdTextMessageToSend);
    notificationSender.sendNotificationToAllChannels(notificationOptions);
}

const getNotificationSender = () => { 
    const Notification = require("./Notification");
    return new Notification();
}

exports.resendAccountVerificationIfNecessary = user => { 
    if(toSendVerificationTo(user)) 
        sendAccountVerificationEmail(user);
}

const toSendVerificationTo = (user) => { 
    return !user.p_active && !user.p_isblocked;
}

exports.isUserNotActivated = (user) => { 
    return !user.p_active;
}

exports.isUserBlocked = (user) => { 
    return user.p_isblocked;
}

exports.isUserAServiceProvider = (user) => { 
    return user.p_id_entity && user.p_isvisitor;
}

exports.hasNotFoundUser = (user) => { 
    return isEmptyObject(user);
}

exports.addRepresentativeOfServiceFlag = async (user) => { 
    const dbCon = await getDatabaseConnection();
    const result = await executeProc({p_cod: user.p_id_entity}, dbCon, "proc_get_all_or_selected_by_cod_company");
    const company = result[0];
    user.is_rep = isUserRepresentativeOfCompany(user, company);
}

const isUserRepresentativeOfCompany = (user, company) => { 
    return company.int_id_user === user.p_id;
}

exports.getUserByDBId = async (userDBId) => { 
    const dbCon = await getDatabaseConnection();
    const result = await executeProc({p_id: userDBId}, dbCon, "proc_get_user_by_id");
    return result[0];
}

exports.getUserMainData = getUserMainData;
exports.getUserDetailsData = getUserDetailsData;
exports.fillDefaultUserDataIfNotExistent = fillDefaultUserDataIfNotExistent;
exports.sendAccountCredentialsToUser = sendAccountCredentialsToUser;
exports.addDefaultPasswordOfInternalUsers = addDefaultPasswordOfInternalUsers;
exports.isUserExistent = isUserExistent;
exports.generateUserName = generateUserName;