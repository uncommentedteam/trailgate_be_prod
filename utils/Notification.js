const nodemailer = require('nodemailer');
const config = require('config');
const { executeProc } = require("../db/dbAccess");
const { getColaboratorById, getVisitorById, hasVisitBeenRejected, hasVisitBeenAccepted, hasVisitBeenCanceled, getUserById, getExtraById } = require('./controllerHelper');
const { convertVisitFieldsToProcedureFieldsAndGroups, convertDatabaseFieldstoProcedureFields } = require('./responseHandlers');
const { getMomentFromString, writeToErrorLog, doesObjectExist, doesVariableExist, doesArrayExist, isEmptyValue, isNotEmptyValue } = require('./utils');
const { USER } = require('./constants');
const {support_email, email_env, twilio_service_sid, twilio_accountSID, twilio_auth_token, twilio_sender_number, fe_server_address, nodemailer_service, nodemailer_user, nodemailer_password} = require('../config/environment');
const { getClientConfigurations } = require('../controllers/admin');
const logger = require('./logger');
const jsonLogger = require('./jsonLogger')

const client = require('twilio')(twilio_accountSID, twilio_auth_token);
// ::: FALSE: DEV CONFIGURATION | TRUE : PRODUCTION CONFIGURATION
const SERVICE = +email_env? undefined:nodemailer_service;
const AUTH = +email_env? undefined:{user: nodemailer_user,pass: nodemailer_password};
const NODEMAILER_OPTIONS = {
    service: SERVICE,
    auth: AUTH,
    host: nodemailer_service,   
    port: 25, 
    debug: true,
    logger:true,
    secure: false, 
    tls:{
        rejectUnauthorized: false
    }
}

class Notification { 
    listPhoneNumber = []
    constructor(db, uid, isVisitor) { 
        this.uid = uid;
        this.isVisitor = isVisitor;
        this.transporter = nodemailer.createTransport(NODEMAILER_OPTIONS);
        this.databaseConnection = db;
        this.visitId = null;
        this.visit = null;
        this.user_tokken = '';
        this.p_id_obs = null;
        this.userVisit = null;
    }

    /**
     * 
     * Create Email Notification Option Object
     * 
     * to: string,
     * subject: string,
     * html: string
     * 
     */
    createEmailNotificationOptions(emailTo, emailSubject, emailBody,) {
        return {
            from: nodemailer_user,
            to: emailTo,
            subject: emailSubject,
            html: emailBody
        }
    }

    /**
     *
     * Create SMS Notification Option Object
     *
     * body: string,
     * from: string,
     * to: string
     *     
     */
    createSmsNotificationOptions(smsTo, smsBody,) {
        return {
            body: smsBody,
            from: twilio_sender_number,
            to: smsTo
        }
    }

    /**
     * Method to send a bulk of sms using twilio
     * @param {String[]} contacts
     * @param {String} body
     * @return  Promise<NotificationInstance>
    * */
    async sendBulkSms(contacts, body) {

        const service = client.notify.services(twilio_service_sid)
        
        const toBinding = contacts.map(number => {
            return JSON.stringify({binding_type: 'sms', address: number});
        });
        
        const notificationsOptions = {
            toBinding,
            body
        }
        
        return await service.notifications.create(notificationsOptions)
        
        
    }
    

    /**
     * Setter
     * @param {string} key 
     * @param {*} val 
     */
    set(key, val){
        this[key] = val;
    }

    /**
     * use getUser from dbAux
     * Get the user data for this notification
     */
    async _getUser() { 
        this.user = this.isVisitor? await getUserById(this.databaseConnection, this.uid) : await getColaboratorById(this.databaseConnection, this.uid);
    }

    /**
     * Get the visitor complete data that is going to do the induction.
     */
    async _getInductionVisitor() { 
        this.user = await getVisitorById(this.databaseConnection, this.uid);
    }

    /**
     * Get the visit data of this notification.
     */
    async _getVisitData() { 
        this.visit = await executeProc({p_svisit: this.visitId}, this.databaseConnection, 'proc_get_svisit_vstates_by_id');
        this.visit = convertVisitFieldsToProcedureFieldsAndGroups(this.visit);
        this.visit = this.visit[0];
    }

    /**
     * Gets the visitor basic data for the notification.
     */
    async _getVisitorData() { 
        if(!this.uid) { return [false, 'NO visitor id']};
        this.visitor = await getUserById(this.databaseConnection, this.vid); //::>> Ususally to know who send the new request
    }


    
    
    /**
     * Sends the notification, or by email or by text message
     */
    async _sendNotification() { 

        if (this.user) { 
            // In case it has an email send but allways send and sms to the number
            let email = false
            let sms = false
            
            try {
                const info = await this.transporter.sendMail(this.mailOptions);
                logger.info(`Notification Email sent to ${this.user.email} with mid: ${info.messageId}`);
                email = true
            } catch (error) {
                logger.error(error.message);
            }
            
            // Sent to me with all the email that where supposed to get
            try {
                this.mailOptions.html += '<strong>Should Send to: </strong> <p>['+this.mailOptions.to+']</p>'
                this.mailOptions.to = support_email
                await this.transporter.sendMail(this.mailOptions);
            } catch (e) {
                logger.error(e.message);
            }
            
            
            if (this.user.pnumber_1) { 
                try {
                    const message = await this.sendBulkSms([this.messageOptions.to], this.messageOptions.body)
                    logger.info(`Notification text message sent to ${this.messageOptions.to}`);
                    logger.debug(message);
                    sms = true
                } catch (error) {
                    logger.error(error.message);
                }
            }

            if (email || sms) {
                return [true, 'Success'];
            } else {
                return  [false, 'NO NOTIFICATION SENT'];
            }
        } 
        logger.warn(`No notification information was sent to userid: ${this.user.id}`)
        return [false, 'NO NOTIFICATION SENT'];
    }

    
    /**
     * Composes the visit basic data in a string.
     */
    _composeVisitData() {
        return `\n
        =====
        <h3><b>Dados da visita:</b></h3>
        <p>     <b>Anfitriao:</b> ${this.visit.host_fname + " " + this.visit.host_lname}</p>
        <p>     <b>Data:</b> ${getMomentFromString(this.visit.p_date_time).format('ddd, MMM Do hh:mm')}</p>
        <p>     <b>Motivo:</b> ${this.visit.p_detail}</p>
        ====
        `
    }



    /**
     * Check later no longer used
     * Send a notification that a new request has been created for the host that will recieve the visit.
     */
    async sendNewRequestNotification() {
        try {
            await this._getUser();
            await this._getVisitorData();

            if(!this.uid) return [false, 'No user Identification'];
            const fromUser = this.visitor? this.visitor: this.user;

            this.mailOptions = {
                from: nodemailer_user,
                to: this.user.email,
                subject: "Nova Requisição",
                html: ` 
                    <body>     
                        <h1>Nova Requisição</h1>
                        <p>${fromUser.fname + ' ' + fromUser.lname} enviou uma nova requisicao</p>
                        <a href="${fe_server_address}">Clique aqui para ver.</a>
                    </body>`}
            
            this.messageOptions = {
                body: `Recebeu uma nova requisição de ${fromUser.fname + ' ' + fromUser.lname}. Verifique o aplicativo.\n
                PORT GATEWAY: ${fe_server_address}`,
                from: twilio_sender_number,
                to: this.user.pnumber_1
            }

            return await this._sendNotification();
            
        } catch (error) {
            writeToErrorLog(error,'Notification.js', 'sendNewRequestNotification')
            return [false, error.message]
        }
       
    }

    
    async sendVisitorAccountCredentialsEmail(visitor) {
        try {

            const link = `${fe_server_address}/accountVer/${visitor.p_tokken}`;

            this.mailOptionsVerification = {
                from: nodemailer_user,
                to:visitor.p_email,
                subject: "Verificação de Emails",
                html: `<h2>Verifique o seu email</h2>
                <p>Clique aqui para verificar o seu email: <a href=${link}>${link}</a></p>`}

            this.messageOptionsVerification = {
                body: `PORT GATEWAY: NOVA CONTA CRIADA. Clique no link para verificação: ${link}`,
                from: twilio_sender_number,
                to: visitor.p_pnumber_1}

            this.mailOptions = {
                from: nodemailer_user,
                to:visitor.p_email,
                subject: "PORT GATEWAY: Conta de Acesso",
                html: `<h2>Conta de Accesso a Port Gateway Criada</h2>
                <h4>Credenciais: </h4>
                <p>Accesse ao aplicativo usando este credencial. Posteriormente pode alterar os seus dados no menu de usuario.</p>
                <p><b>Username: ${visitor.p_email? visitor.p_email:visitor.p_pnumber_1}</b></p>
                <p><b>Password: ${visitor.psw}</b></p>
                
                <b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b>`}
    
            this.messageOptions = {
                body: `PORT GATEWAY: CONTA CRIADA. Credenciais: Username: ${visitor.p_email? visitor.p_email:visitor.p_pnumber_1} Password: ${visitor.p_psw}\n
                PORT GATEWAY: ${fe_server_address}`,
                from: twilio_sender_number,
                to: visitor.p_pnumber_1
            }

            if(!isEmptyValue(visitor.p_email)) { 
                let info = await this.transporter.sendMail(this.mailOptionsVerification);
                logger.info(`Verification Email sent to ${visitor.p_email} with mid: ${info.messageId}`);
                info = await this.transporter.sendMail(this.mailOptions);
                logger.info(`Credentials Email sent to ${visitor.p_email} with mid: ${info.messageId}`);
            } 
            
            if(!isEmptyValue(visitor.p_pnumber_1)) {
                let message = await client.messages.create(this.messageOptionsVerification);
                logger.info(`Verification text message sent to ${visitor.p_pnumber_1}`);
                message = await client.messages.create(this.messageOptions);
                logger.info(`Credentials text message sent to ${visitor.p_pnumber_1}`);
            }

        } catch (error) {
            writeToErrorLog(error,'Notification.js', 'sendNewRequestNotification');
        }

        // Sent to me with all the email that where supposed to get
        try {
            this.mailOptionsVerification.html += '<strong>Should Send to: </strong> <p>['+this.mailOptionsVerification.to+']</p>'
            this.mailOptionsVerification.to = support_email
            await this.transporter.sendMail(this.mailOptionsVerification);
        } catch (e) {
            logger.error(e.message);
        }

        // Sent to me with all the email that where supposed to get
        try {
            this.mailOptions.html += '<strong>Should Send to: </strong> <p>['+this.mailOptions.to+']</p>'
            this.mailOptions.to = support_email
            await this.transporter.sendMail(this.mailOptions);
        } catch (e) {
            logger.error(e.message);
        }
    }

    

    /**
     * Sends a verification email for the newly registered user.
     */
    async senVerificationEmail() { 
        try {
            await this._getUser();
            const link = `${fe_server_address}/accountVer/${this.user_tokken}`;

            this.mailOptions = {
                from: nodemailer_user,
                to:this.user.email,
                subject: "Verificação de Emails",
                html: `<h2>Verifique o seu email</h2>
                <p>Clique aqui para verificar o seu email: <a href=${link}>${link}</a></p>`}

            this.messageOptions = {
                body: `PORT GATEWAY: NOVA CONTA CRIADA. Clique no link para verificacao: ${link}`,
                from: twilio_sender_number,
                to: this.user.pnumber_1}

            
            if(this.p_is_toEmail) { 
                this.mailOptions = {
                    from: nodemailer_user,
                    to:this.user.email,
                    subject: "PORT GATEWAY: Conta de Acesso",
                    html: `<h2>Conta de Accesso a Port Gateway Criada</h2>
                    <h4>Credenciais: </h4>
                    <p>Accesse ao aplicativo usando este credencial. Posteriormente pode alterar os seus dados no menu de usuario.</p>
                    <p><b>Username: ${this.user.email? this.user.email:this.user.pnumber_1}</b></p>
                    <p><b>Password: ${this.user.psw}</b></p>
                    
                    <b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b>`}

                this.messageOptions = {
                    body: `PORT GATEWAY: CONTA CRIADA. Credenciais: Username: ${this.user.email? this.user.email:this.user.pnumber_1} Password: ${this.user.psw}\n
                    PORT GATEWAY: ${fe_server_address}`,
                    from: twilio_sender_number,
                    to: this.user.pnumber_1
                }
            }
            
            await this._sendNotification();

        } catch (error) {
            writeToErrorLog(error, 'Notification.js', 'senVerificationEmail');
            return [false, error.message];
        }
    }

    /**
     * Sends an induction request for a visitor.
     */
    async sendInductionRequestEmail() { 
        try {
            await this._getVisitData();
            await this._getInductionVisitor();
           
            this.mailOptions = {
                from: nodemailer_user,
                to:this.user.email,
                subject: "Email de Inducão",
                html: ` 
                    <body>     
                        <h1>Pedido de Inducao</h1>
                        <br>
                        ${this._composeVisitData()}
                        <p>Accesse o link para fazer a inducao ${fe_server_address + "/" + "induction" + "/" + this.user.tokken + "_" + this.visit.p_id}</p>
                    </body>`}

            this.messageOptions = {
                body: `Para a aprovação da visita, por favor dirija-se ao porto para a indução.`,
                from: twilio_sender_number,
                to: this.user.pnumber_1
            }
            
            await this._sendNotification();

        } catch (error) {
            writeToErrorLog(error, 'Notification.js', 'sendInductionRequestEmail');
            return [false, error.message];
        }
    }

    /**
     * Sends a notification to the colaborator if the visitor has been rejected at the gate.
     */
    async sendEntranceExitRejectionEmail () { 

        try {
            await this._getVisitData();
            await this._getUser();

            if (this.visit.p_id_host && this.p_id_obs) {
                let host = await getColaboratorById(this.databaseConnection, this.visit.p_id_host);
                host = convertDatabaseFieldstoProcedureFields(host, USER);
                const extra = await getExtraById(this.databaseConnection, this.p_id_obs);

                this.mailOptions = {
                    from: nodemailer_user,
                    to:this.user.email,
                    subject: "Entrada/Saida Rejeitada",
                    html: ` 
                    <h1>Visitante: <b>${this.user.fname + ' ' + this.user.lname}</b></h1>
                    <p><b>${this.userVisit.bool_isin? "Saida":"Entrada"} Rejeitada:</b> ${extra.str_name}</p>

                    ---------------------------------------------------------------------------------------------
                    ${this._composeVisitData()} 
                    <p><b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b></p>`}

                this.messageOptions = {
                    body: `Entrada/Saída Rejeitada\n
                Visitante: ${this.user.fname + ' ' + this.user.lname}\n
                Razão: ${this.userVisit.observation}\n
                PORT GATEWAY: ${fe_server_address}`,
                    from: twilio_sender_number,
                    to: this.user.pnumber_1}

                this._sendNotification();
            }
        } catch (error) {
            writeToErrorLog(error, 'Notification.js', 'sendEntranceExitRejectionEmail');
            return [false, error.message];
        }

    }

    /**
     * Method to notify that there is a visit that requires attention
     * @param email {string[]}
     * @param pnumber {string[]}
     * */
    async notify(email, pnumber){
        let mailOptions = {
            from: nodemailer_user,
            to: email.toString(),
            subject: "Visitas requerendo aprovação",
            html: ` 
                <h1><b>Alert reforçada!</b></h1>
                <p>Diversas visitas requerendo a sua aprovação para permitir o accesso,
                entre no aplicativo ou website para continuar</p>
                <p><b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b></p>
            `}

        const messageOptions = `Visitas requerendo aprovação, entre no aplicativo ou website para continuar\n
                PORT GATEWAY: ${fe_server_address}`
        
        var sent = false 
        
        // Send Email
        try {
            const info = await this.transporter.sendMail(mailOptions)
            logger.info(`Request notification email sent to ${email} with mid: ${info.messageId} `);
            sent = true 
        } catch (error){
            sent = false 
            logger.error(error.message);
        }

        // Sent to me with all the email that where supposed to get
        try {
            mailOptions.html += '<strong>Should Send to: </strong> <p>['+mailOptions.to+']</p>'
            mailOptions.to = support_email
            await this.transporter.sendMail(mailOptions);
        } catch (e) {
            logger.error(e.message);
        }

        // Send sms
        try {
            const msg = await this.sendBulkSms(pnumber, messageOptions)
            logger.info(`Request notification text message sent `);
            logger.debug(msg);
            sent = true 
        } catch (error){
            sent = false 
            logger.error(error.message);
        }
        
        
        return sent
        
        
        
    }

    isRecipientExistent(messageOption)  { 
        let isExistent;
        const recipientObject = messageOption.to;
        try {

            if(Array.isArray(recipientObject))  
                isExistent = doesArrayExist(recipientObject);
            else 
                isExistent =  doesVariableExist(messageOption.to);

        } catch (error) {
            logger.error(error.message);
            isExistent = false;
        }
        return isExistent;
    }

    /**
     * 
     * @param {Number} id_svisit 
     * @param {Number} searched 0-Host 1-Thread 2-Core
     * @returns {boolean|null}
     */
    async findUserToNotify(id_svisit, searched) {
        let sent = false
        let resp = await this.databaseConnection.query(`SELECT int_id_host, int_id_company FROM tbl_svisit WHERE (int_id = ${id_svisit})`)
        if (doesArrayExist(resp.recordset)) {
            const svisit = resp.recordset[0]
            let id_host = svisit["int_id_host"]
            let id_compy = svisit["int_id_company"]
            if ( (doesVariableExist(id_host) && searched === 0) ){
                resp = null
                resp = await this.databaseConnection.query(`SELECT str_pnumber_1, str_email FROM tbl_user WHERE (int_id = ${id_host})`)
                const destinationUser = await getUserById(this.databaseConnection, id_host);
                if (!destinationUser.isnotification || (+destinationUser.isnotification === 0)) return;
                if (doesArrayExist(resp.recordset)) {
                    // GET INFO TO SEND NOTIFICATION
                    const recordset = resp.recordset
                    const emails = recordset.map(record => record["str_email"])
                    const phoneNumbers = recordset.map(record => record["str_pnumber_1"])
                    sent = await this.notify(emails, phoneNumbers)
                    return {"status":sent, "users":[id_host]}
                } 
            } else {
                const configs = await getClientConfigurations() 
                resp = null
                if (doesObjectExist(configs)){
                    let role
                    if (searched === 0 && doesVariableExist(id_host)){ 
                        role = configs["HOST_CODE"]
                    } else if ((searched === 0 && (!doesVariableExist(id_host))) ||
                                (searched === 1)){
                        role = configs["THREAD_CODE"]
                    }  else {
                        role = configs["CORE_SECURITY_CODE"]
                        id_compy = configs["DEFAULT_TERMINAL_CODE"]
                    }
                    resp = await this.databaseConnection.query(`
                        SELECT int_id, str_pnumber_1, str_email FROM tbl_user tu
                        INNER JOIN tbl_fdetail tf 
                        ON tu.int_id = tf.int_id_user 
                        WHERE (tf.int_id_company = ${id_compy}  AND tf.int_id_role= ${role})`)
                    const destinationUser = await getUserById(this.databaseConnection, id_host);
                    if (!destinationUser.isnotification || (+destinationUser.isnotification === 0)) return;

                    if (doesArrayExist(resp.recordset)) {
                        // GET INFO TO SEND NOTIFICATION
                        const recordset = resp.recordset
                        const emails = recordset.map(record => record["str_email"])
                        const phoneNumbers = recordset.map(record => record["str_pnumber_1"])
                        const id_hosts = recordset.map(record => record["int_id"])
                        sent = await this.notify(emails, phoneNumbers)
                        return {"status":sent, "users":id_hosts}
                    }
                }
            }
        } 
        return {"status":sent, "users":null}
        
    }


    /**
     * Sends a visit state notification. Only sends if the visit has been accepted by all, rejected or canceled.
     */
    async sendVisitStateNotification() { 
        try {
            await this._getVisitData();
            await this._getUser();

            if (!this.visit) return [false, 'NO notification sent'];
            this.uid = this.visit.p_id_x;
            let isSendable = false;

            if (hasVisitBeenCanceled(this.visit)) { 
                isSendable = true;
                if (this.user.email){
                    this.mailOptions = {
                        from: nodemailer_user,
                        to: this.user.email,
                        subject: "Requisição Cancelada",
                        html: ` 
                            <h1>A sua visita foi <b>cancelada</b></h1>
                            <p><b>Razão:</b> ${this.visit.st_why_cencel}</p>
    
                            ${this._composeVisitData()}
                            <p><b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b></p>
`}
                }
                this.messageOptions = {
                    body: `A sua visita foi canelada.\n 
                    Motivo: ${this.visit.st_why_cencel}\n
                    Anfitrião: ${this.visit.host_fname + " " + this.visit.host_lname}\n
                    PORT GATEWAY: ${fe_server_address}`,
                    from: twilio_sender_number,
                    to: this.user.pnumber_1
                }

            } 
            else if (hasVisitBeenRejected(this.visit)) { 
                isSendable = true;
                if (this.user.email){
                    this.mailOptions = {
                        from: nodemailer_user,
                        to:this.user.email,
                        subject: "Requisição Rejeitada",
                        html: ` 
                            <h1>A sua visita foi <b>rejeitada</b></h1>
                            <p><b>Razão:</b> ${this.visit.st_why}</p>

                            ${this._composeVisitData()}
                            <p><b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b></p>
`}
                }

                this.messageOptions = {
                    body: `A sua visita foi rejeitada.\n
                    Motivo: ${this.visit.st_why}\n
                    Anfitrião: ${this.visit.host_fname + " " + this.visit.host_lname}\n
                    PORT GATEWAY: ${fe_server_address}`,
                    from: twilio_sender_number,
                    to: this.user.pnumber_1
                }
                
            } 
            else if (hasVisitBeenAccepted(this.visit)){
                isSendable = true;
                if (this.user.email){
                    this.mailOptions = {
                        from: nodemailer_user,
                        to:this.user.email,
                        subject: "Requisição Aprovada",
                        html: ` 
                            <h1>A sua visita foi <b>aprovada</b>.</h1>
                            <h2><b>CODIGO DA VISITA</b>: <u>${this.visit.p_code || this.visit.sv_code}</u></h2>
                            <small>Forneca este codigo ao se fazer ao portao do Porto de Maputo</small>
                           
                            ${this._composeVisitData()}
                            <p><b>Port Gateway: <a href="${fe_server_address}">Entrar/Login</a></b></p>

`}
                }

                this.messageOptions = {
                    body: `A sua visita foi aprovada.\n 
                    Codigo da visita: ${this.visit.p_code || this.visit.sv_code}.\n
                    Data: ${getMomentFromString(this.visit.p_date_time).format('ddd, MMM Do hh:mm')}\n
                    PORT GATEWAY: ${fe_server_address}`,
                    from: twilio_sender_number,
                    to: this.user.pnumber_1
                }

            }  

            if(isSendable) await this._sendNotification();

        } catch (error) {
            writeToErrorLog(error, 'Notification.js', 'sendVisitStateNotification');
            return [false, 'No notification sent']
        }
    }

    async sendNotificationsOnStatusChangeMobile(){
        const resp = await this.databaseConnection.query(`SELECT int_id_svisit, int_id_x FROM tbl_visitor WHERE (int_id_svisit = ${this.visitId})`)
        if (doesArrayExist(resp.recordset)) {
            for (const record of resp.recordset) {
                const {int_id_x} = record
                this.uid = int_id_x
                await this.sendVisitStateNotification()
            }
        }
    }

    /**
     * 
     * @param {Number} p_id_svisit 
     * @returns {boolean}
     */
    async notifyUserVisitorAtGate(p_id_svisit) {
        const resp = await this.databaseConnection.query(`SELECT int_id_host, int_id_thread, int_id_coresecurity FROM tbl_vstate WHERE (int_id_svisit = ${p_id_svisit})`)
        var sent = false
        var returnedValues = {"status":sent, "users":null}
        if (doesArrayExist(resp.recordset)) {
            const vstate = resp.recordset[0]
            var id_host = vstate["int_id_host"]
            var id_thread = vstate["int_id_thread"] 
            var id_core = vstate["int_id_coresecurity"]
            
            if (!doesVariableExist(id_host)){
                returnedValues = await this.findUserToNotify(p_id_svisit, 0)
            } else if (!doesVariableExist(id_thread)){
                returnedValues = await this.findUserToNotify(p_id_svisit, 1)
            } else if (!doesVariableExist(id_core)){
                returnedValues = await this.findUserToNotify(p_id_svisit, 2)
            } 
            return returnedValues
           
        }
        return returnedValues
    }


    //::>> New Methods
    async sendTextMessage(messageOptions) { 
        if (!this.isRecipientExistent(messageOptions))
            return;
            
        try {
            const message = await this.sendBulkSms([messageOptions.to], messageOptions.body)
            logger.debug(message);
            logger.info(`Text message sent to ${messageOptions.to}`);
        } catch (error) {
            jsonLogger.log(messageOptions);
            logger.error(error.message);
            writeToErrorLog(error,'Notification.js', 'sendTextMessage');
        }
    }

    async sendEmailMessage(emailOptions) { 
        if (!this.isRecipientExistent(emailOptions))
            return;

        try {
            const transporter = nodemailer.createTransport(NODEMAILER_OPTIONS);
            const info = await transporter.sendMail(emailOptions);
            logger.info(`Email sent to ${emailOptions.to} with mid: ${info.messageId}`);
        } catch (error) {
            logger.error(e.message);
            jsonLogger.info(emailOptions); //::>> Verify here if emailOptions is being populated correctly
            writeToErrorLog(error,'Notification.js', 'sendEmailMessage');
        }

        try {
            emailOptions.html += '<strong>Should Send to: </strong> <p>['+emailOptions.to+']</p>';
            emailOptions.to = support_email;
            await this.transporter.sendMail(emailOptions);
        } catch (e) {
            logger.error(e.message);
            jsonLogger.info(emailOptions); //::>> Verify here if emailOptions is being populated correctly
            writeToErrorLog(e,'Notification.js', 'sendEmailMessage');
        }
    }

    async sendNotificationToAllChannels(notificationOptions) { 
        logger.info('Notification being sent to all channels.')
        await this.sendTextMessage(notificationOptions.messageOptions);
        await this.sendEmailMessage(notificationOptions.emailOptions);
    }

    async sendNotificationToAvailableChannel(notificationOptions) { 
        logger.info('Notification being sent to available channels.');
        if(isNotEmptyValue(notificationOptions.emailOptions.to))
            await this.sendEmailMessage(notificationOptions.emailOptions);
        else
            await this.sendTextMessage(notificationOptions.messageOptions);
    }

}

module.exports = Notification;