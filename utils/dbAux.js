const {executeProc} = require("../db/dbAccess");
const {convertVisitFieldsToProcedureFieldsAndGroups} = require("./responseHandlers");
const {getVisitorById} = require("./controllerHelper");
const {getColaboratorById} = require("./controllerHelper");
const {getUserById} = require("./controllerHelper");

/**
 * Get user by id
 * @param isVisitor {boolean}
 * @param dbCon {databaseConnection}
 * @param userId {number | string}
 * */
exports.getUserById = async(isVisitor= false, dbCon, userId) => {
    if (isVisitor) {
        return await getUserById(dbCon, userId)
    }
    return await getColaboratorById(dbCon, userId);
}

/**
 * Get the visitor by id.
 * @param dbCon {databaseConnection}
 * @param userId {number | string}
 */
exports.getVisitorById = async(dbCon, userId) => {
    return await getVisitorById(dbCon, userId);
}

/**
 * Get the visit data by visit id.
 * @param visitId {number | string}
 */
exports.getVisitDataById = async(visitId, dbCon) => {
    let visit = await executeProc({p_svisit: visitId}, dbCon, 'proc_get_svisit_vstates_by_id');
    visit = convertVisitFieldsToProcedureFieldsAndGroups(visit);
    return visit[0];
}

