const fs = require('fs');
const path = require('path');
const moment = require('moment');
const useragent = require('useragent');
const { executeProc } = require('../db/dbAccess');
const randomString = require('randomstring');
const crypto = require('crypto');
const { getVisitsHappening,getRecievedVisits,getNumberVisitors,getVisitsToHappen, getChartData, getUserById} = require('../utils/controllerHelper');
const { ALL, HAPPENING,CANCELED_TODAY, PENDING_TODAY, REJECTED_TODAY, ACCEPTED_TODAY, PENDING, ACCEPTED, REJECTED, VISITORS, CANCELED, MAX_ERR_LOG_SIZE_IN_MB, VISIT, USER, DEFAULT_CONTRY, MACOS_UA_STRING } = require('./constants');
const logger = require('./logger');
const { getAndBeginTransaction } = require('../db/dbHelpers');

//::>> update regex library so it will be up to date to the latest
useragent(true);


/**
 * Writes to the error.log the error messages
 * @param {object} error 
 * @param {string} file 
 * @param {string} method 
 */
const writeToErrorLog = (error, fileWithError, methodWithError) => {
    
    if(doesObjectExist(error)){
        const message = ` ERROR_ID: ${error.number || error.name} ${error.message} | File: ${fileWithError} | Method: ${methodWithError}\n`;
        logger.error(message);
        const errorFile = path.join(__dirname,'..', 'logs', 'error.log');
   
        const fileStats = fs.statSync(errorFile);
        const fileSizeBytes = fileStats["size"];
        const fileSizeMB = fileSizeBytes / 1000000.0;

        if(fileSizeMB >= MAX_ERR_LOG_SIZE_IN_MB) { 
            fs.renameSync(errorFile , path.join(__dirname,'..', 'logs', `error_${getToday().format('DD_MM_YYYY')}.txt` ));
            fs.writeFileSync(errorFile, '');
        }

        fs.writeFile(errorFile, message, {flag: 'a+'}, error => { });
    } else { 
        logger.error(`Unhandled Error ${error}`);
    }
    
}

/**
 * Converts the user object with the direct database fields to a user object with the procedure fields
 * @param {object} userWithDbFields 
 */
exports.convertUserFieldsToProcedureFields = (userWithDbFields) => {
	const {doesObjectExist} = require('../utils/utils');
	if(!doesObjectExist(userWithDbFields)) return;
	let convertedUserFields = {};
	const boolsToConvertToIntegers = ["istemp", "isvisitor", "isactive"];

	Object.keys(USER_FIELDS_PROCEDURE_FIELDS_MAP).forEach((key) => {
		if (boolsToConvertToIntegers.includes(key)) {
			convertedUserFields = { ...convertedUserFields, [USER_FIELDS_PROCEDURE_FIELDS_MAP[key]]: userWithDbFields[key] ? 1 : 0 };
		} else {
			convertedUserFields = { ...convertedUserFields, [USER_FIELDS_PROCEDURE_FIELDS_MAP[key]]: userWithDbFields[key] };
		}
	});

	return convertedUserFields;
};


exports.addDataToObject = (data={}, object) => { 
    Object.keys(data).forEach(key => { 
        object[key] = data[key];
    })
}
 
/**
 * Checks if an object exists or if it has fields
 * @param {object} object 
 */
const doesObjectExist = (object) => { 
  try {
    return !((object === null) || (object === undefined) || (object === {}) || (Object.keys(object).length === 0));
  } catch (error) {
    return false;
  }
}

/**
* Checks if an array exists or if it has content
* @param {any[]} array 
*/
const doesArrayExist = (array) => { 
  try {
    return !((array === null) || (array === undefined) || (array === []) || (array.length === 0));
  } catch (error) {
    return false;
  }
}

/**
* Check if a variable as an allowed value. Cannot be null or undefined.
* @param {*} variable 
*/
const doesVariableExist = (variable) => { 
  return !((variable === undefined) || (variable === null));
}

/**
 * Gets the content of the server files
 * @param {string} fileName 
 */
const getServerFilesContent = async (fileName) => { 
    try {
        let content = fs.readFileSync(path.join(__dirname,'..', 'res', 'serverFiles', fileName));
        return content;
        
    } catch (error) {
        writeToErrorLog(error, 'utils.js', 'getFileContent')
        return '';
    }
}

/**
 * Writes contents to a server file.
 * @param {string} fileName 
 * @param {*} content 
 */
const writeContentToServerFile = async (fileName, content) => {
    fs.writeFile(path.join(__dirname,'..', 'res', 'serverFiles', fileName), content,
     error => {
        writeToErrorLog(error, 'utils.js', 'writeToFileContent');
     });
}

/**
 * Gets the current date as moment.js object.
 */
const getToday = () => { 
    return moment();
}

/**
 * Creates a moment object from date string.
 * @param {string} dateString 
 */
const getMomentFromString = (dateString, format) => {
    let momentObj;
    if (format) 
       momentObj = moment(dateString, format); 
     else 
       momentObj = moment(dateString, ['YYYY-MM-DDTHH:mm:ss.sssZ']);
    return momentObj;
  }


const getProcParamsBackupSrcipt = (procedure) => { 
    const script = `SELECT parameters.name param_name , types.name param_type FROM sys.parameters
    inner join sys.procedures on parameters.object_id = procedures.object_id
    inner join sys.types on parameters.system_type_id = types.system_type_id AND parameters.user_type_id = types.user_type_id
    where procedures.name = '${procedure}'`;
    return script;
}

/**
 * Creates a new object from the object parameter only with the fields specified in the array
 * @param {string} object 
 * @param {array} arrayOfFields 
 */
const extractFieldsFromObject = (object, arrayOfFields) => { 
    if(!doesObjectExist(object) || !doesArrayExist(arrayOfFields)) return {};
    let newObject = {};
    arrayOfFields.forEach(field => { 
        newObject = {...newObject, [field]:object[field]}
    });
    return newObject;
}

/**
 * Checks if the visit is to the current date.
 * @param {object} visit 
 */
const isVisitToToday = (visit) => {
    if(!doesObjectExist(visit)) return true;
    const today = getToday();
    const visitDate = getMomentFromString(visit.p_date_time);
    return visitDate.isSame(today, 'day');
}

/**
 * Gets the last id of a table.
 * @param {object} databaseConnection 
 * @param {string} tableName 
 */
const getLastIdOfTable = async (databaseConnection, tableName) => {
    if(!doesObjectExist(databaseConnection) || !doesVariableExist(tableName)) return 0;

    let visitId = await databaseConnection.query(`SELECT IDENT_CURRENT('${tableName}')`);
    return visitId.recordset[0][""];
}

/**
 * Removes a certain property from an object
 * @param {object} object 
 * @param {string} property 
 */
const removePropertyFromObject = (object, property) => { 
    let copyObj = {...object};
    delete copyObj[property];
    return copyObj;
}

/**
 * Gets an error message by the error code.
 * @param {number} errorCode 
 */
const getErrorMessageByErrorId = (error) => {
    const errorCode = extractErrorCodeFromError(error);
    const errors = {
        515: 'Could Not Save! (Missing Data Attribute)',
        547: 'Could Not Update! Server Error: Data Conflict. Contact Admin.',
        'TypeError': 'TE: Could not perform operation!',
        201: 'PE: Operation completed with error!',
        'ESOCKET': 'Could not Connect To Database',
        'EREQINPROG':'Could not perform operation. Try again!',
        'EREQUEST2627': 'Duplicate Data is trying to be registred!',
        'EREQUEST201': 'DBOPT: Invalid request data!',
        'EPARAM': 'There is an Invalid Request Paramenter!',
        'ReferenceError': 'RFR: Could not perform operation. contact Support!',
        'TypeError': 'TPR: Could not perform operation. Possible Network Error!',
        'ETIMEOUT': 'Connection Timeout!',
        'Error': error.message,
    }
    return errors[errorCode] || 'An Unknown Error Ocurred';
}

const extractErrorCodeFromError = (error) => { 
    let errorCode = error.code;
    errorCode = errorCode? error.code : error.number;
    if(isEmptyValue(errorCode))
        if(isNotEmptyValue(error.stack))
            errorCode = error.stack.split(":")[0];
    else if(isNotEmptyValue(error.number))
        errorCode += `${error.number}`;
    return errorCode;
}

const formalizeXcelData = (req, xlData) => { 
    const formalized = [];
    const fieldNames = ['p_fname', 'p_lname', 'p_nid', 'p_expiration_date_id', 'p_pnumber_1', 'p_pnumber_2', 'p_email'];
    let isMacintosh = false;
    verifyFileFormat(fieldNames, xlData[1]);
    xlData.shift(); //:: Remove instructions row
    xlData.shift(); //:: Remove header
    const agent = useragent.parse(req.headers['user-agent']);
    const userOSType = agent.os.toJSON().family === MACOS_UA_STRING;

    let currentUserData = {}, colName = "", colValue = "";
    for (let userRow of xlData) { 
        for(let colIndex = 0; colIndex < userRow.length; colIndex++){
            colName = fieldNames[colIndex];
            colValue = userRow[colIndex];
            if(colName === 'p_expiration_date_id') { 
                if((typeof colValue) === 'number')
                    currentUserData[colName] = convertExcelSerialDateToDateTimeString(userOSType, colValue);
                else
                    currentUserData[colName] = (colValue ?? "01/01/2100").split('/').length > 1 ? colValue : "01/01/2100";
            } else if (['p_pnumber_1', 'p_pnumber_2'].includes(colName)){
                currentUserData[colName] = String(colValue).replace(/\s+/g, '');
            } else { 
                currentUserData[colName] = colValue || '';
            }


        }
        formalized.push(currentUserData);
        currentUserData = {};
    }

    if(isEmptyArray(formalized) || isEmptyObject(formalized))
        logger.warn(`There are no users to process for the file.`);
    return formalized;
}

const convertExcelSerialDateToDateTimeString = (userOSType, excelSerialDate) => { 
    let unixTimeStamp = 0;
    
    if (userOSType === MACOS_UA_STRING) 
        unixTimeStamp = (excelSerialDate - 24107) * 86400;
    else 
        unixTimeStamp = (excelSerialDate - 25569) * 86400;
    
    const date = moment.unix(unixTimeStamp);
    const dtFormat = date.format('DD/MM/YYYY');
    return dtFormat;
}

const validateProcessedUsersFromXcelFile = async (formalizedUsers) => { 
    const { isUserExistent } = require('./authUtils');
    const genPassword = randomString.generate({length: 8});
    const defaultFieldValues = {p_active: 0, p_isvisitor: 1, p_id_type_id: 47, p_registry: getToday().toISOString(), p_istemp: 1, p_psw_2: genPassword, p_psw: genPassword};


    const transaction = await getAndBeginTransaction();
    let isExistent;
    const today = getToday();
    for (let user of formalizedUsers) { 
        isExistent =  await isUserExistent(user, transaction);
        if(isExistent) { 
            user.warning = 1;
            user.messages = {
                warning: 'User already exists',
            }
            logger.warn(`${user.p_fname} ${user.p_lname} is already existent.`);
            continue;
        }

        if (isEmptyObject(user.p_fname) || isEmptyObject(user.p_lname)) { 
            user.p_fname = 'NO NAME'; user.p_lname = 'NO NAME';
            addErrorToFileuser(user, 'Missing first name or last name');
            continue;
        }

        if (isEmptyValue(user.p_nid)){
            addErrorToFileuser(user, "Missing id document");
            continue;
        }
        if (isEmptyValue(user.p_expiration_date_id)) { 
            addErrorToFileuser(user, "Missing id document expiration date");
            continue;
        }
        if (!moment(user.p_expiration_date_id, "DD/MM/YYYY").isValid()) {
            addErrorToFileuser(user, "Invalid document expiration date");
            continue;
        }
        if (getMomentFromString(user.p_expiration_date_id, "DD/MM/YYYY").isBefore(today)) { 
            addErrorToFileuser(user, "Id document has expired");
            continue;
        }
        if (isEmptyValue(user.p_pnumber_1)) { 
            addErrorToFileuser(user, "Missing main phone number");
            continue;
        }
        if (isEmptyValue(user.p_pnumber_2)) { 
            addErrorToFileuser(user, "Missing secondary phone number");
            continue;
        }
        if (user.p_pnumber_1 === user.p_pnumber_2) { 
            addErrorToFileuser(user, "Main and secondary phone numbers are equal");
            continue;
        }

        Object.keys(defaultFieldValues).forEach(field => {
            user[field] = defaultFieldValues[field];
        })
        user.error = 0; 
        user.warning = 0;
        user.p_nationality = DEFAULT_CONTRY};

}

const addErrorToFileuser = (user, errorMessage) => { 
    user.error = 1;
    user.messages = {
        error: errorMessage,
    }
    logger.error(`User ${user.p_fname} ${user.p_lname} has invalid fields : ${errorMessage}`);
}

const verifyFileFormat = (defaultFieldNames, userFieldValues) => {
    let invalidFormat = false;

    if (userFieldValues.includes(null) || userFieldValues.includes(undefined)) 
        invalidFormat = true;
    if (defaultFieldNames.length !== userFieldValues.length)
        invalidFormat = true;

    if (invalidFormat) { 
        logger.error(`Could not process excel file`);
        throw new Error('Excel file data format is not recognized.');
    }

}





/**
 * Converts the users' excel file data content to objects of user.
 * @param {object} databaseConnection 
 * @param {string[][]} xlArray 
 */
const convertXlDataToUserObjects = async (databaseConnection, xlArray) => { 
    const fieldNames = ['p_fname', 'p_lname', 'p_nid', 'p_expiration_date_id', 'p_pnumber_1', 'p_pnumber_2', 'p_email'];
    let defaultFieldValues = {p_active: 0, p_isvisitor: 1, p_id_type_id: 47, p_registry: getToday().toISOString(), p_istemp: 1};
    let users = [];
    let user = {};
    let xlColumnData;
    let gottenUserAttempt1;
    let gottenUserAttempt2;
    let date;

    for(fileUserXlRow of xlArray ) {

        for (let i = 0; i < fileUserXlRow.length; i++) {
            xlColumnData = fileUserXlRow[i];
            if(xlColumnData) user = {...user, [fieldNames[i]]: xlColumnData}
        };

        try { 
            gottenUserAttempt1 = await executeProc({p_nid: user.p_nid, p_isvisitor: 1}, databaseConnection, 'proc_get_user_by_nid');
            gottenUserAttempt2 = await executeProc({p_nid: user.p_nid, p_isvisitor: 0}, databaseConnection, 'proc_get_user_by_nid');

            date = getMomentFromString(user.p_expiration_date_id);
            user = {...user, p_expiration_date_id: date.toISOString()}

            if((gottenUserAttempt1.length + gottenUserAttempt2.length) > 0) user = {...user, exists: 1};
            if(date.toISOString() === null) user = {...user, failed: 1}; 
            if(date.isBefore(getToday())) user = {...user, failed: 1};
            
            
            if((Object.keys(user).length < 7) || !date) {
                if(user.p_email) { 
                    user = {...user, missing: 1}
                } else if (!user.p_email && (Object.keys(user).length < 6)) { 
                    user = {...user, missing: 1}
                }
            }
            user = {...user, p_psw: randomString.generate({length: 8}), p_nid: `${user.p_nid}`}
        
        } catch (error)  {
            writeToErrorLog(error, 'utils.js', 'convertXlDataToUserObjects' );
            user = {...user, failed: 1} 
        }   

        user = {...user, ...defaultFieldValues, p_nationality: DEFAULT_CONTRY};
        users = [...users, user];
        user = {};
    };

    return users;
}

/**
 * Sends and update to the frontend. (Realtime feature)
 * @param {object} req 
 */
const sendUpdate = (req) => { 
    if(req.vSocket){
        req.vSocket.broadcast.emit('update')
        req.vSocket.emit('update');
    }

}

/**
 * Gets statistics of all the visits passed.
 * @param {array} allVisits 
 */
const getStatsOfVisits = (allVisits) => {
    const stats = {
        total: 0,
        recieved_v: 0,
        visitors_in: 0, // all the visitors that are currently inside
        pending: 0, // all the ones that have no acceptance
        toConfirm: 0, // all the ones that are waiting for host confirmation
        toHappen: 0, // all the ones that have been accepted but do not have entrance time
        happening: 0, // all the ones that have entrance
        finished: 0, // all the ones that have happened
    }

    // let allVisits = allVisits;
    // fileterdVisits = ch.unifyGroupedVisits(allVisits);
    //:::>> GET the total amount of visits 
    stats.total = allVisits.length;

    //::>> GET THE NUMBER OF VISITS THAT ARE HAPPENING
    const happeningVisits = getVisitsHappening(allVisits);
    stats.happening = happeningVisits.length;

    //::>> GET THE NUMBER OF RECIEVED VISITS
    const recievedVisits = getRecievedVisits(allVisits);
    stats.recieved_v = recievedVisits.length;
    stats.finished = recievedVisits.length;

    //::>> GET THE NUMBER OF VISITORS CURRENTLY INSIDE
    // const groupedVisits = ch.identifyGroups(happeningVisits);
    stats.visitors_in = getNumberVisitors(happeningVisits);

    //::>> GETH THE NUMBER OF VISITS TO HAPPEND
    const visitsToHappen = getVisitsToHappen(allVisits);
    stats.toHappen = visitsToHappen.length;

    //::>> GET THE CHART DATA
    const chartData = getChartData(allVisits);
  

    const statsVisits = { allVisits, happeningVisits, recievedVisits, visitsToHappen };
    return { stats, statsVisits, chartData }
}

/**
 * Gets the visit of a specific visitor.
 * @param {object} databaseConnection 
 * @param {number} visitId 
 * @param {number} userId 
 */
const getVisitorVisit = async (databaseConnection, visitId, userId) => { 
    let result = await databaseConnection.query(`
        SELECT * FROM view_user_visitor tu 
        LEFT JOIN tbl_visit tv ON tu.cod = tv.int_id_x
        LEFT JOIN tbl_extra  ON tv.int_id_obs = tbl_extra.int_id 
        WHERE tv.int_id_svisit = ${visitId} AND tv.int_id_x = ${userId};
    `);
    return result.recordset[0];
}

/**
 * Gets the visits statistics of a terminal.
 * @param {array} allVisits 
 * @param {object} databaseConnection 
 */
const getTerminalStats = async (allVisits, databaseConnection) => {
    const {convertDatabaseFieldstoProcedureFields} = require('../utils/responseHandlers');
    let visitors = [];
    let allVisitors = [];
    let visits = [];
    let stats = { 
        [ALL]: 0,
        [HAPPENING]: 0,
        [PENDING_TODAY]: 0,
        [CANCELED_TODAY]: 0,
        [ACCEPTED_TODAY]: 0,
        [VISITORS]: [],
    }

    let convertedVisit, visitor, canCountHappeningVisit, visitorIds;
    visitorIds = [];
    
    for (let visit of allVisits) { 
        canCountHappeningVisit = false;
        convertedVisit = convertDatabaseFieldstoProcedureFields(visit, VISIT);
        visits = [...visits, convertedVisit];

        if(getMomentFromString(convertedVisit.p_date_time).isSameOrAfter(getToday(), 'day')) stats[ALL] = stats[ALL] + 1;

        try{ 

            if(convertedVisit.state === PENDING) { 
                stats[PENDING + '_TODAY'] = stats[PENDING + '_TODAY'] + 1;
            } else if (convertedVisit.state === ACCEPTED) {
                stats[ACCEPTED + '_TODAY'] = stats[ACCEPTED + '_TODAY'] + 1;
            } else if (convertedVisit.state === CANCELED) {
                stats[CANCELED + '_TODAY'] = stats[CANCELED + '_TODAY'] + 1;
            }
    
            visitor = await getVisitorVisit(databaseConnection, convertedVisit.p_id, visit.x_cod);
            if (convertedVisit.sv_is_ps) visitor.sv_is_ps = 1;
            allVisitors = [convertDatabaseFieldstoProcedureFields(visitor, USER), ...allVisitors];
                
            if (visitor.bool_isin) { 
                canCountHappeningVisit = true && !visit.sv_is_ps;
                visitors = [...visitors, convertDatabaseFieldstoProcedureFields(visitor, USER)];
                visitorIds = [...visitorIds, visitor.cod];
            }
    

        } catch (err) { 
            writeToErrorLog(err, 'utis.js', 'getTerminalsStats')
            if(convertedVisit.state === PENDING) { 
                stats[PENDING + '_TODAY'] = stats[PENDING + '_TODAY'] - 1;
            } else if (convertedVisit.state === ACCEPTED) {
                stats[ACCEPTED + '_TODAY'] = stats[ACCEPTED + '_TODAY'] - 1;
            } else if (convertedVisit.state === CANCELED) {
                stats[CANCELED + '_TODAY'] = stats[CANCELED + '_TODAY'] - 1;
            }
    
        }
    

        if(doesArrayExist(convertedVisit.grouped)) {
            for (let joinedVisit of convertedVisit.grouped){
                visitor = await getVisitorVisit(databaseConnection, convertedVisit.p_id, joinedVisit.x_cod);
                allVisitors = [convertDatabaseFieldstoProcedureFields(visitor, USER), ...allVisitors];
                if(visitor.bool_isin && !visitorIds.includes(visitor.cod)) { 
                    canCountHappeningVisit = true  && !visit.sv_is_ps;
                    visitors = [...visitors, convertDatabaseFieldstoProcedureFields(visitor, USER)];
                    visitorIds = [...visitorIds, visitor.cod]
                }
            }
        }
        if(canCountHappeningVisit) { stats[HAPPENING] = stats[HAPPENING] + 1};
    }

    return {stats: stats, visitors, visits, allVisitors};


}

const isEmptyValue = (value) => { 
    return (value === '') || (value === undefined) || (value === null);
  }

const isNotEmptyValue = (value) => { 
    return !isEmptyValue(value);
}
  
const isEmptyObject = (object) => { 
    let isEmpty = isEmptyValue(object);
    isEmpty = isEmpty || (Object.keys(object).length === 0);
    return isEmpty;
  }

const isNotEmptyObject = (object) => { 
    return !isEmptyObject(object);
}

const isEmptyArray = (array) => { 
    let isEmpty = isEmptyValue(array);
    isEmpty = isEmpty || (array.length === 0);
    return isEmpty;
  }

const isNotEmptyArray = (array) => { 
    return !isEmptyArray(array);
}

const validateArrayParameter = (array) => { 
    let arrayToCheck = array;
    if(isEmptyArray(array))
      arrayToCheck = [];
    return arrayToCheck;
  }

const checkTransactionErrorMessage = err => { 
    if(err)
        throw new Error(err.message);
}

const hashString = stringToHash => { 
    const sha256 = crypto.createHash('sha256');
    return sha256.update(stringToHash.toString()).digest('hex');
}



exports.getProcParamsBackupSrcipt = getProcParamsBackupSrcipt;
exports.writeToErrorLog = writeToErrorLog;
exports.getMomentFromString = getMomentFromString;
exports.extractFieldsFromObject = extractFieldsFromObject;
exports.getLastIdOfTable = getLastIdOfTable;
exports.getToday = getToday;
exports.removePropertyFromObject = removePropertyFromObject;
exports.getErrorMessageByErrorId = getErrorMessageByErrorId;
exports.convertXlDataToUserObjects = convertXlDataToUserObjects;
exports.sendUpdate = sendUpdate;
exports.isVisitToToday = isVisitToToday;
exports.getStatsOfVisits = getStatsOfVisits;
exports.getTerminalStats = getTerminalStats;
exports.getServerFilesContent = getServerFilesContent;
exports.writeContentToServerFile = writeContentToServerFile;
exports.doesObjectExist = doesObjectExist;
exports.doesArrayExist = doesArrayExist;
exports.doesVariableExist = doesVariableExist;
exports.validateArrayParameter = validateArrayParameter;
exports.isEmptyValue = isEmptyValue;
exports.isEmptyArray = isEmptyArray;
exports.isNotEmptyArray = isNotEmptyArray;
exports.isEmptyObject = isEmptyObject;
exports.isNotEmptyValue = isNotEmptyValue;
exports.isNotEmptyObject = isNotEmptyObject;
exports.checkTransactionErrorMessage = checkTransactionErrorMessage;
exports.hashString = hashString;
exports.formalizeXcelData = formalizeXcelData;
exports.validateProcessedUsersFromXcelFile = validateProcessedUsersFromXcelFile;
exports.convertExcelSerialDateToDateTimeString = convertExcelSerialDateToDateTimeString;