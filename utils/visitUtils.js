const { execProcedureInTransaction, executeProc } = require("../db/dbAccess");
const { getDatabaseFieldsOfAllObjects, getUserById, hasVisitBeenAccepted, hasVisitBeenCanceled, hasVisitBeenRejected, isUserThread, isUserCoreSec, isUserHost } = require("./controllerHelper")
const { extractFieldsFromObject, isEmptyArray, getToday, isNotEmptyArray, isEmptyObject, isNotEmptyValue, isEmptyValue, doesArrayExist, removePropertyFromObject, isNotEmptyObject } = require("./utils");
const randomString = require("randomstring");
const config = require("config");
const { getDbObjectIdFromResult } = require("../db/dbHelpers");
const { saveUserFromVisitWhenNonExistent, getUserByUniqueData, sendAccountCredentialsToUser, generateUserName, addDefaultPasswordOfInternalUsers } = require("./authUtils");
const { getNewVisitForHostNotificationMessage, buildNotificationMessageOptions, getVisitorGuestNotificationMessage, getVisitorGuesAccountCredentialsNotificationMessage, getVisitAcceptedNotification, getVisitCanceledNotification, getVisitRejectedNotification } = require("./NotificationMessageBuilder");

const { convertVisitFieldsToProcedureFieldsAndGroups } = require("./responseHandlers");
const logger = require("./logger");
const { getDatabaseConnection } = require("../db/dbConnector");

exports.saveVisitorVisitDataOnly = async (visitReqData, transaction) => { 
    const visitDbId = await saveVisitUsingProcedure('proc_add_svisit_visitors', visitReqData, transaction);
    return visitDbId;
}

exports.saveHostVisitDataOnly = async (visitReqData, transaction) => { 
    const visitDbId = await saveVisitUsingProcedure('proc_add_svisit_hosts', visitReqData, transaction);
    return visitDbId;
}

const saveVisitUsingProcedure = async (proceureName, visitReqData, transaction) => { 
    const visitData = extractFieldsFromObject(visitReqData, getDatabaseFieldsOfAllObjects().request);
    addAccessCodeToVisit(visitData);
    const result = await execProcedureInTransaction(proceureName,visitData, transaction);
    const visitDbId = getDbObjectIdFromResult(result);
    return visitDbId;
}

const addAccessCodeToVisit = (visitReqData) => { 
    visitReqData.p_code = randomString.generate(config.get("others.visitCodeSize"));
}

exports.saveVisitMaterialsOnly = async (visitDbId, visitReqData, transaction) =>  {
    const visitMaterials = getVisitMaterialsArrayFromVisitData(visitReqData);
    if(!isEmptyArray(visitMaterials)) {
        prepareVisitMaterialsDataForSave(visitDbId, visitMaterials, visitReqData);
        for(let visitMaterial of visitMaterials) 
            await execProcedureInTransaction('proc_add_svextra', visitMaterial, transaction);
    }
}

const getVisitMaterialsArrayFromVisitData = (visitReqData) => { 
    let visitExtras = [];
    const defaultMaterialsFields = {p_is_active: 1, p_dtm: getToday().toISOString() }
    if(isEmptyArray(visitReqData.extraArray)) { 
        visitExtras = [{
            ...extractFieldsFromObject(visitReqData, getDatabaseFieldsOfAllObjects().requestExtra),
            ...defaultMaterialsFields
        }];
    } else { 
        visitExtras = visitReqData.extraArray.map(extra => { 
            return {
                ...extractFieldsFromObject(extra, getDatabaseFieldsOfAllObjects().requestExtra), 
                ...defaultMaterialsFields
            }
        });
    }
    visitExtras = visitExtras.filter(vis => vis.p_oname);
    return visitExtras;
}


exports.updateVisitDataOnly = async (visit, transaction) => { 
    const visitData = extractFieldsFromObject(visit, getDatabaseFieldsOfAllObjects().request);
    visitData.p_id = visit.p_id;
    await execProcedureInTransaction('proc_edit_svisit', visitData, transaction);
}


exports.updateVisitMaterialsOnly = async (visit, transaction) => { 
    const {p_svisit, p_cod_user} = visit;
    const today = getToday().toISOString();
    if(doesArrayExist(visit.extraArray) && visit.p_oname) {
        const toSend = removePropertyFromObject(visit, 'extraArray');
        if(toSend.p_id){ 
            await execProcedureInTransaction('proc_edit_svextra', {...toSend, p_dtm:today }, transaction);
        } else { 
            await execProcedureInTransaction('proc_add_svextra', {...toSend, p_svisit, p_dtm: today, p_is_active: 1 }, transaction);
        }
    } else { 
        for(let extra of visit.extraArray) {
            let toSend;
            if(extra.p_id || extra.p_id_e){
                toSend = {...extra, p_id: extra.p_id_e? extra.p_id_e: extra.p_id}
                toSend = removePropertyFromObject(toSend, 'p_id_e')
                await execProcedureInTransaction('proc_edit_svextra', {...toSend, p_svisit, p_dtm: today, p_cod_user }, transaction);
            } else { 
                await execProcedureInTransaction('proc_add_svextra', {...extra, p_svisit, p_dtm: today, p_is_active: 1 }, transaction);
            }
        };
    }
}

exports.resetVisitState = async (visit, colaborator, transaction) => { 
    if(isNotEmptyObject(colaborator)) {
        let stateUpdate = { 
            p_int: 0,
            p_id_svisit: visit.p_id,
            p_id_htc: visit.p_id_host,
            p_bool_htc: null,
            p_dtm_htc: getToday().toISOString()
        }
        const isToUpdateCurrentInstance = !visit.p_internal && !(visit.p_cod_user === visit.p_id_host);

        if (isUserHost(colaborator)) { 
            if (isToUpdateCurrentInstance)
                await execProcedureInTransaction('proc_edit_vstate_state_htc', stateUpdate, transaction);
            await execProcedureInTransaction('proc_edit_vstate_state_htc', {...stateUpdate, p_int: 1}, transaction);
            await execProcedureInTransaction('proc_edit_vstate_state_htc', {...stateUpdate, p_int: 2}, transaction);
        }
        else if(isUserThread(colaborator)) { 
            if (isToUpdateCurrentInstance)
                await execProcedureInTransaction('proc_edit_vstate_state_htc', {...stateUpdate, p_int: 1}, transaction);
            await execProcedureInTransaction('proc_edit_vstate_state_htc', {...stateUpdate, p_int: 2}, transaction);
        } else if (isUserCoreSec(colaborator)){
            if (isToUpdateCurrentInstance)
                await execProcedureInTransaction('proc_edit_vstate_state_htc', {...stateUpdate, p_int: 2}, transaction);
        }

        logger.debug(`Visit ${visit.p_id} states have been reset!`);
    }
}

const prepareVisitMaterialsDataForSave = (visitDbId, visitExtraArray, visitReqData) => { 
    visitExtraArray.forEach( visitExtra => { 
        visitExtra.p_svisit = visitDbId;
        if(visitExtra.p_cod_user < 0 || isEmptyValue(visitExtra.p_cod_user))
            visitExtra.p_cod_user = visitReqData.p_id_x;
    });
}

exports.saveVisitorsToVisitLink = async (visitDbId, visitReqData, transaction) =>  { 
    if(isToLinkHostToVisitorVisit(visitReqData))
        await saveVisitorVisitData(visitDbId, visitReqData.p_id_x, transaction);
    if(isNotEmptyArray(visitReqData.p_users))
        await linkVisitorGuestsToVisit(visitDbId, visitReqData.p_users, transaction);
}

const linkVisitorGuestsToVisit = async (visitDbId, visitorGuestsArray, transaction) => { 
    const registeredUsers = [];
    
    await generateUsernameForUsers(visitorGuestsArray);
    for (let guest of visitorGuestsArray) {
        if (guest.error){
            logger.error(`${guest.p_fname} ${guest.p_lname} cannot be associated to visit because of invalid data (vid: ${visitDbId})`)
            throw new Error(`Could not create visit. ${guest.p_fname} ${guest.p_lname} cannot be associated to visit because of invalid data.`)
        }
        
        const [guestDbId, wasExistent] = await saveUserFromVisitWhenNonExistent(guest, transaction);

        if (registeredUsers.includes(guestDbId)) { 
            logger.warn(`Attemped dublicate link of user ${guest.p_fname} ${guest.p_lname} for visit ${visitDbId}`);
            continue;
        }

        await saveVisitorVisitData(visitDbId, guestDbId, transaction);
        registeredUsers.push(guestDbId);
        logger.debug(`User ${guest.p_fname} ${guest.p_lname} was linked to visit ${visitDbId}`);
        if(!wasExistent) { 
            logger.debug(`Sending account credentials to ${guestDbId}...`);
            sendVisitorGuestNewAccountCredentials(guest);
        }
     
    }
}

const generateUsernameForUsers = async (users) => { 
    const dbCon = await getDatabaseConnection();
    for(let user of users) { 
        user.p_uname = await generateUserName(user, dbCon);
    }
};

const saveVisitorVisitData = async (visitDbId, visitorDbId, transaction) => { 
    const visitVisitorDbEntry = getVisitVisitorDbEntryObject(visitDbId, visitorDbId);
    await execProcedureInTransaction('proc_add_visitor', visitVisitorDbEntry, transaction);
    await execProcedureInTransaction('proc_add_visit', {p_id_x: visitorDbId, p_id_svisit: visitDbId}, transaction);
}

const getVisitVisitorDbEntryObject = (visitDbId, visitorDbId) => { 
    return {
        p_id_svisit: visitDbId,
        p_id_x: visitorDbId,
        p_is_active: 1,
        p_induction: 1
    }
}

const isToLinkHostToVisitorVisit = (visitReqData) => { 
    let isToLink = !visitReqData.p_internal;
    isToLink = isToLink || visitReqData.p_fast;
    if(isNotEmptyArray(visitReqData.id_users)) 
        isToLink = isToLink && !visitReqData.id_users.includes(visitReqData.p_id_x);
    if(isNotEmptyArray(visitReqData.id_users)) //::>> For service providers request. force to only link users form p_users array
        isToLink = false;
    return isToLink;
}

const sendNewVisitNotificationToHostIfNecessary = async (databaseConnection, visitReqData) => { 
    try {
        if(isToSendNewVisitNotification(visitReqData))
            await sendNewVisitNotification(databaseConnection, visitReqData);
        
    } catch (error) {
        logger.error(`Could not send Notification to host : ${error.message}`);
    }
}

const sendNewVisitNotification = async (databaseConnection, visitReqData) => { 
    const visitor = await getUserById(databaseConnection, visitReqData.p_id_x);
    const destinationUser = await getUserById(databaseConnection, visitReqData.p_id_host);
    const emailNdTextMessageToSend = getNewVisitForHostNotificationMessage(visitor);
    const notificationOptions = buildNotificationMessageOptions(destinationUser, emailNdTextMessageToSend);
    const notificationSender = getNotificationSender();
    notificationSender.sendNotificationToAllChannels(notificationOptions);  
    
}

const isToSendNewVisitNotification = (visitReqData) => { 
    return !visitReqData.p_internal
}

const sendNewVisitNotificationToVisitorGuestsIfNecessary = async (databaseConnection, visitReqData) => { 
    if(isNotEmptyArray(visitReqData.p_users)) {
        const hostVisitor = await getUserById(databaseConnection, visitReqData.p_id_x);
        const notificationSender = getNotificationSender();
        visitReqData.p_users.forEach(async visitorGuestData => {
            let visitorGuest = await getUserByUniqueData(visitorGuestData, databaseConnection);
            //::>> When it uses visitorGuestData it meaans its a new user, so there's no risk of sending to an unintended user
            visitorGuest = isEmptyObject(visitorGuest)? visitorGuestData : visitorGuest;
            const emailNdTextMessageToSend = getVisitorGuestNotificationMessage(hostVisitor, visitorGuest);
            const notificationOptions = buildNotificationMessageOptions(visitorGuest, emailNdTextMessageToSend);
            notificationSender.sendNotificationToAllChannels(notificationOptions);
        })
    }
}

const sendVisitorGuestNewAccountCredentials = (visitorGuest) => { 
    sendAccountCredentialsToUser(visitorGuest);
}

const getNotificationSender = () => { 
    const Notification = require("./Notification");
    return new Notification();
}

exports.prepareVisitorFastVisitForSave = async (userId, visitData) => { 
    visitData.p_id_x = userId;
}

exports.sendNotificationToUsers = (dbConnection, visitReqData) => { 
    sendNewVisitNotificationToHostIfNecessary(dbConnection, visitReqData);
    sendNewVisitNotificationToVisitorGuestsIfNecessary(dbConnection, visitReqData);
}

exports.getVisitByDBId = async (visitDBId) => { 
    const dbCon = await getDatabaseConnection();
    const result = await executeProc({p_svisit: visitDBId}, dbCon, "proc_get_svisit_vstates_by_id");
    return result[0];
}

const getMembersOfVisit = async (visitDbId) => { 
    const dbCon = await getDatabaseConnection();
    const membersDbIds = await executeProc({p_svisit: visitDbId}, dbCon, 'proc_get_selected_visitors');
    const members = [];
    for(let {int_id_x} of membersDbIds){ 
        const member = await getUserById(dbCon, int_id_x);
        members.push(member);
    }
    return members;
}

exports.getVisitorsFromVisit = async (visit) => { 
    const membersOfVisit = await getMembersOfVisit(visit.cod);
    const visitorsOfvisit = membersOfVisit.filter(member => member.cod !== visit.st_host_cod);
    return visitorsOfvisit;
}

exports.sendNotificationToUsersByVisitState = async (visit, visitors) => { 

    try {
        const visitWithStats = convertVisitFieldsToProcedureFieldsAndGroups([visit])[0];
        const notificationSender = getNotificationSender();
        let emailNdTextMessageToSend;
    
        if (hasVisitBeenAccepted(visitWithStats)) { 
            emailNdTextMessageToSend = getVisitAcceptedNotification(visitWithStats);
        } else if (hasVisitBeenCanceled(visitWithStats)) { 
            emailNdTextMessageToSend = getVisitCanceledNotification(visitWithStats);
        } else if (hasVisitBeenRejected(visitWithStats)) { 
            emailNdTextMessageToSend = getVisitRejectedNotification(visitWithStats);
        }
        
        if(isNotEmptyValue(emailNdTextMessageToSend)){
            visitors.forEach(visitor => { 
                const notificationOptions = buildNotificationMessageOptions(visitor, emailNdTextMessageToSend);
                notificationSender.sendNotificationToAllChannels(notificationOptions);
            })
        }
        
    } catch (error) {
        logger.error(error.message);
    }
   
}


exports.sendFastVisitorAccountCredentials = sendVisitorGuestNewAccountCredentials;
