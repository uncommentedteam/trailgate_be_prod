const {getVisitorGuestNotificationMessage} = require("./NotificationMessageBuilder");
const {USER} = require("./constants");
const {convertDatabaseFieldstoProcedureFields} = require("./responseHandlers");
const {getUserById} = require("./controllerHelper");
const {Notification} = require("../utils/Notification")

class RequestNotificationHelper {

    /**
     *
     * Send notification to visitor to let him know that the user was created
     * 
     * */
    async sendNotificationRequestCreatedToVisitor(dbCon, hostId, visitorId) {
        const hostResult = await getUserById(dbCon, hostId);
        const visitorResult = await getUserById(dbCon, visitorId);
        
        const host = convertDatabaseFieldstoProcedureFields(hostResult, USER);
        const visitor = convertDatabaseFieldstoProcedureFields(visitorResult, USER);
        
        const {emailMessage, textMessage, subject} = getVisitorGuestNotificationMessage(host, visitor);
        
        let notification = new Notification(dbCon);
        
        await notification.createSmsNotificationOptions(visitor.str_pnumber_1, textMessage);

        const email = visitor.str_email ? visitor.str_email : null; 
        if (email) {
            await notification.createEmailNotificationOptions(email, subject, emailMessage);
        }
        
    }
}

module.exports = new RequestNotificationHelper();
