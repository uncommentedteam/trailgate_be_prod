const RandomString = require('randomstring')
//`Method to set the parameters to allow the creation os public service`

exports.setParam = async(req) => {
    const url = req.protocol + '://'+req.get('host')
	const imgPath = url + "/userIds/"
	let parm = req.body
	let lfNames = []
	if (req.files){
		if(req.files.length){
			for (let file of req.files){
				lfNames.push(imgPath + file.filename)
			}
		} else{
			lfNames.push(imgPath + req.file.filename)
		}
		parm['p_path'] = "["+String(lfNames)+"]"
	}
	const code = RandomString.generate(6)
    parm['p_code'] = code
    return parm
}
