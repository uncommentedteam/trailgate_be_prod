const nodemailer = require('nodemailer');
const { writeToErrorLog } = require('./utils');
const { email_env ,fe_server_address, nodemailer_service, nodemailer_user, nodemailer_password, support_email } = require('../config/environment');
const logger = require('./logger');
// ::: FALSE: DEV CONFIGURATION | TRUE : PRODUCTION CONFIGURATION
const SERVICE = +email_env? undefined:nodemailer_service;
const AUTH = +email_env? undefined:{user: nodemailer_user,pass: nodemailer_password};
const NODEMAILER_OPTIONS = {
    service: SERVICE,
    auth: AUTH,
    host: nodemailer_service,
    port: 25, 
    debug: true,
    logger:true,
    secure: false, 
    tls:{
        rejectUnauthorized: false
    }
}

/**
 * Sends the contact us concern to the app support email.
 * @param {string} userEmail 
 * @param {string} detail 
 */
exports.sendContactUsEmail = async (userEmail, details) => {

    const transporter = nodemailer.createTransport(NODEMAILER_OPTIONS);

    let mailOptions = {
        from: nodemailer_user,
        to: support_email,
        subject: "Email de Suporte",
        html: ` 
            <body>     
                <h1>Email de Suporte</h1>
                <h2>From: ${userEmail} </h2>
                <p>${details}</p>
            </body>`}

    const info = await transporter.sendMail(mailOptions);
    logger.info(`Email sent to ${userEmail} with mid: ${info.messageId}`);
}


/**
 * Sends email for the company registration
 * @param {string} email 
 * @param {string} token 
 */
exports.sendCompRegEmail = async (email, token) => { 

    try {
        const transporter = nodemailer.createTransport(NODEMAILER_OPTIONS);
        let mailOptions = {
            from: nodemailer_user,
            to: +email_env? [email,support_email] : [support_email],
            subject: "Registro da Empresa/Servico",
            html: ` 
                <body>     
                    Receba aqui o link do registro da empresa
                    <br>
                    <br>
                    <p>${fe_server_address + "/" + "registerEntity" + "/" + token}</p>
                </body>`}
    
    
        const info = await transporter.sendMail(mailOptions);
        logger.info(`Company Registration Email sent to ${email} with mid: ${info.messageId}`);

    } catch (error) {
        writeToErrorLog(error, "mailSender.js", "sendCompRegEmail");
    }
  
    
}