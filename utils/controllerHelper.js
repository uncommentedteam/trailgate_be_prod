const config = require("config");
const { CANCELED, REJECTED, USER_FIELDS_PROCEDURE_FIELDS_MAP, USER_TYPES_STORE_KEY, ACCEPTED, HOST, THREAD, CORE_SEC} = require("./constants");
const store = require('store2');

/**
 * Gets the user in the database by its document Id
 * @param {object} databaseConnection 
 * @param {string} documentId 
 */
exports.getUserbyDocumentId = async (databaseConnection, documentId) => {
	const {doesVariableExist, doesObjectExist, writeToErrorLog, getErrorMessageByErrorId} = require('../utils/utils');
	if(!doesObjectExist(databaseConnection) || !doesVariableExist(documentId)) return {};
	const result = await databaseConnection.query(`
        SELECT *
        FROM view_user
        WHERE nid = '${documentId}'
    `);
	return result.recordset[0];
};

/**
 * Gets the user in the database by its first phone number
 * @param {object} databaseConnection 
 * @param {number} phoneNumber1 
 */
exports.getUserByPhoneNumber1 = async (databaseConnection, phoneNumber1) => {
	const {doesObjectExist, doesVariableExist} = require('../utils/utils');
	if(!doesObjectExist(databaseConnection) || !doesVariableExist(phoneNumber1)) return {};
	const result = await databaseConnection.query(`
        SELECT *
        FROM view_user
        WHERE pnumber_1 = '${phoneNumber1}'
    `);
	return result.recordset[0];
};

/**
 * Gets the user data in the database by the user id.
 * @param {databaseConnection} databaseConnection 
 * @param {number} userId 
 */
exports.getVisitorById = async(databaseConnection, userId) => {
	const {doesObjectExist, doesVariableExist} = require('../utils/utils');
	if(!doesObjectExist(databaseConnection) || !doesVariableExist(userId)) return {};
    let result = await databaseConnection.query(`
		SELECT *
		FROM view_user_visitor
		WHERE cod = ${userId}
`)

    return result.recordset[0];
}

/**
 * Gets the user in the database by its main phone number
 * @param {object} databaseConnection 
 * @param {string} documentId 
 */
exports.getVisitorByDocumentId = async(databaseConnection, documentId) => {
    let result = await databaseConnection.query(`
		SELECT *
		FROM view_user_visitor
		WHERE nid = '${documentId}'
		`)
    return result.recordset[0];
}

/**
 * Gets the user in the database by its email
 * @param {object} databaseConnection 
 * @param {string} email 
 */
exports.getUserByEmail = async(databaseConnection, email) => {
    let result = await databaseConnection.query(`
	SELECT *
	FROM view_user
	WHERE email = '${email}'
	`)
    return result.recordset[0];
}

/**
 * Gets a visit extra (material or vehicle) by its database id.
 * @param {object} databaseConnection 
 * @param {number} extraId 
 */
exports.getExtraById = async(databaseConnection, extraId) => { 
	let result = await databaseConnection.query(`
    SELECT *
    FROM tbl_extra
    WHERE int_id = ${extraId}
    `)
    return result.recordset[0];
}

/**
 * Gets the user by its database id.
 * @param {object} databaseConnection 
 * @param {number} userId 
 */
exports.getUserById = async(databaseConnection, userId) => {
    let result = await databaseConnection.query(`
    SELECT *
    FROM view_user
    WHERE cod = ${userId}
    `)
    return result.recordset[0];
}

/**
 * Unifies all the visits registries retrieved from the database of visits that are grouped. This avois repetition of data of visits that only the id of the visitor changes.
 * @param {array} visits 
 */
exports.unifyGroupedVisits = (visits) => {
	const {doesArrayExist} = require('../utils/utils');
	if (!doesArrayExist(visits)) return [];

	const unifyVisitsRecursively = (visits, arrayOfUniqueVisitCodes, unifiedVisits, currentVisitIndex) => {
		if (currentVisitIndex === visits.length) return unifiedVisits;
		const currentVisit = visits[currentVisitIndex];

		if (arrayOfUniqueVisitCodes.includes(currentVisit.cod)) {
			const indexInUniqueVisitCodes = arrayOfUniqueVisitCodes.indexOf(currentVisit.cod);
			const visit = unifiedVisits[indexInUniqueVisitCodes];
			
			if (visit.x_cods && visit.x_cods.includes(currentVisit.x_cod)) {
				//::>> If the visitor code already exists in the unified visit, it skips.
				return unifyVisitsRecursively(visits, arrayOfUniqueVisitCodes, unifiedVisits, currentVisitIndex + 1);
			} else {
				//::>> Checks if its the id of the owner of the visit, if it is it skips.
				if (visit.x_cod === currentVisit.x_cod) return unifyVisitsRecursively( visits, arrayOfUniqueVisitCodes, unifiedVisits,currentVisitIndex + 1,); 

				//::>> If the visitor code does not exist, it joins or unifies the visits
				let _ = visit.grouped? visit.grouped.push(currentVisit) : (visit.grouped = [currentVisit]);
				let __ = visit.x_cods? visit.x_cods.push(currentVisit.x_cod) : (visit.x_cods = [currentVisit.x_cod]);
				return unifyVisitsRecursively(visits, arrayOfUniqueVisitCodes, unifiedVisits, currentVisitIndex + 1);
			}
		} else {
			return unifyVisitsRecursively(visits,[...arrayOfUniqueVisitCodes, currentVisit.cod],[...unifiedVisits, currentVisit],currentVisitIndex + 1,);
		}
	};
	addCurrentVisitAsGrouped(visits);
	return unifyVisitsRecursively(visits, [], [], 0);
};

const addCurrentVisitAsGrouped = (visits) => { 
	visits.forEach(visit => { 
		visit.grouped = visit.grouped? visit.grouped.push({...visit}) : [{...visit}];
		// if(!visit.sv_is_internal) { 
		// }
	});
}

/**
 * Gets the colaborator in the database by the colaborator id
 * @param {object} databaseConnection 
 * @param {number} colaboratorId 
 */
exports.getColaboratorById = async (databaseConnection, colaboratorId) => {
	let result = await databaseConnection.query(`
    SELECT *
    FROM view_user_funcionario
    WHERE cod = ${colaboratorId}
    `);
	return result.recordset[0];
};

exports.getVisitorOrHostById = async (dbCon, userId) => {
	let result = await this.getVisitorById(dbCon, userId);
	if(!result) 
		result = await getColaboratorById(dbCon, userId);
	return result;
}


/**
 * Verifies if the user is a Terminal Security User.
 * @param {objec} user 
 */
exports.isUserThread = (user) => {
	try {
		const storeValue = store.get(USER_TYPES_STORE_KEY);
    	return user.cod_role === storeValue.THREAD_CODE;
	} catch (error) {
		return user.cod_role === 52;
	}
	
}

/**
 * Verifies if the user is a Department of Homeland Security Manager.
 * @param {objec} user 
 */
exports.isUserCoreSec = (user) => {
	try {
		const storeValue = store.get(USER_TYPES_STORE_KEY);
		return user.cod_role === storeValue.CORE_SECURITY_CODE;
	} catch (error) {
		return user.cod_role === 53;
	}

}

/**
 * Verifies if the user is a Security Officer.
 * @param {objec} user 
 */
exports.isUserGateKeeper = (user) => {
	try {
		const storeValue = store.get(USER_TYPES_STORE_KEY);
    	return user.cod_role === storeValue.GATEKEEPER_CODE;
	} catch (error) {
		return user.cod_role === 55;
	}
	
}

/**
 * Verifies if the user is a Colaborator.
 * @param {objec} user 
 */
exports.isUserHost = (user) => {
	try {
		const storeValue = store.get(USER_TYPES_STORE_KEY);
    	return user.cod_role === storeValue.COLABORATOR_CODE;
	} catch (error) {
		return user.cod_role === 51;
	}
	
}




/**
 * Returns an object containing each entitity (table in a database) with an array of his fields
 */
exports.getDatabaseFieldsOfAllObjects = () => {
	return {
		request: [
			"p_id_host",
			"p_id_department",
			"p_id_company",
			"p_date_time",
			"p_detail",
			"p_submited",
			"p_rule",
			"p_period",
			"p_id_rpp",
			"p_code",
			"p_multi_entry",
			"p_access",
			"p_tocompany",
			"p_fast",
			"p_id_gate",
			"p_internal",
			"p_id_subcompany",
		],
		requestExtra: [
			"p_svisit",
			"p_oname",
			"p_qnt",
			"p_ismtrl",
			"p_is_active",
			"p_cod_user",
			"p_dtm",
		],
		user: [
			"p_fname",
			"p_lname",
			"p_nid",
			"p_uname",
			"p_psw",
			"p_email",
			"p_pnumber_1",
			"p_pnumber_2",
			"p_active",
			"p_isvisitor",
			"p_nationality",
			"p_id_type_id",
			"p_expiration_date_id",
			"p_is_toEmail",
		],
		userDetails: [
			"p_isvisitor",
			"p_id_user",
			"p_registry",
			"p_id_entity",
			"p_id_provenance",
			"p_istemp",
			"p_id_role",
			"p_id_company",
			"p_id_department",
			"p_tokken",
			"p_isnotification",
		],
		visitor: ["p_id_svisit", "p_id_x", "p_induction", "p_is_active"],
		company: [
			"p_cname",
			"p_cemail",
			"p_cpnumber_1",
			"p_is_ps",
			"p_cpnumber_2",
			"p_cpsw",
			"p_cport",
			"p_cactive",
			"p_id_cuser",
			"p_dmt",
			"p_cod_core",
		],
		alertMsg: ["p_id", "p_msg", "p_title", "p_start", "p_end", "p_active"],
	};
};

/**
 * Verifies if a visit has been canceled.
 * @param {object} visit 
 */
exports.hasVisitBeenCanceled = visit => {
    for(let state of Object.values(visit.states)){
        if(state === CANCELED) return true;
      }
      return false;
}

/**
 * Verifies if a visit has been rejected.
 * @param {object} visit 
 */
exports.hasVisitBeenRejected = visit => {
    for(let state of Object.values(visit.states)){
        if(state === REJECTED){
          return true;
        }
      }
      return false;
}
/**
 * Verifies if a visit has been accepted.
 * @param {object} visit 
 */
exports.hasVisitBeenAccepted = visit => {
    let verdict = true;
	verdict = (visit.states[HOST] === ACCEPTED) && verdict
	verdict = (visit.states[THREAD] === ACCEPTED) && verdict
	verdict = (visit.states[CORE_SEC] === ACCEPTED) && verdict
	
    // for(let state of Object.values(visit.states)){
    //     verdict = (state === ACCEPTED) && verdict;
    //   }
    return verdict;
}


/**
 * Filters all the visitis that are happening
 * @param {object} visits 
 */
exports.getVisitsHappening = (visits) => {
    const newVisits = visits.filter(visit => {
        if (visit.x_is_in) {
            return true;
        } else if (visit.grouped && visit.grouped.length > 0) { 
            for( let vis of visit.grouped){
                if(vis.x_is_in) return true;
            }
        }
    })
    return newVisits;
}

/**
 * Filters all the visitis that are being received (that have and entrance and leave)
 * @param {object} visits 
 */
exports.getRecievedVisits = (visits) => {
    const newVisits = visits.filter(visit => {
        if (visit.x_is_in !== null) {
            return true;
        }
    })
    return newVisits;
}

/**
 * Counts all the visitors that are currently inside
 * @param {object} visits 
 */
exports.getNumberVisitors = (visits) => {
    let number = 0;
    visits.forEach(visit => {
        if (visit.grouped) {
            number = visit.x_is_in ? number + 1 : number;
            visit.grouped.forEach(vs => {
                if (vs.x_is_in) number = number + 1;
            })
        } else {
            number = visit.x_is_in ? number + 1 : number;
        }
    })
    return number;
}

/**
 * Filters all the visitis that will happen
 * @param {object} visits 
 */
exports.getVisitsToHappen = (visits) => {
    const newVisits = visits.filter(visit => {
        if (visit.x_is_in === null) return true;
    })
    return newVisits;
}

/**
 * Generate data for chart representing visits by the day
 * @param {object} visits 
 */
exports.getChartData = (visits) => {
	const dtsCount = {};
	const {getToday, getMomentFromString} = require('../utils/utils');
    let visitDt;
    const now = getToday();
    for (let i = 1; i <= 31; i++) {
        dtsCount[`${now.year()}-${now.month()}-${i}`] = 0;
    }

    visits.forEach(visit => {
        visitDt = getMomentFromString(visit.schedule_date_time);
        dtsCount[`${visitDt.year()}-${visitDt.month()}-${visitDt.date()}`]++;
    })

    const chartData = Object.keys(dtsCount).map((chDt, index) => {
        return { x: index + 1, y: dtsCount[chDt] }
    })

    return chartData;
}

/**
 * Sends a response with status 500 and writes the error to the logs.
 * @param {object} res 
 * @param {object} error 
 * @param {string} fileName 
 * @param {string} methodName 
 */
exports.sendResponseError = (res, error, fileName, methodName) => { 
	const {writeToErrorLog, getErrorMessageByErrorId} = require('../utils/utils');
	writeToErrorLog(error, fileName, methodName);
	const message = getErrorMessageByErrorId(error);
	res.status(500).json({root_err: message, error: "Server Error!", });
}