const express = require('express');
const dbMiddleware = require('../middleware/connectionAdderMiddleware');
const { check, oneOf,validationResult } = require('express-validator');
const aControllers = require('../controllers/auth');
const imgExtractor = require('../middleware/imgFile')
const { validateRequestFields } = require('../middleware/globalMiddleware');

const router = express.Router();

//::>> PREVIOUS VERSION
/**
 * Endpoint to get a specific user by the id
 * @route       auth/getUser
 * @method      POST
 */
router.post('/getUser', [
    check('p_id').isInt().exists()
], dbMiddleware, validationResult, aControllers.getUser);

//::>>>

/**
 * Endpoint to login.
 * @route       auth/login
 * @method      POST
 */
router.post('/login',[
    check('p_isvisitor').isInt().exists(),
    check('p_email_pnumber').isString().exists(),
    check('p_psw').exists()
], dbMiddleware, validateRequestFields, aControllers.login); 


/**
 * Endpoint to get the user by token.
 * @route       auth/getUserByTkn
 * @method      POST
*/
router.post('/getUserByTkn', [
    check('p_tokken').isString().exists(),
], dbMiddleware, validateRequestFields, aControllers.getUserByToken); 


/**
 * Endpoint to register a new user (visitors or callaborators).
 * @route       auth/regUser
 * @method      POST
 */
router.post('/regUser', [
    check('p_fname').isString().not().isEmpty(),
    check('p_lname').isString().not().isEmpty(),
    check('p_nid').isString().not().isEmpty(),
    check('p_psw').isString().not().isEmpty(),
    check('p_pnumber_1').not().isEmpty(),
    check('p_pnumber_2').not().isEmpty(),
    check('p_active').isInt().exists(),
    oneOf([
        check('p_email').isEmpty(),
        check('p_email').isEmail(),
    ]),
    check('p_nationality').isString().not().isEmpty(),
    check('p_id_type_id').isInt().exists(),
    check('p_expiration_date_id').isString().not().isEmpty(),
    check('p_isvisitor').isInt().exists(),
    check('p_registry').isString().not().isEmpty(),
], dbMiddleware, validateRequestFields, aControllers.regUser);

/**
 * Endpoint to register a group of users (visitors).
 * @route       auth/regUser
 * @method      POST
 */
router.post('/regBulkUsers', [
    check('users').isArray(),
], dbMiddleware, validateRequestFields, aControllers.regBulkUsers);


router.post('/grantAcc', [
    check('p_fname').isString().not().isEmpty(),
    check('p_lname').isString().not().isEmpty(),
    check('p_nid').isString().not().isEmpty(),
    // check('p_psw').isString().not().isEmpty(),
    check('p_pnumber_1').not().isEmpty(),
    check('p_pnumber_2').not().isEmpty(),
    check('p_active').isInt().exists(),
    oneOf([
        check('p_email').isEmpty(),
        check('p_email').isEmail(),
    ]),
    check('p_nationality').isString().not().isEmpty(),
    check('p_id_type_id').isInt().exists(),
    check('p_expiration_date_id').isString().not().isEmpty(),
    check('p_isvisitor').isInt().exists(),
    check('p_id_role').exists(),
    check('p_registry').isString().not().isEmpty(),
], dbMiddleware, validateRequestFields, aControllers.grantAcccess);

router.post('/grantAccCol', dbMiddleware, aControllers.grantAccessBulk);


/**
 * Endpoit      to resend the verification email for the visitor who is about to register.
 * @route auth/resVer
 * @method      POST
 */
router.post('/resVer', [
    check('p_nid').isString().not().isEmpty(),
], dbMiddleware, validateRequestFields, aControllers.resendVerificationEmail);

/**
 * Endpoint to edit the main information about the user.
 * @route       auth/editUser
 * @method      POST
 */
router.post('/editUser', [
    check('p_id').isInt().exists(),
    check('p_fname').isString().not().isEmpty(),
    check('p_lname').isString().not().isEmpty(),
    check('p_nid').isString().not().isEmpty(),
    check('p_psw').isString().not().isEmpty(),
    check('p_email'),
    check('p_pnumber_1'),
    check('p_pnumber_2'),
    check('p_active').isInt().exists(),
    check('p_nationality').isString().not().isEmpty(),
    check('p_id_type_id').isInt().exists(),
    check('p_expiration_date_id').isString().not().isEmpty(),
], dbMiddleware, validateRequestFields, aControllers.editUser);

/**
 * Endpoint     to edit the main information about the user.
 * @route       auth/editUser_v2
 * @method      POST
 * @deprecated
 */
router.post('/editUser_v2', [
    check('p_id').isInt().exists(),
    check('p_fname').isString().not().isEmpty(),
    check('p_lname').isString().not().isEmpty(),
    check('p_nid').isString().not().isEmpty(),
    check('p_psw').isString().not().isEmpty(),
    check('p_email'),
    check('p_pnumber_1'),
    check('p_pnumber_2'),
    check('p_active').isInt().exists(),
    check('p_nationality').isString().not().isEmpty(),
    check('p_id_type_id').isInt().exists(),
    check('p_expiration_date_id').isString().not().isEmpty(),
], dbMiddleware, validateRequestFields, aControllers.editUser_v2);

/**
 * Endpoit      to verify the user.
 * @route auth/verifyUSer
 * @method      POST
 */
router.post('/verifyUser', [
    check('p_id').exists().isInt(),
    check('p_fname').exists().isLength({min: 3}),
    check('p_lname').exists().isLength({min: 3}),
    check('p_nid').exists().isLength(),
    check('p_psw').exists().isLength({min: 6}),
    check('p_pnumber_1').exists().not().isEmpty(),
    check('p_pnumber_2').exists().not().isEmpty(),
    check('p_active').exists().isInt()
], dbMiddleware, validateRequestFields, aControllers.verification);

/**
 * Endpoit      to create a user for public services.
 * @route auth/publicService
 * @method      POST
 */
router.post("/create_PublicService",[
    check('p_fname').exists(),
    check('p_lname').exists(),
    check('p_pnumber_1').exists(),
    check('p_nid').exists(),
    check('p_id_type_id').exists(),
    check('p_cod_company').exists(),
    check('p_cod_gate').exists(),
    check('p_cod_rpp').exists(),
    check('p_cod_gk').exists(),
    check('p_cod_visitor')
    ], 
    
    imgExtractor,
    dbMiddleware, 
    aControllers.create_PublicService)
module.exports = router;
