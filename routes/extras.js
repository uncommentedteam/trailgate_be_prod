const express = require('express');
const { check } = require('express-validator');
const dbMiddleware = require('../middleware/connectionAdderMiddleware');
const eControllers = require('../controllers/extras');
const { validateRequestFields } = require('../middleware/globalMiddleware');
const router = express.Router();

//::>> PREVIOUS VERSION
/**
 * Endpoint to add alert messages.
 * @route       extras/addAlertMsg
 * @method      POST
 * @deprecated
 */
router.post('/addAlertMsg', [
    check('p_msg').isString().not().isEmpty(), 
	check('p_title').isString().not().isEmpty(),
	check('p_start').isString().not().isEmpty(), 
	check('p_end').isString().not().isEmpty(), 
    check('p_active').isInt()], dbMiddleware, validateRequestFields, eControllers.addAlertMsg);

/**
 * Endpoint to edit alert messages.
 * @route       extras/editAlertMsg
 * @method      POST
 */
router.post('/editAlertMsg', [ 
    check('p_id').isInt().exists(),
    check('p_msg').isString().not().isEmpty(), 
	check('p_title').isString().not().isEmpty(),
	check('p_start').isString().not().isEmpty(), 
	check('p_end').isString().not().isEmpty(), 
], dbMiddleware, validateRequestFields, eControllers.editAlertMsg)


/**
 * Endpoint to get companies (active ones and non active ones).
 * @route       extras/getCompanies
 * @method      POST
 * @deprecated
 */
router.post('/getCompanies', dbMiddleware, eControllers.getCompanies);


/**
 * Endpoint to get colaborador by the company that is related to.
 * @route       extras/getFuncionarioByCompany
 * @method      POST
 */
router.post('/getFuncionarioByCompany', [
    check('p_cod_company').isInt().exists()
], dbMiddleware, validateRequestFields, eControllers.getFuncionarioByCompany)
 
/**
 * Endpoint         to get extras in the database of a certain type.
 * @route extra/getExt
 * @method      POST
 */
router.post('/getExt',[
    check('p_cod_etype').isString().not().isEmpty()
], dbMiddleware, validateRequestFields, eControllers.getExtras);

/**
 * Endpoint         to get all the extras in the database.
 * @route extra/getExt
 * @method      POST
 */
 router.post('/getAllExtras',[
], dbMiddleware, validateRequestFields, eControllers.getAllExtras);



/**
 * Endpoint         to edit the extra (departments, id_types, reasons to reject, etc).
 * @route extra/editExtra
 * @method      POST
 * @deprecated
 */
router.post('/editExtra', [
    check('p_id').isInt().exists(),
    check('p_id_etype').isString().not().isEmpty(),
    check('p_name').isString().not().isEmpty(),
    check('p_active').isInt().exists()
], dbMiddleware, validateRequestFields, eControllers.editExtra);


/**
 * Endpoint         to add the extra (departments, id_types, reasons to reject, etc).
 * @route extra/addExtra
 * @method      POST
 */
router.post('/addExtra',[
    check('p_id_etype').isString().exists(),
    check('p_name').isString().exists(),
    check('p_active').isBoolean().exists()
], dbMiddleware, validateRequestFields, eControllers.addExtras);

module.exports = router;
