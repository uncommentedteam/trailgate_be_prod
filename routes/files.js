const express = require('express');
const path = require('path');
const fs = require('fs');
const multer = require('multer');
const mime = require('mime');
const xlFile = require('read-excel-file/node')
const dbMiddleware = require('../middleware/connectionAdderMiddleware');
const { convertXlDataToUserObjects, writeToErrorLog, formalizeXcelData, validateProcessedUsersFromXcelFile } = require('../utils/utils');
const { DEFAULT_DOCUMENT_PHOTO_FILE, DEFAULT_INDUCTION_PHOTO_FILE } = require('../utils/constants');
const { sendResponseError } = require('../utils/controllerHelper');
const logger = require('../utils/logger');
const router = express.Router();


/**
 * Endpoint to get visitors template files. This templates will be available for dowload for private users.
 * @route     files/visitors/templates
 * @method    GET
 */
router.get('/visitors/templates', (req, res) => {
  const file = path.join(__dirname, '..', 'res', 'templates', 'MultipleVisitors.xlsx')
  res.download(file);
});

/**
 * Endpoint to get colaborators template files. This templates will be available for dowload for private users.
 * @route     files/access/templates
 * @method    GET
 */
router.get('/access/templates', (req, res) => {
  const file = path.join(__dirname, '..', 'res', 'templates', 'MultipleAccess.xlsx');
  logger.info('MultipleAccess.xls downloaded!: ');
  res.download(file);
});

/**
 * Endpoint to get support email instruction file for changing the support email.
 * @route     files//supprotEmail/instruction
 * @method    GET
 */
router.get('/supprotEmail/instruction', (req, res) => {
  const file = path.join(__dirname, '..', 'res', 'templates', 'activate_support_email_instruction.pdf')
  res.download(file);
});

/**
 * Endpoint to get user manual (hosts).
 * @route     files/hosts/manual
 * @method    GET
 */
 router.get('/hosts/manual', (req, res) => {
  const file = path.join(__dirname, '..', 'res', 'templates', 'HostsManual.pdf')
  res.download(file);
});
/**
 * Endpoint to get user manual (hosts).
 * @route     files/hosts/manual
 * @method    GET
 */
 router.get('/visitors/manual', (req, res) => {
  const file = path.join(__dirname, '..', 'res', 'templates', 'UserManual.pdf')
  res.download(file);
});

/**
 * 
 */
router.get('/videos/inductionVideo/:videoType', (req, res) => {
  const videoType = req.params.videoType;
  const filePath = path.join(__dirname, '..', 'res', 'videos', `${videoType}.mp4`);
  try {

    fs.stat(filePath, (err, stat) => { 
      //::>> If file has not been found
      if( err !== null && err.code === 'ENOENT') { 
        return sendResponseError(res, 'File Not Found!', 'files.js');
      }
  
      const fileSize = stat.size;
      const range = req.headers.range
  
      if(range) { 
  
        const parts = range.replace(/bytes=/, "").split("-");
  
        const start = parseInt(parts[0], 10);
        const end = parts[1] ? parseInt(parts[1], 10) : fileSize-1;
        
        const chunksize = (end-start)+1;
        const file = fs.createReadStream(filePath, {start, end});
        const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': 'video/mp4',
        }
        
        res.writeHead(206, head);
        file.pipe(res);
  
      } else { 
        const head = {
          'Content-Length': fileSize,
          'Content-Type': 'video/mp4',
        }
  
        res.writeHead(200, head);
        fs.createReadStream(filePath).pipe(res);
      }
    })
    
  } catch (error) {
    sendResponseError(res, error, 'files.js', '...')
  }
  
})

/**
 * Endpoint to get the user identfication photo.
 * @route     files/uId/:userID
 * @method    GET
 */
router.get('/uId/:userID', (req, res) => { 
  try{
    const userDocumentNumber = req.params.userID;
    const defaultFile = path.join(__dirname, '..', 'public', 'induction', DEFAULT_DOCUMENT_PHOTO_FILE);
    if(!userDocumentNumber) return res.sendFile(defaultFile);

    const userDocumentPhoto = path.join(__dirname, '..', 'res', 'userDocumentPhotos', `${userDocumentNumber}.jpg`);
    if(fs.existsSync(userDocumentPhoto))  return res.sendFile(userDocumentPhoto);
    
    res.sendFile(defaultFile);
  } catch (error) { 
    sendResponseError(res, error, 'files.js', '/uId/:userID');
  }
})

/**
 * Endpoint to get the get induction question image.
 * @route     /induction/:imgName
 * @method    GET
 */
router.get('/induction/:imgName', (req, res) => { 
  try{
    const imageName = req.params.imgName;
    const defaultFile = path.join(__dirname, '..', 'res', 'induction', DEFAULT_INDUCTION_PHOTO_FILE);
    if(!imageName) return res.sendFile(defaultFile);

    const imageFile = path.join(__dirname, '..', 'res', 'induction', imageName);
    if(fs.existsSync(imageFile)) return res.sendFile(imageFile);

    res.sendFile(defaultFile);
  } catch (error) { 
    sendResponseError(res, error, 'files.js', '/induction/:imgName');
	}
  }
)

/**
 * Endpoit to udpate the document photo of a user.
 * @route     files/renUid
 * @method    POST
 */
router.post('/renUid', (req, res) => { 
  try{
      const oldDocumentId = req.body.p_nid;
      const newDocumentId = req.body.p_new_nid;

      const userDocumentPhotoPath = path.join(__dirname, '..', 'res', 'userDocumentPhotos', `${oldDocumentId}.jpg`);
      if(fs.existsSync(userDocumentPhotoPath)) fs.renameSync(userDocumentPhotoPath , path.join(__dirname, '..', 'res', 'userDocumentPhotos', `${newDocumentId}.jpg`));

      res.json({msg: "ID Image renamed Successfully!"});
  } catch (error) { 
    sendResponseError(res, error, 'files.js', '/renUid');
	}
})






//::>> Configuration for the path to store filled template files. [MultipleVisitor, MultipleAccess]
const storageTemplateFile = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './res/multipleUserRequestFiles')
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.xlsx`)
  }
})
//::>> Configuration to filter files (only excel files will be allosed)
const templateFileFilter = (req, file, cb) => {
    if(file.mimetype === mime.getType('xlsx')){
        cb(null, true);
    }
    cb(null, false);
}
const upload = multer({storage: storageTemplateFile, fileFilter: templateFileFilter});

/**
 * Endpoint to upload filled template files. [MultipleVisitors, MultipleAccess]
 * @route     files/uploadFile
 * @method    POST
 */
router.post('/uploadFile', dbMiddleware , upload.single('file'), async (req, res) => {   
    try{ 
        if(!req.file) throw new Error("The server only accepts excel files");
        const rows = await xlFile(req.file.path);
        const formalizedUsers = formalizeXcelData(req, rows);
        await validateProcessedUsersFromXcelFile(formalizedUsers);
        res.json(formalizedUsers);
    } catch (err) { 
      sendResponseError(res, err, 'files.js', '/uploadFile');
    }
});


//::>> Configuration for the path to store induction images.
const storageInductionImageConfig = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.join(__dirname, '..', 'res', 'induction'))
  },
  filename: function (req, file, cb) {
    cb(null, `${file.originalname}.jpg`);
  }
})
//::>> Configuration to filter induction images (only jpg and png files will be allosed)
const inductionImageFilter = (req, file, cb) => {
  if(file.mimetype === mime.getType('png')){
      cb(null, true);
  }
  if(file.mimetype === mime.getType('jpg')){
      cb(null, true);
  }
  if(file.mimetype === mime.getType('jpeg')){
    cb(null, true);
  }
}
const uploadInductionImage = multer({storage: storageInductionImageConfig, fileFilter: inductionImageFilter});

/**
 * Endpoint to upload induction image Files.
 * @route     files/uploadFile
 * @method    POST
 */
router.post('/uploadIndQuest', dbMiddleware , uploadInductionImage.any('file'), async (req, res) => {   
  try{ 
      if(!req.files) throw new Error("The server only accepts png or jpg files");
      res.json({msg: 'Success'});
  } catch (err) { 
    sendResponseError(res, error, 'files.js', '/uploadFile')
  }
})


//::> Configuration for the path to store user document images.
const storageUserDocumentPhoto = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './res/userDocumentPhotos')
    },
    filename: function (req, file, cb) {
        cb(null, `${file.originalname}.jpg`);
    }
})
//::>> Configuration to filter induction images (only jpg, jpeg and png files will be allosed)
const userDocumentPhotoFilter = (req, file, cb) => {
    if(file.mimetype === mime.getType('png')){
        cb(null, true);
    }
    if(file.mimetype === mime.getType('jpg')){
        cb(null, true);
    }
    if(file.mimetype === mime.getType('jpeg')){
      cb(null, true);
  }
}
const uploadUid = multer({storage: storageUserDocumentPhoto, fileFilter: userDocumentPhotoFilter});

/**
 * Endpoit to update user document photos.
 * @route     files/uploadId
 * @method    POST
 */
router.post('/uploadId', dbMiddleware , uploadUid.any('file'), async (req, res) => {
    try{
        if(!req.files) throw new Error("The server only accepts png or jpg files");
        res.json({msg: 'Success'});
    } catch (err) {
        sendResponseError(res, error, 'files.js', '/uploadFile');
    }
});

module.exports = router;