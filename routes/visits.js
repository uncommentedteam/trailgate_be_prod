const express = require('express');
const { check } = require('express-validator');
const vControllers = require('../controllers/visits');
const dbMiddleware = require('../middleware/connectionAdderMiddleware');
const { validateRequestFields } = require('../middleware/globalMiddleware');
const router = express.Router();


// TESTE VI
/**
 * Endpoint to get status of the schedule visit.
 * @route       visits/getScheduleVisitStatus
 * @method      POST
 */
router.post('/getScheduleVisitStatusThread', [
    check('p_cod_thread').exists().isInt(),
    check('p_data_start').isString().not().isEmpty(),
    check('p_data_end').isString().not().isEmpty(),
    check('p_cod_dep'),
    check('p_cod_host')
], dbMiddleware, validateRequestFields, vControllers.getScheduleVisitStatusThread)


// TESTE III
/**
 * Endpoint         to change the material in schedule visit.
 * @route visits/       changeScheVisitMaterial
 * @method POST
 */
router.post('/changeScheVisitMaterial', [
    check('p_material'),
    check('p_cod').exists().isInt()
], dbMiddleware, validateRequestFields, vControllers.changeSVisitMaterial)



// TESTE IX
/**
 * Endpoint to get status of the schedule visit.
 * @route       visits/getDepByTer
 * @method      POST
 */
router.post('/getDepByTer', [
    check('p_cod_ter').exists().isInt()
], dbMiddleware, validateRequestFields, vControllers.getDepByTer)


// TESTE X
/**
 * Endpoint to get status of the schedule visit.
 * @route       visits/getUserByDepAndTer
 * @method      POST
 */
router.post('/getUserByDepAndTer', [
    check('p_cod_dep').exists().isInt(),
    check('p_cod_ter').exists().isInt()
], dbMiddleware, validateRequestFields, vControllers.getUserByDepAndTer);


/**
 * Endpoint to add visits by the host.
 * @route       visits/submitVisitHost
 * @method      POST
 * @deprecated
 */
router.post('/submitVisitHost', [
    check('p_id_host').exists().isInt(),
    check('p_id_department').exists().isInt(),
    check('p_id_terminal').exists().isInt(),
    check('p_date_time').exists().isString(),
    check('p_material').isString(),
    check('p_lplate').isString(),
    check('p_detail').isString().not().isEmpty().isLength({min: 6}),
    check('p_period').isInt().exists(),
    check('p_submited').exists().isString().not().isEmpty(),
    check('p_rule').isInt().exists(),
	check("p_bool_host").isInt(),
	check("p_dtm_host").isString(),
    check("p_why"),
    check("p_multi_entry").isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.submitVisitHost);



/**
 * Endpoint to add visits by the Thread.
 * @route       visits/submitVisitThread
 * @method      POST
 * @deprecated
 */
router.post('/submitVisitThread', [
    check('p_id_thread').exists().isInt(),
    check('p_id_department').exists().isInt(),
    check('p_id_terminal').exists().isInt(),
    check('p_date_time').exists().isString(),
    check('p_material').isString(),
    check('p_lplate').isString(),
    check('p_detail').isString().not().isEmpty().isLength({min: 6}),
    check('p_period').isInt().exists(),
    check('p_submited').exists().isString().not().isEmpty(),
    check('p_rule').isInt().exists(),
	check("p_bool_thread").exists().isInt(),
	check("p_dtm_thread").exists().isString(),
    check("p_why"),
    check("p_multi_entry").isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.submitVisitThread);

/**
 * Endpoint         to add visits by the Core Security.
 * @route visits/       submitVisitCoreSec
 * @method POST
 * @deprecated
 */
router.post('/submitVisitCoreSec', [
    check('p_id_coresecurity').exists().isInt(),
    check('p_id_department').exists().isInt(),
    check('p_id_terminal').exists().isInt(),
    check('p_date_time').exists().isString(),
    check('p_material').isString(),
    check('p_lplate').isString(),
    check('p_detail').isString().not().isEmpty().isLength({min: 6}),
    check('p_period').isInt().exists(),
    check('p_submited').exists().isString().not().isEmpty(),
    check('p_rule').isInt().exists(),
	check("p_bool_coresecurity").exists().isInt(),
	check("p_dtm_coresecurity").exists().isString(),
    check("p_why"),
    check("p_multi_entry").isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.submitVisitCoreSecurity);


// TESTE IX
/**
 * Endpoint         to count the amout of visit that started.
 * @route visits/       getCountVisit
 * @method POST
 */
router.post('/getCountVisit', [
    check('p_id_svisit').exists().isInt()
], dbMiddleware, validateRequestFields, vControllers.getCountVisitIn)



//Mr1 Get svisit State
/**
 * #TODO:        Comment this.
 * @route visits/       getVisitsState
 * @method POST
 */
router.post('/getVisitsState', dbMiddleware, vControllers.getVisitsState)

//Same but less : really?
/**
 * #TODO:        Comment this
 * @route visits/       getVisitsStateUserMini
 * @method POST
 */
router.post('/getVisitsStateUserMini', dbMiddleware, validateRequestFields, vControllers.getVisitsStateUserMini)

/**
 * #TODO:        Comment this
 * @route visits/       getVisitsStateUserMiniFiltrar
 * @method POST
 */
router.post('/getVisitsStateUserMiniFiltrar', dbMiddleware, validateRequestFields, vControllers.getVisitsStateUserMiniFiltrar)

//Mr1 Get Data for Mobile  || IF YOU DO NOT NEED THIS, PLEASE REMOMVE
//@p_dtm_start datetimeoffset = '2020-01-01 10:30:00 +02:00',
//@p_dtm_end datetimeoffset = '2020-12-31 10:30:00 +02:00',
//@p_cod_comp int = NULL
//router.post('/getMobileDashboard', dbMiddleware, vControllers.getMobileDashboard)

//Mr1 Get Data for Mobile Dashboard
//@p_dtm_start datetimeoffset = '2020-01-01 10:30:00 +02:00',
//@p_dtm_end datetimeoffset = '2020-12-31 10:30:00 +02:00',
//@p_cod_comp int = NULL

/**
 * #TODO: Comment this.
 * @route       visits/getMobileDashboard
 * @method      POST
 */
router.post('/getMobileDashboard', [
    check('p_dtm_start'),
    check('p_dtm_end'),
    check('p_id_host'),
    check('p_id_comp'),
],dbMiddleware, validateRequestFields, vControllers.getMobileDashboard)

// Mr1 Get Data Pedido
// @p_int int
// visit information
/**
 * #TODO: Comment this.
 * @route       visits/getVisitInfo
 * @method      POST
 */
router.post('/getVisitInfo', [check('p_cod').isInt().exists()], dbMiddleware, validateRequestFields, vControllers.getVisitInfo)

// visit visitors
/**
 * #TODO: Comment this.
 * @route       visits/getVisitVisitor
 * @method      POST
 */
router.post('/getVisitVisitor', [check('p_cod').isInt().exists()], dbMiddleware, validateRequestFields, vControllers.getVisitVisitor)

// visit extras
/**
 * #TODO: Comment this.
 * @route       visits/getVisitExtra
 * @method      POST
 */
router.post('/getVisitExtra', [check('p_cod').isInt().exists()], dbMiddleware, validateRequestFields, vControllers.getVisitExtra)



// GET EXTRA
/** MOVE TO EXTRA ROUTE FILE AND THE CONTROLLER OF THIS METHOD TO THE EXTRA CONTROLLER FILE
 * Endpoint to get extras by the extra code type.
 * @route       visits/getExtras
 * @method      POST
 * @deprecated
 */
router.post('/getExtras', [check('p_cod_etype').isString().exists()], dbMiddleware, vControllers.getExtras)


// Mr1 Respond request
/**
 * #TODO: Edit the state of the visit, to aprove or not
 * @route       visits/editVstate_state_htc
 * @method      POST
 */
router.post('/editVstate_state_htc', dbMiddleware, validateRequestFields, vControllers.editVstate_state_htc)


// @p_int int,
// @p_id_svisit int,
// @p_htc_cancel bit,
// @p_cancel_why text,
// @p_when_cancel datetimeoffset
/**
 * #TODO: Edit the state of the visit to cancel 
 * @route       visits/editVstate_state_htc
 * @method      POST
 */
router.post('/editVstate_state_htc_cancel', [
    check('p_int').isInt().exists(),
    check('p_id_svisit').isInt().exists(),
    check('p_htc_cancel'),
    check('p_cancel_why'),
    check('p_cod_why'),
    check('p_when_cancel').isString().exists() ]
, dbMiddleware, validateRequestFields, vControllers.editVstate_state_htc_cancel)



// GET GATEKEEPER ENTRADAS
/**
 * #TODO: Get list of visits that are about that want to get in at the Gate
 * @route       visits/getVisitEntrace
 * @method      POST
 */
router.post('/getVisitEntrace', 
    [check('p_dtm_start').isString().exists(),
    check('p_dtm_end').isString().exists()], 
    dbMiddleware, validateRequestFields, vControllers.getVisitEntrace)


	
// GET GATEKEEPER LEAVING
/**
 * #TODO: Get list of visits that are about that want to get out at the Gate
 * @route       visits/getVisitOut
 * @method      POST
 */
router.post('/getVisitOut', 
[check('p_dtm_start').isString().exists(),
check('p_dtm_end').isString().exists()], 
dbMiddleware, validateRequestFields, vControllers.getVisitOut)



// GET TERMINALS
/**
 * #TODO:        Comment this. Dude, you can use the method in extras if you want to get terminals.
 * @route visits/       getTerminalPort
 * @method POST
 */
router.post('/getTerminalPort',[
    check('p_active'),
],dbMiddleware, validateRequestFields, vControllers.getTerminalPort)


// GET DEP BY TERMINAL
/**
 * #TODO: Comment this. [THis should be moved to Extras Routes]
 * @route       visits/getDepByTerm
 * @method      POST
 */
router.post('/getDepByTerm', [
    check('p_id_comp').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.getDepByTerm);


// GET USER BY DEP AND TERMINAL
/**
 * #TODO: Comment this. [THis should be moved to Admin Routes]
 * @route       visits/getUsersByDepAndTerm
 * @method      POST
 */
router.post('/getUsersByDepAndTerm', [
    check('p_id_comp').isInt().exists(),
    check('p_id_dep').isInt().exists()
], dbMiddleware, vControllers.getUsersByDepAndTerm);


// CHECK VISIT STARTED
/**
 * #TODO: Comment this.
 * @route       visits/checkVisitStarted
 * @method      POST
 */
router.post('/checkVisitStarted', [
    check('p_cod_svisit').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.checkVisitStarted);

// EDIT PERIODO VISIT
/**
 * #TODO: Comment this.
 * @route       visits/editPeriodoOnly => /editVisitSpec
 * @method      POST
 * @deprecated
 */
router.post('/editVisitSpec', [
    check('p_cod').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.editVisitSpec);


//::>> Cherno
/**
 * #TODO: Comment this.
 * @route       visits/addVisit
 * @method      POST
 * @deprecated
 */
router.post('/addVisit', [
    check('p_id_x').isInt().exists(),
	check('p_id_gk').isInt().exists(),
    check('p_id_svisit').isInt().exists(),
	check('p_dtm').isString().not().isEmpty(),
	check('p_id_obs').isInt().exists(),
	check('p_isin').isInt().exists(),
    check('p_id_gate').isInt().exists(),
    check('p_is_active').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.addVisit);

//::>> Cherno
/**
 * #TODO: Comment this.
 * @route       visits/editVisit
 * @method      POST
 */
router.post('/editVisit', [
    check('p_id').isInt().exists(),
    check('p_id_x').isInt().exists(),
	check('p_id_gk'),
	check('p_id_svisit').isInt().exists(),
	check('p_dtm'),
	check('p_id_obs'),
	check('p_isin'),
    check('p_id_gate'),
    check('p_is_active'),
    check('p_is_entrance'),
    check('p_period'),
], dbMiddleware, validateRequestFields, vControllers.editVisit);

//::>>

/**
 * Endpoint that searches the visit in the dababase.
 * @route       visits/getScheVisit
 * @method      POST
 */
router.post('/getScheVisit', [ 
    check('p_dtm_start').exists().isString(),
    check('p_dtm_end').exists().isString(),
], dbMiddleware, validateRequestFields, vControllers.getScheduleVisit);


/**
 * Endpoint to save a simple or collective visit.
 * @route       visits/addScheVisit
 * @method      POST
 */
router.post('/addScheVisit', [
	check('p_id_department').isInt().exists(),
	check('p_id_company').isInt(),
	check('p_date_time').isString().not().isEmpty(),
	check('p_submited').isString().not().isEmpty(),
    check('p_rule').isInt(),
    check('p_internal').isInt().exists(),
	check('p_period').isInt(),
	check('p_multi_entry').isInt().exists(),
	check('p_access').isInt().exists(),
	check('p_tocompany').isInt().exists(),
	check('p_fast').isInt().exists(),
    check('p_id_gate').isInt().exists(),
    check('type_visit').isInt().exists(),
], dbMiddleware, validateRequestFields, vControllers.addScheVisit);

/**
 * Endpoint to add a fast request made by a non registered user. 
 * @route       visits/addVisitorFastRequest
 * @method      POST
 */
router.post('/addVisitorFastRequest', [
	check('p_id_department').isInt().exists(),
	check('p_id_company').isInt(),
	check('p_date_time').isString().not().isEmpty(),
	// check('p_detail').isString().not().isEmpty(),
	check('p_submited').isString().not().isEmpty(),
    check('p_rule').isInt(),
    check('p_internal').isInt().exists(),
	check('p_period').isInt(),
	check('p_multi_entry').isInt().exists(),
	check('p_access').isInt().exists(),
	check('p_tocompany').isInt().exists(),
	check('p_fast').isInt().exists(),
    check('p_id_gate').isInt().exists(),
    check('p_id_rpp').isInt().exists(),
], dbMiddleware, validateRequestFields, vControllers.addVisitorFastRequest);

/**
 * Endpoint to get the visit extras.
 * @route       visits/getSvisitExtra
 * @method      POST
 */
router.post('/getSvisitExtra', [
    check('p_int_id').isInt().exists(),
], dbMiddleware, validateRequestFields, vControllers.getSvisitExtra);

/**
 * Endpoint to edit only the visit data (not the materials or vehicles).
 * @route       visits/editScheVisit
 * @method      POST
 */
router.post('/editScheVisit', [
    check('p_id').isInt().exists(),
	check('p_id_department').isInt().exists(),
	check('p_id_company').isInt(),
	check('p_date_time').isString().not().isEmpty(),
	check('p_submited').isString().not().isEmpty(),
	check('p_rule').isInt(),
	check('p_period').isInt(),
	check('p_multi_entry').isInt().exists(),
	check('p_access').isInt().exists(),
	check('p_tocompany').isInt().exists(),
	check('p_fast').isInt().exists(),
    check('p_id_gate').isInt().exists(),
], dbMiddleware, validateRequestFields, vControllers.editScheVisit);

/**
 * Endpoint         to edit only the visit  the materials and/or vehicles.
 * @route visits/       editSvisitExtra
 * @method POST
 */
router.post('/editSvisitExtra', [
    check('p_id'),
	check('p_svisit'),
	check('p_oname'),
	check('p_qnt'),
	check('p_ismtrl'),
], dbMiddleware, validateRequestFields, vControllers.editSVisitExtra );

/**
 * Endpoint         to add vist extras (material and vehicles);
 * @route visits/       addSvisitExtra
 * @method POST
 */
router.post('/addSvisitExtra', [
	check('p_svisit'),
	check('p_oname'),
	check('p_qnt'),
    check('p_ismtrl'),
    check('p_is_active'),
    check('p_cod_user'),
    check('p_dtm')
], dbMiddleware, vControllers.addSvisitExtra);

/**
 * Endpoint to edit the visit extras.
 * @route       visits/editSvisitExtra_v2
 * @method      POST
 * @deprecated
 */
router.post('/editSvisitExtra_v2', [
    check('p_id'),
	check('p_svisit'),
	check('p_oname'),
	check('p_qnt'),
    check('p_ismtrl'),
    check('p_is_active'),
    check('p_cod_user'),
    check('p_dtm')
], dbMiddleware, validateRequestFields, vControllers.editSvisitExtra_v2 );

// GIVE VISIT HOST TO A VISIT
/**
 * ???
         * @route visits/       edit_visit_host
 * @method POST
 * @deprecated
 */
router.post('/edit_visit_host', 
    [check('p_id_host').isInt().exists(),
    check('p_id').isInt().exists()], 
    dbMiddleware, vControllers.edit_visit_host)

 /**
 * Endpoint         to reset the visit aproval to  its initial state of aproval.
 * @route visits/       resetVisitState
 * @method POST
 */   
router.post('/resetVisitState',[
    check('p_id_svisit').isInt().exists(),
    check('p_id_host').isInt().exists(),
], dbMiddleware, validateRequestFields, vControllers.resetVisitState);

/**
 * Endpoint         to cancel the visit.
 * @route visits/       editVstateCanc
 * @method POST
 */
router.post('/editVstateCanc', [
    check('p_int').isInt().exists(),
	check('p_id_svisit').isInt().exists(),
	check('p_cancel_why').isString().exists(),
	check('p_when_cancel').isString().exists(),
], dbMiddleware, validateRequestFields, vControllers.editVstateCanc);

/**
 * Endpoint to change the visit state of aproval.
 * @route       visits/editVstate
 * @method      POST
 */
router.post('/editVstate', [
    check('p_id_svisit').isInt().exists(),
    check('p_int').isInt().exists(),
	check('p_id_svisit').isInt().exists(),
	check('p_dtm_htc').isString(),
] , dbMiddleware, validateRequestFields, vControllers.editVisitState);

/**
 * Endpoint to get all visitors of a visit.
 * @route       visits/getSelectedVisitors
 * @method      POST
 */
router.post('/getSelectedVisitors', [
    check('p_svisit').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.getSelectedVisitors);

/**
 * Endpoint         to add visitors to a visit.
 * @route visist/       addReqVis
 * @method POST
 */
router.post('/addReqVis', [
    check('p_id_svisit').isInt().exists(),
	check('p_users').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.addRequestVisitors); 

/**
 * Endpoint to edit the host of a visit.
 * @route       visits/editVisitCodHost
 * @method      POST
 */
router.post('/editVisitCodHost', [
    check('p_id').isInt().exists(),
    check('p_id_host').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.editVisitCodHost);

/**
 * #TODO:        Comment this.
 * @route visits/       editVisitCodHost
 * @method POST
 */
router.post('/setIsDone', [
    check('p_cod').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.setIsDone)


// ADDED FOR PUBLICK SERVICE
router.post('/getTerminalWithSpecs', [
    check('p_active').exists(),
	check('p_isport').exists(),
	check('p_isps').exists()
], dbMiddleware, vControllers.getTerminalWithSpecs)

/**
 * @route visit/notifyUser
 * @method POST
*/
router.post('/notifyUser', [
    check('p_id_svisit').isInt().exists(),
    check('p_id_from').isInt().exists()
], dbMiddleware, validateRequestFields, vControllers.notifyUser)

module.exports = router;
