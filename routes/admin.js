const express = require('express');
const { check, } = require('express-validator');
const dbMiddleware = require('../middleware/connectionAdderMiddleware');
const aControllers = require('../controllers/admin');
const { validateRequestFields } = require('../middleware/globalMiddleware');
const router = express.Router();

/**
 * Endpoint to get All the users in the database by filtering what kind of user it is required.
 * @route       admin/getAllVisitors
 * @method      POST
 */
router.post('/getAllUsers', [
    check('p_isvisitor').isInt().exists()
], dbMiddleware, validateRequestFields, aControllers.getAllUsrs)


/**
 * Endpoint to block visitors.
 * @route       admin/addBlocked
 * @method      POST
 */
router.post('/addBlocked', [
    check('p_id_user').isInt().exists(),
    check('p_id_who').isInt().exists(),
    check('p_isblocked').isInt().exists(),
    check('p_why').isString().not().isEmpty(),
    check('p_when').isString().not().isEmpty()
], dbMiddleware, validateRequestFields, aControllers.addBlocked)


/**
 * Endpoint to edit blocked visitors.
 * @route       admin/editBlocked
 * @method      POST
 */
router.post('/editBlocked', [
    check('p_id_user').isInt().exists(),
    check('p_id_who').isInt().exists(),
    check('p_isblocked').isInt().not().isEmpty(),
    check('p_when').isString().not().isEmpty()
], dbMiddleware, validateRequestFields, aControllers.editBlocked);

/**
 * Endpoint to get Current Date Stats of a terminal.
 * @route       admin/getStatsTerm
 * @method      POST
 */
router.post('/getStatsTerm', [
    check('p_id_thread').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.getThreadStats)


/**
 * Endpoint to get Current Date Stats.
 * @route       admin/getStats
 * @method      POST
 */
router.post('/getStats', dbMiddleware, validateRequestFields, aControllers.getStats);


/**
 * Endpoint to write the client app configurations.
 * @route       admin/setConfig
 * @method      POST
 */
router.post('/setConfig', validateRequestFields, aControllers.writeClientConfigurations);


/**
 * Endpoint to get the client configurations.
 * @route       admin/getConfig
 * @method      POST
 */
router.post('/getConfig', validateRequestFields, aControllers.getClientConfigurations);


/**
 * Endpoint to get request an induction to a user of a visit.
 * @route       admin/reqInduction
 * @method      POST
 */
router.post('/reqInduction', [
    check('p_id_x').isInt().exists(),
    check('p_id_svisit').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.reqInduction)


/**
 * Endpoint to get request an induction to all users of a visit.
 * @route       admin/reqInductionAll
 * @method      POST
 */
router.post('/reqInductionAll', [
    check('p_id_svisit').isInt().exists() //::>> this is the code of the visit, cherno inserted p_cod_host in the db, dunno why, copy paste maybe. don't blame mee...!!!!
], dbMiddleware, validateRequestFields, aControllers.reqInductionAll)


/**
 * Endpoint to get the induction Questions.
 * @route       admin/getInductionQuest
 * @method      POST
 */
router.post('/getInductionQuest', [
    // check('p_induction_group').isString()
], dbMiddleware, validateRequestFields, aControllers.getInductionQuest)


/**
 * Endpoint submit induction results.
 * @route       admin/subInduction
 * @method      POST
 */
router.post('/subInduction', [
    // check('p_id_svisit').isInt().exists(),
    // check('p_id_x').isInt().exists(),
    // check('p_isinduction')
], dbMiddleware, validateRequestFields, aControllers.subInduction)


router.post('/forcIndRes', [
    check('p_id_visitor').isInt().exists(),
    check('p_tokken').isString().exists(),
    check('p_deep_induction').isInt().exists(),
    check('p_dtm_deep_induction').isString().exists(),
    check('p_id_tip_induc').isInt().exists(),
    check('p_svisit').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.forceInductionResult);

/**
 * Endpoint to get submit issues to the support team.
 * @route       admin/contactUs
 * @method      POST
 */
router.post('/contactUs', [
    check('p_email').isEmail().exists(),
    check('p_detail').isString().exists(),
], validateRequestFields,  aControllers.contactUs)

/**
 * Endpoint to get the last induction question id.
 * @route       admin/getIndQId
 * @method      POST
 */
router.post('/getIndQId', dbMiddleware, validateRequestFields,  aControllers.getInductioQuestionId);

/**
 * Endpoint to get the induction question for the induction process.
 * @route       admin/getInduction
 * @method      POST
 */
router.post('/getInduction', dbMiddleware, validateRequestFields, aControllers.getInduction);

/**
 * Endpoint to add Induction Question.
 * @route       admin/addIndQuest
 * @method      POST
 */
router.post('/addIndQuest', [
    check('p_question').isString().exists(),
    check('p_answer').isString().exists(),
    check('p_created').isString().exists(),
    check('p_options').isString().exists(),
    check('p_is_valid').isInt().exists(),
    check('p_cod_extra').isInt().exists(),
    check('p_points').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.addInductionQuestion);


/**
 * Endpoint to edit induction questions.
 * @route       admin/editIndQuest
 * @method      POST
 */
router.post('/editIndQuest', [
    check('p_cod').isInt().exists(),
    check('p_question').isString().exists(),
    check('p_answer').isString().exists(),
    check('p_created').isString().exists(),
    check('p_options').isString().exists(),
    check('p_is_valid').isInt().exists(),
    check('p_cod_extra').isInt().exists(),
    check('p_points').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.editInductionQuestion);


/**
 * Endpoint to add an external company.
 * @route       admin/addEntity
 * @method      POST
 */
router.post('/addEntity', [
    check('p_fname').isString().not().isEmpty(),
    check('p_lname').isString().not().isEmpty(),
    check('p_nid').isString().not().isEmpty(),
    check('p_pnumber_1').isString().not().isEmpty(),
    check('p_pnumber_2').isString().not().isEmpty(),
    check('p_active').isInt().exists(),
    check('p_nationality').isString().not().isEmpty(),
    check('p_id_type_id').isInt().exists(),
    check('p_expiration_date_id').isString().not().isEmpty(),
    check('p_isvisitor').isInt().exists(),
    check('p_registry').isString().not().isEmpty(),
    check('p_cname').isString().not().isEmpty(),
    check('p_cemail').isString().not().isEmpty(), 
    check('p_cpnumber_1').isString().not().isEmpty(),
    check('p_cpnumber_2').isString().not().isEmpty(),
    check('p_cpsw').isString().not().isEmpty(),
    check('p_cport').isInt().exists(),
    check('p_cactive').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.addExternalCompanyAndUser);



/**
 * Endpoint to visits by entity.
 * @route       admin/getVisByEntity
 * @method      POST
 */
router.post('/getVisByEntity', [
    check('p_cod_entity').isInt().exists()
], dbMiddleware, validateRequestFields, aControllers.getUsrByEntity);

/**
 * Endpoint to get all the saved companies.
 * @route       admin/getCompanies
 * @method      POST
 */
router.post('/getCompanies', dbMiddleware, validateRequestFields, aControllers.getCompanies);

/**
 * Endpoint to get all the saved companies.
 * @route       admin/getAllCompanies
 * @method      POST
 * @deprecated
 */
router.post('/getAllCompanies', dbMiddleware, validateRequestFields, aControllers.getAllCompanies);

/**
 * Endpoint to get subcompany of a company.
 */
router.post('/getSubCompanies', dbMiddleware, validateRequestFields, aControllers.getSubCompanies);

/**
 * Endpoint to add an internal compnay or terminal.
 * @route       admin/addCompany
 * @method      POST
 */
router.post('/addCompany', [
    check('p_cname').isString().not().isEmpty(),
    check('p_cemail').isString().not().isEmpty(),
    check('p_cpnumber_1').isString().not().isEmpty(),
    check('p_cpnumber_2').isString().not().isEmpty(),
    check('p_cpsw').isString().not().isEmpty(),
    check('p_cport').isInt().exists(),
    check('p_cactive').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.addCompany);

/**
 * Endpoint to add a subcompany to a company(terminal).
 * @route       admin/addSubComp
 * @method      POST
 */
router.post('/addSubComp', [
    check('p_id_company').isInt().exists(),
    check('p_scname').isString().not().isEmpty(),
    check('p_is_active').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.addSubComp);


/**
 * Endpoint to edit an internal company or terminal.
 * @route       admin/editCompany
 * @method      POST
 */
router.post('/editCompany', [
    check('p_id').isInt().exists(),
    check('p_cname').isString().not().isEmpty(),
    check('p_cpnumber_1').isString().not().isEmpty(),
    check('p_cpnumber_2').isString().not().isEmpty(),
    check('p_cpsw').isString().not().isEmpty(),
    check('p_cport').isInt().exists(),
    check('p_cactive').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.editCompany);

/**
 * Endpoit to edit a subcompnay of a company(a terminal).
 * @route       admin/editSubCompany
 * @method      POST
 */
router.post('/editSubCompany', [
    check('p_id').isInt().exists(),
    check('p_id_company').isInt().exists(),
    check('p_scname').isString().not().isEmpty(),
    check('p_is_active').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.editSubCompany);


/**
 * Endpoint to add an Alert Message.
 * @route       admin/addAlerMessage
 * @method      POST
 */
router.post('/addAlerMessage',[
    check('p_msg').isString().not().isEmpty(),
    check('p_title').isString().not().isEmpty(),
    check('p_start').isString().not().isEmpty(),
    check('p_end').isString().not().isEmpty(),
    check('p_active').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.addAlertMsg  )

/**
 * Endpoint to edit an Alert Message.
 * @route       admin/editAlertMessage
 * @method      POST
 */
router.post('/editAlertMessage',[
    check('p_id').isInt().exists(),
    check('p_msg').isString().not().isEmpty(),
    check('p_title').isString().not().isEmpty(),
    check('p_start').isString().not().isEmpty(),
    check('p_end').isString().not().isEmpty(),
    check('p_active').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.editAlertMsg )

/**
 * Endpoint to get Alert Messages that are active.
 * @route       admin/getAlertMsg
 * @method      POST
 */
router.post('/getAlertMsg',[
    check('p_cur_date').isString().not().isEmpty(), 
], dbMiddleware, validateRequestFields, aControllers.getAlertMsg);


/**
 * Endpoint to send a link for an external company to register.
 * @route       admin/compRegLink
 * @method      POST
 */
router.post('/compRegLink', [
    check('p_email').isString().exists(),
    check('p_cod_core').isInt().exists(),
], dbMiddleware, validateRequestFields, aControllers.sendCompRegLink)


/**
 * Endpoint for password recovery
 * @route       admin/recPass
 * @method      POST
 */
router.post('/recPass', [
    check('p_contact').exists(),
], dbMiddleware, validateRequestFields, aControllers.recoverPassword)

/**
 * Endpoint for reports with SSRS view
 * @route       admin/get_report_ssrs_view
 * @method      get
 */
 router.get('/get_report_ssrs_view', dbMiddleware, aControllers.report_ssrs_view)


 router.post('/sendAdmNot', [
     check('notifications').isArray(),
 ], dbMiddleware, validateRequestFields, aControllers.sendAdminNotifications)

module.exports = router;
