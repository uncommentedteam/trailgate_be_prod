const express = require('express');
const http = require('http');
const path = require('path');
const cors = require('cors');
const { port } = require('./config/environment');

const authRouter = require('./routes/auth');
const filesRouter = require('./routes/files');
const extrasRouter = require('./routes/extras');
const visitRouter = require('./routes/visits');
const adminRouter = require('./routes/admin');
const {performVisitCheck, performPathCheck, getUserCodes} = require('./utils/tasks');
const logger = require('./utils/logger');
const { createConnectionPool } = require('./db/dbConnector');
const app = express();
const server = http.createServer(app);

const notification = require('./utils/Notification')


//::>> Public folder setup
const PUBLIC_DIRECTORY = path.join(__dirname, 'public');

app.use(express.json())
app.use(cors())

app.use(express.static(PUBLIC_DIRECTORY));

// Make folder accessivle
app.use("/api/userIds", express.static(path.join(__dirname,'res','userDocumentPhotos')))

//::>> Routing setup.
app.use('/api/auth', authRouter);
app.use('/api/files', filesRouter);
app.use('/api/extras', extrasRouter);
app.use('/api/visits', visitRouter);
app.use('/api/admin', adminRouter);


createConnectionPool();
server.listen(port);
logger.info(`SERVER RUNNIG AT PORT ${port}`, {fileName: 'Server.js'});
logger.debug(`ENVIRONMENT: ${process.env.NODE_ENV}`);
getUserCodes();
performVisitCheck();
performPathCheck();