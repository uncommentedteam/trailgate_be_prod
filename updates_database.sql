--1 -> 15 ABRIL 2021 
CREATE TABLE settings (
	name varchar(250) NOT NULL,
	description text NOT NULL,
	value text NOT NULL,
	registed date NOT NULL,
	CONSTRAINT settings_PK PRIMARY KEY (name)
)

--2 -> 19 ABRIL 
CREATE TABLE tbl_log_vstate (
	int_id int IDENTITY(0,1) NOT NULL,
	int_id_svisit int NOT NULL,
	int_id_htc int NOT NULL,
	bool_htc bit NULL,
	dtm_htc datetimeoffset NULL,
	str_why text NULL,
	bool_htc_cancel bit NULL,
	str_cancel_why text NULL,
	dtm_when_cancel datetimeoffset NULL,
	int_id_why int NULL,
	CONSTRAINT tbl_log_vstate_PK PRIMARY KEY (int_id,int_id_svisit),
	CONSTRAINT tbl_log_vstate_FK FOREIGN KEY (int_id_htc) REFERENCES tbl_fdetail(int_id_user),
	CONSTRAINT tbl_log_vstate_FK_1 FOREIGN KEY (int_id_svisit) REFERENCES tbl_svisit(int_id),
	CONSTRAINT tbl_log_vstate_FK_2 FOREIGN KEY (int_id_why) REFERENCES tbl_extra(int_id)
) 
------------
CREATE TABLE tbl_log_blocked (
	int_id int IDENTITY(0,1) NOT NULL,
	int_id_user int NOT NULL,
	int_id_who int NOT NULL,
	bool_isblocked bit NOT NULL,
	str_why text NOT NULL,
	dtm_when datetimeoffset NOT NULL,
	CONSTRAINT tbl_log_blocked_PK PRIMARY KEY (int_id,int_id_user),
	CONSTRAINT tbl_log_blocked_FK FOREIGN KEY (int_id_who) REFERENCES tbl_fdetail(int_id_user),
	CONSTRAINT tbl_log_blocked_FK_1 FOREIGN KEY (int_id_user) REFERENCES tbl_user(int_id)
)

--------------
ALTER PROCEDURE proc_add_blocked(
	@p_id_user int,
	@p_id_who int,
	@p_isblocked bit,
	@p_why text,
	@p_when datetimeoffset
)
AS
BEGIN
	
	BEGIN TRANSACTION [trans_block_user]
	
		BEGIN TRY
			INSERT INTO tbl_blocked
				(int_id_user,
				int_id_who,
				bool_isblocked,
				str_why,
				dtm_when)
			VALUES
				(@p_id_user,
				@p_id_who,
				@p_isblocked,
				@p_why,
				@p_when)
		--
			INSERT INTO tbl_log_blocked
				(int_id_user, 
				int_id_who, 
				bool_isblocked, 
				str_why, 
				dtm_when)
			VALUES
				(@p_id_user,
				@p_id_who,
				@p_isblocked,
				@p_why,
				@p_when)
		    COMMIT TRANSACTION [trans_block_user]
		END TRY
	
	
		BEGIN CATCH

			ROLLBACK TRANSACTION [trans_block_user]

		END CATCH  
END 


--3 -> 22 ABRIL
CREATE TABLE teste_db.dbo.tbl_log_notification (
	int_id int IDENTITY(0,1) NOT NULL,
	int_id_from int NOT NULL,
	int_id_to int NOT NULL,
	int_id_svisit int NOT NULL,
	dtm_when datetimeoffset DEFAULT CURRENT_TIMESTAMP NOT NULL,
	CONSTRAINT tbl_log_notification_PK PRIMARY KEY (int_id),
	CONSTRAINT tbl_log_notification_FK FOREIGN KEY (int_id_from) REFERENCES teste_db.dbo.tbl_user(int_id),
	CONSTRAINT tbl_log_notification_FK_1 FOREIGN KEY (int_id_to) REFERENCES teste_db.dbo.tbl_user(int_id),
	CONSTRAINT tbl_log_notification_FK_2 FOREIGN KEY (int_id_svisit) REFERENCES teste_db.dbo.tbl_svisit(int_id) ON DELETE RESTRICT ON UPDATE RESTRICT
) 
--------
CREATE PROCEDURE proc_log_notification(
	@p_id_from int,
	@p_id_to int,
	@p_id_svisit int,
	@p_dtm_when datetimeoffset = null
)
AS
BEGIN
	INSERT INTO tbl_log_notification
		(int_id_from, int_id_to, int_id_svisit, dtm_when)
	VALUES
		(@p_id_from, @p_id_to, @p_id_svisit, @p_dtm_when)
END

--4 -> 26 ABRIl
CREATE PROCEDURE proc_add_svisit_hosts 
	@p_id_host int,
	@p_id_department int,
	@p_id_company int,
	@p_date_time datetimeoffset,
	@p_detail text ,
	@p_submited datetimeoffset,
	@p_rule bit,
	@p_period int,
	@p_id_rpp int,
	@p_code varchar(6),
	@p_multi_entry bit,
	@p_access bit,
	@p_tocompany bit,
	@p_fast bit,
	@p_id_gate int,
	@p_internal bit
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @cod_svisit int,
				@type_user int;
		BEGIN
			EXEC proc_add_svisit @p_id_host, @p_id_department, @p_id_company, @p_date_time, @p_detail, @p_submited, @p_rule, @p_period, @p_id_rpp, @p_code, @p_multi_entry, @p_access, @p_tocompany, @p_fast, @p_id_gate, @p_internal;
			SET @cod_svisit = ( SELECT IDENT_CURRENT('tbl_svisit'));
			EXEC proc_add_ivstate @cod_svisit;
			SET @type_user = ( SELECT int_id_role FROM tbl_fdetail tf WHERE tf.int_id_user = @p_id_host )
			EXEC proc_edit_vstate_state_htc @p_int=0, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
			IF (@type_user = 52) 
				BEGIN 
					EXEC proc_edit_vstate_state_htc @p_int=1, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
				END
			ELSE IF (@type_user = 53)
				BEGIN
					EXEC proc_edit_vstate_state_htc @p_int=1, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
					EXEC proc_edit_vstate_state_htc @p_int=2, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
				END
			SELECT 1 AS status, @cod_svisit AS svisit, NULL AS error_num, NULL AS error_msg ;
			COMMIT TRANSACTION;
		END
	END TRY
	BEGIN CATCH  
 		-- RETURN ERROR
		SELECT 0 AS status, NULL AS svisit, ERROR_NUMBER() AS error_num, ERROR_MESSAGE() AS error_msg;
		--ROOLBACK
		ROLLBACK TRANSACTION;
	END CATCH  
END

----------

CREATE PROCEDURE proc_add_svisit_visitors
	@p_id_host int,
	@p_id_department int,
	@p_id_company int,
	@p_date_time datetimeoffset,
	@p_detail text,
	@p_submited datetimeoffset,
	@p_rule bit,
	@p_period int,
	@p_id_rpp int,
	@p_code varchar(6),
	@p_multi_entry bit,
	@p_access bit,
	@p_tocompany bit,
	@p_fast bit,
	@p_id_gate int,
	@p_internal bit
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @cod_svisit int,
				@type_user int;
		BEGIN
			EXEC proc_add_svisit @p_id_host, @p_id_department, @p_id_company, @p_date_time, @p_detail, @p_submited, @p_rule, @p_period, @p_id_rpp, @p_code, @p_multi_entry, @p_access, @p_tocompany, @p_fast, @p_id_gate, @p_internal;
			SET @cod_svisit = ( SELECT IDENT_CURRENT('tbl_svisit'));
			EXEC proc_add_ivstate @cod_svisit;
			SET @type_user = ( SELECT int_id_role FROM tbl_fdetail tf WHERE tf.int_id_user = @p_id_host )
			IF (@type_user = 52) 
				BEGIN 
					EXEC proc_edit_vstate_state_htc @p_int=0, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
				END
			ELSE IF (@type_user = 53)
				BEGIN
					EXEC proc_edit_vstate_state_htc @p_int=0, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
					EXEC proc_edit_vstate_state_htc @p_int=1, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
				END
			SELECT 1 AS status, @cod_svisit AS svisit, NULL AS error_num, NULL AS error_msg ;
			COMMIT TRANSACTION;
		END
	END TRY
	BEGIN CATCH  
 		-- RETURN ERROR
		SELECT 0 AS status, NULL AS svisit, ERROR_NUMBER() AS error_num, ERROR_MESSAGE() AS error_msg;
		--ROOLBACK
		ROLLBACK TRANSACTION;
	END CATCH  
END

----------------------------

ALTER PROCEDURE proc_add_ivstate ( @p_id_svisit INT )
AS 
BEGIN
	INSERT INTO tbl_vstate
		(int_id_svisit)
	VALUES
		(@p_id_svisit)
END
;

--4 -> 03 MAY

CREATE PROCEDURE proc_get_all_companies_subcompanies
	@p_active BIT = NULL,
	@p_isport BIT = NULL,
	@p_isps BIT = NULL
AS
BEGIN
	IF (@p_active IS NULL AND @p_isport IS NOT NULL AND @p_isps IS NULL)
		BEGIN
			SELECT *
			FROM tbl_company
			LEFT JOIN tbl_sub_company ON tbl_company.int_id = tbl_sub_company.int_id_company 
			WHERE bool_port = @p_isport
		END
	ELSE IF (@p_active IS NULL AND @p_isport IS NULL AND @p_isps IS NOT NULL)
		BEGIN
			SELECT *
			FROM tbl_company
			LEFT JOIN tbl_sub_company ON tbl_company.int_id = tbl_sub_company.int_id_company 
			WHERE bool_is_ps = @p_isps 
		END
	ELSE IF (@p_isport IS NULL AND @p_active IS NOT NULL AND @p_isps IS NULL)
		BEGIN 
			SELECT * 
			FROM tbl_company
			LEFT JOIN tbl_sub_company ON tbl_company.int_id = tbl_sub_company.int_id_company 
			WHERE bool_active = @p_active 
		END
	ELSE IF (@p_isport IS NOT NULL AND @p_active IS NOT NULL AND @p_isps IS NULL)
		BEGIN 
			SELECT *
			FROM tbl_company
			LEFT JOIN tbl_sub_company ON tbl_company.int_id = tbl_sub_company.int_id_company 
			WHERE bool_active = @p_active AND bool_port = @p_isport
		END
	ELSE IF (@p_isps IS NOT NULL AND @p_active IS NOT NULL)
		BEGIN 
			SELECT *
			FROM tbl_company
			LEFT JOIN tbl_sub_company ON tbl_company.int_id = tbl_sub_company.int_id_company 
			WHERE bool_active = @p_active AND bool_is_ps = @p_isps
		END
	ELSE
		BEGIN 
				SELECT *
			FROM tbl_company
			LEFT JOIN tbl_sub_company ON tbl_company.int_id = tbl_sub_company.int_id_company 
		END
END

Chernomirdin



--5 -> 09 MAY
ALTER PROCEDURE proc_get_extras
	@p_active BIT = NULL,
	@p_cod_etype VARCHAR(3) = NULL
AS
BEGIN
	IF (@p_cod_etype IS NULL AND @p_active IS NULL)
		BEGIN 
			SELECT cod, cod_etype, etype, extra, active 
			FROM view_extras 
		END
	ELSE IF (@p_active IS NULL)
		BEGIN
			SELECT cod, cod_etype, etype, extra, active 
			FROM view_extras
			WHERE 
			cod_etype = @p_cod_etype 
		END
	ELSE IF (@p_cod_etype IS NULL)
		BEGIN 
			SELECT cod, cod_etype, etype, extra, active 
			FROM view_extras
			WHERE 
			active = @p_active 
		END
	ELSE
		BEGIN 
			SELECT cod, cod_etype, etype, extra, active 
			FROM view_extras
			WHERE 
			active = @p_active and cod_etype = @p_cod_etype 
		END
	
END

--> 23 May
ALTER PROCEDURE proc_get_users
	@p_isvisitor bit 
AS
BEGIN
	IF (@p_isvisitor = 1)
		BEGIN
			SELECT
			cod, fname, lname, nationality, cod_type_id, 
			type_id, nid, exp_date_id, email, pnumber_1, 
			pnumber_2, psw, isvisitor, isactive, cod_entity, 
			entity, cod_provenance, provenance, istemp, 
			dtm_registry, tokken, is_lite_induction, 
			dtm_lite_induction, ia_deep_induction, dtm_deep_induction,is_blocked, int_id_who_blocked, blocked_why, blocked_when
			FROM view_user_visitor 
			WHERE isvisitor = 1
		END
	ELSE
		BEGIN 
			SELECT 
			cod, fname, lname, nationality, cod_type_id, 
			type_id, nid, exp_date_id, email, pnumber_1, 
			pnumber_2, psw, isvisitor, isactive, cod_company, 
			company, cod_department, department, cod_role, 
			user_role, dtm_registry, tokken
			FROM view_user_funcionario 
		END 
END

ALTER PROCEDURE proc_get_svisit_vstates (
	@p_dtm_start datetimeoffset,
	@p_dtm_end datetimeoffset,
	@p_id_host int = NULL,
	@p_id_comp int = NULL,
	@p_id_entity int = NULL,
	@p_id_x int = NULL,
	@p_id int = NULL)
AS
BEGIN
	IF (@p_id_host IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, sv_cod_rrp, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE st_host_cod = @p_id_host AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id_comp IS NOT NULL )
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail, sv_cod_host,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE sv_cod_comp = @p_id_comp AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end)) AND sv_is_ps = 0
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id_x IS NOT NULL ) 
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, sv_cod_rrp,
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		WHERE tvi.int_id_x = @p_id_x AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		WHERE vsv.cod = @p_id AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
	END
	ELSE IF (@p_id_entity IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE vuv.cod_entity = @p_id_entity AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
	END
	ELSE 
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, sv_is_internal,
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp, tv.bool_isin  as x_is_in,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		INNER JOIN tbl_visit tv
		ON vsv.cod = tv.int_id_svisit AND tv.int_id_x = vuv.cod
		WHERE ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
		--sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
END

ALTER PROCEDURE proc_edit_blocked(
	@p_id_user int,
	@p_id_who int = NULL,
	@p_isblocked bit = NULL,
	@p_why text = NULL,
	@p_when datetimeoffset = NULL
)
AS
BEGIN
	BEGIN TRANSACTION [trans_block_user]
	
		BEGIN TRY
			UPDATE tbl_blocked
			SET int_id_who= @p_id_who,
			bool_isblocked= @p_isblocked,
			str_why= @p_why,
			dtm_when= @p_when
			WHERE int_id_user= @p_id_user;
		
			INSERT INTO tbl_log_blocked
							(int_id_user, 
							int_id_who, 
							bool_isblocked, 
							str_why, 
							dtm_when)
						VALUES
							(@p_id_user,
							@p_id_who,
							@p_isblocked,
							@p_why,
							@p_when)
			COMMIT TRANSACTION [trans_block_user]
		 END TRY
	
		 BEGIN CATCH
		
		    ROLLBACK TRANSACTION [trans_block_user]
		
		 END CATCH  
END ;


--> 06 Jun
ALTER PROCEDURE proc_add_extra(
	@p_id_etype  varchar(3),
	@p_name text,
	@p_active bit
)
AS
BEGIN
	INSERT INTO tbl_extra
	(str_id_etype,
	str_name,
	bool_active)
	VALUES
	(@p_id_etype,
	@p_name,
	@p_active)
	
	SELECT int_id FROM tbl_extra WHERE str_id_etype LIKE @p_id_etype AND str_name LIKE @p_name AND bool_active = @p_active
END 

--> 11 Jun
CREATE PROCEDURE proc_get_user_by_one_of_property(
	@p_email varchar(100) = NULL,
	@p_pnumber_1 varchar(100) = NULL,
	@p_nid varchar(500) = NULL
)

AS
BEGIN
	IF(@p_email IS NOT NULL)
		BEGIN
			SELECT * FROM view_user_detail vud 
			WHERE email = @p_email
		END
	ELSE IF (@p_pnumber_1 IS NOT NULL) 
		BEGIN 
			SELECT * FROM view_user_detail vud 
			WHERE pnumber_1 = @p_pnumber_1
		END
	ELSE
		BEGIN 
			SELECT * FROM view_user_detail vud 
			WHERE nid = @p_nid
		END
END

--> 12 Jun
CREATE PROCEDURE proc_get_svisit_vstates (
	@p_dtm_start datetimeoffset,
	@p_dtm_end datetimeoffset,
	@p_id_host int = NULL,
	@p_id_comp int = NULL,
	@p_id_entity int = NULL,
	@p_id_x int = NULL,
	@p_id int = NULL)
AS
BEGIN
	IF (@p_id_host IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, sv_cod_rrp, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE st_host_cod = @p_id_host AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id_comp IS NOT NULL )
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail, sv_cod_host,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE sv_cod_comp = @p_id_comp AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end)) AND sv_is_ps = 0
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id_x IS NOT NULL ) 
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, sv_cod_rrp,
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		WHERE ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end)) AND vsv.cod IN (SELECT vsv.cod 
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		WHERE tvi.int_id_x = @p_id_x)
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		WHERE vsv.cod = @p_id AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
	END
	ELSE IF (@p_id_entity IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE vuv.cod_entity = @p_id_entity AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
	END
	ELSE 
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, sv_is_internal,
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp, tv.bool_isin  as x_is_in,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		INNER JOIN tbl_visit tv
		ON vsv.cod = tv.int_id_svisit AND tv.int_id_x = vuv.cod
		WHERE ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
		--sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
END

--> 12 Jun
CREATE PROCEDURE proc_get_user_by_id(
	@p_id int,
)
AS
BEGIN
	SELECT * FROM view_user_detail vud 
	WHERE vud.cod = @p_id
END

--> 03 Sept
ALTER PROCEDURE proc_edit_company(
	@p_id int, 
	@p_cname varchar(100) = NULL,
	@p_cemail varchar(250) = NULL,
	@p_cpnumber_1 varchar(100) = NULL,
	@p_cpnumber_2 varchar(100) = NULL,
	@p_cpsw varchar(250) = NULL,
	@p_cport bit = NULL,
	@p_cactive bit = NULL,
	@p_id_user int = NULL,
	@p_id_core int = NULL,
	@p_is_ps int = NULL,
	@p_dtm datetimeoffset
)
AS
BEGIN
	UPDATE tbl_company
	SET 
	str_cname=@p_cname,
	str_email=@p_cemail,
	str_pnumber=@p_cpnumber_1,
	str_pnumber_2=@p_cpnumber_2,
	str_psw=@p_cpsw,
	bool_port=@p_cport,
	bool_active=@p_cactive,
	int_id_user=@p_id_user,
	bool_is_ps=@p_is_ps
	WHERE int_id=@p_id
	
	INSERT INTO tbl_log_company
	(int_id_core, dtm_when, bool_status, int_id_company)
	VALUES(@p_id_core, @p_dtm, @p_cactive, @p_id)
END GO


--> 17 Oct
ALTER TABLE teste_db.dbo.tbl_user ADD str_uname varchar(100) NULL GO
CREATE TABLE teste_db.dbo.tbl_user (
	int_id int IDENTITY(1,1) NOT NULL,
	str_fname varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	str_lname varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	str_nid varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	str_psw varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	str_email varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	str_pnumber_1 varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
	str_pnumber_2 varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	bool_active bit NULL,
	bool_isvisitor bit NULL,
	str_nationality varchar(250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	int_id_type_id int NULL,
	dtm_expiration_date_id date NULL,
	str_uname varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT tbl_user_PK PRIMARY KEY (int_id),
	CONSTRAINT tbl_user_UN_NID UNIQUE (str_nid),
	CONSTRAINT tbl_user_UN_PNUMBER UNIQUE (str_pnumber_1),
	CONSTRAINT tbl_user_FK FOREIGN KEY (int_id_type_id) REFERENCES teste_db.dbo.tbl_extra(int_id) ON DELETE CASCADE ON UPDATE CASCADE
) GO
CREATE UNIQUE INDEX tbl_user_UN_NID ON teste_db.dbo.tbl_user (str_nid) GO
CREATE UNIQUE INDEX tbl_user_UN_PNUMBER ON teste_db.dbo.tbl_user (str_pnumber_1) GO


--

ALTER VIEW view_user AS
SELECT 
tu.int_id as cod,
tu.str_fname as fname,
tu.str_lname as lname,
tu.str_uname as uname,
tu.str_nationality as nationality,
tu.int_id_type_id as cod_type_id,
te.str_name as type_id,
tu.str_nid as nid,
tu.dtm_expiration_date_id as exp_date_id,
tu.str_email as email,
tu.str_pnumber_1 as pnumber_1,
tu.str_pnumber_2 as pnumber_2,
tu.str_psw as psw,
tu.bool_isvisitor as isvisitor,
tu.bool_active as isactive
FROM 
tbl_user tu
INNER JOIN tbl_extra te
ON te.int_id = tu.int_id_type_id GO


ALTER VIEW view_user_funcionario AS
SELECT 
cod,
fname,
lname,
uname,
nationality,
cod_type_id,
type_id,
nid,
exp_date_id,
email,
pnumber_1,
pnumber_2,
psw,
tf.int_id_company as cod_company,
tc.str_cname as company,
tf.int_id_department as cod_department,
dep.str_name as department,
tf.int_id_role as cod_role,
rol.str_name as user_role,
tf.dtm_registry,
tf.str_tokken as tokken,
isvisitor,
isactive
FROM 
tbl_fdetail tf 

INNER JOIN view_user vu 
ON tf.int_id_user = vu.cod 

LEFT OUTER JOIN tbl_company tc
ON tf.int_id_company = tc.int_id 

LEFT OUTER JOIN tbl_extra dep
ON tf.int_id_department = dep.int_id 

LEFT OUTER JOIN tbl_extra rol
ON tf.int_id_role = rol.int_id  GO

--

ALTER VIEW view_user_visitor AS
SELECT 
cod,
fname,
lname,
uname,
nationality,
cod_type_id,
type_id,
nid,
exp_date_id,
email,
pnumber_1,
pnumber_2,
psw,
isvisitor,
isactive,
tv.int_id_entity as cod_entity,
tc.str_cname as entity,
tv.int_id_provenance as cod_provenance,
prov.str_name as provenance,
tv.bool_istemp as istemp,
tv.dtm_registry,
tv.str_tokken as tokken,
tv.bool_lite_induction as is_lite_induction, 
tv.dtm_lite_induction,
tv.bool_deep_induction as ia_deep_induction,
tv.dtm_deep_induction,
tb.bool_isblocked as is_blocked,
tb.int_id_who as int_id_who_blocked,
tb.str_why as blocked_why,
tb.dtm_when as blocked_when
FROM 
tbl_vdetail tv 

INNER JOIN view_user vu 
ON tv.int_id_user = vu.cod 

LEFT OUTER JOIN tbl_extra prov
ON tv.int_id_provenance = prov.int_id 

LEFT OUTER JOIN tbl_company tc
ON tv.int_id_entity = tc.int_id 

LEFT OUTER JOIN tbl_blocked tb 
ON tv.int_id_user = tb.int_id_user

UNION ALL 

SELECT 
cod,
fname,
lname,
uname,
nationality,
cod_type_id,
type_id,
nid,
exp_date_id,
email,
pnumber_1,
pnumber_2,
psw,
isvisitor,
isactive,
tf.int_id_company as cod_entity,
tc.str_cname as entity,
NULL,
NULL,
0 as istemp,
tf.dtm_registry,
tf.str_tokken as tokken,
NULL, 
NULL,
NULL,
NULL,
tb.bool_isblocked as is_blocked,
tb.int_id_who as int_id_who_blocked,
tb.str_why as blocked_why,
tb.dtm_when as blocked_when
FROM 
tbl_fdetail tf

INNER JOIN view_user vu 
ON tf.int_id_user = vu.cod 

LEFT OUTER JOIN tbl_company tc
ON tf.int_id_company = tc.int_id 

LEFT OUTER JOIN tbl_blocked tb 
ON tf.int_id_user = tb.int_id_user
 GO


--

ALTER PROCEDURE proc_get_selected_by_email_or_pnumber_and_psw_users(
	@p_isvisitor bit,
	@p_email_pnumber varchar(250),
	@p_psw varchar(250) = NULL
	)
AS
BEGIN
	IF (@p_isvisitor = 1)
		BEGIN
			SELECT
			cod, fname, lname, uname, nationality, cod_type_id, 
			type_id, nid, exp_date_id, email, pnumber_1, 
			pnumber_2, psw, isvisitor, isactive, cod_entity, 
			entity, cod_provenance, provenance, istemp, 
			dtm_registry, tokken, is_lite_induction, is_blocked,
			dtm_lite_induction, ia_deep_induction, dtm_deep_induction
			FROM view_user_visitor 
			WHERE 
			(email = @p_email_pnumber OR pnumber_1 = @p_email_pnumber OR uname = @p_email_pnumber) AND psw = @p_psw
		END
	ELSE
		BEGIN 
			SELECT 
			cod, fname, lname, uname, nationality, cod_type_id, 
			type_id, nid, exp_date_id, email, pnumber_1, 
			pnumber_2, psw, isvisitor, isactive, cod_company, 
			company, cod_department, department, cod_role, 
			user_role, dtm_registry, tokken
			FROM view_user_funcionario 
			WHERE 
			(email = @p_email_pnumber OR pnumber_1 = @p_email_pnumber OR uname = @p_email_pnumber) AND psw = @p_psw
		END 
END GO


---

CREATE PROCEDURE proc_get_user_by_uname(
	@p_uname varchar(100)
)
AS
BEGIN
	SELECT * FROM tbl_user 
	WHERE str_uname = @p_uname
END

--

ALTER PROCEDURE proc_add_user
	@p_fname varchar(100),
	@p_lname varchar(100),
	@p_nid varchar(50)=null,
	@p_psw varchar(250)=null, 
	@p_email varchar(250)=null, 
	@p_pnumber_1 varchar(15)=null, 
	@p_pnumber_2 varchar(15)=null, 
	@p_active bit=null,
	@p_uname varchar(250)=null,
	@p_isvisitor bit=null, 
	@p_nationality varchar(250)=null, 
	@p_id_type_id int=null, 
	@p_expiration_date_id date=null
AS 
BEGIN
	INSERT INTO tbl_user
		(str_fname, 
		str_lname,
		str_uname,
		str_nid, 
		str_psw, 
		str_email, 
		str_pnumber_1, 
		str_pnumber_2, 
		bool_active, 
		bool_isvisitor, 
		str_nationality, 
		int_id_type_id, 
		dtm_expiration_date_id)
	VALUES
		(
		@p_fname,
		@p_lname,
		@p_uname,
		@p_nid,
		@p_psw, 
		@p_email, 
		@p_pnumber_1, 
		@p_pnumber_2, 
		@p_active,
		@p_isvisitor, 
		@p_nationality, 
		@p_id_type_id, 
		@p_expiration_date_id)
		
	--GET INSERTED USER
	SELECT int_id FROM tbl_user 
	WHERE (str_email = @p_email OR str_pnumber_1 = @p_pnumber_1) AND str_psw  = @p_psw
END;
 GO

-- 18 OUT 
drop procedure proc_get_user_by_one_of_property
    go

CREATE PROCEDURE proc_get_user_by_one_of_property(
    @p_contact varchar(100) = NULL
)

    AS
BEGIN
SELECT * FROM
    view_user_detail vud
WHERE
        email like @p_contact OR
        uname like @p_contact OR
        pnumber_1 like @p_contact
END
go




---
alter VIEW view_user_detail AS
SELECT
    tu.int_id as cod,
    tu.str_fname as fname,
    tu.str_lname as lname,
    tu.str_uname as uname,
    tu.str_nationality as nationality,
    tu.int_id_type_id as cod_type_id,
    te.str_name as type_id,
    tu.str_nid as nid,
    tu.dtm_expiration_date_id as exp_date_id,
    tu.str_email as email,
    tu.str_pnumber_1 as pnumber_1,
    tu.str_pnumber_2 as pnumber_2,
    tu.str_psw as psw,
    tu.bool_isvisitor as isvisitor,
    tu.bool_active as isactive,
    tv.int_id_entity as cod_entity,
    tc.str_cname as entity,
    NULL as cod_department,
    NULL as department,
    NULL as cod_role,
    NULL as user_role,
    tv.int_id_provenance as cod_provenance,
    prov.str_name as provenance,
    tv.bool_istemp as istemp,
    tv.dtm_registry,
    tv.str_tokken as tokken,
    tv.bool_lite_induction as is_lite_induction,
    tv.dtm_lite_induction,
    tv.bool_deep_induction as ia_deep_induction,
    tv.dtm_deep_induction,
    tb.bool_isblocked as is_blocked,
    tb.int_id_who as int_id_who_blocked,
    tb.str_why as blocked_why,
    tb.dtm_when as blocked_when
FROM tbl_vdetail tv
         INNER JOIN tbl_user tu
                    ON tv.int_id_user = tu.int_id
         INNER JOIN tbl_extra te
                    ON te.int_id = tu.int_id_type_id
         LEFT OUTER JOIN tbl_extra prov
                         ON tv.int_id_provenance = prov.int_id
         LEFT OUTER JOIN tbl_company tc
                         ON tv.int_id_entity = tc.int_id
         LEFT OUTER JOIN tbl_blocked tb
                         ON tv.int_id_user = tb.int_id_user

UNION ALL

SELECT
    tu.int_id as cod,
    tu.str_fname as fname,
    tu.str_lname as lname,
    tu.str_uname as uname,
    tu.str_nationality as nationality,
    tu.int_id_type_id as cod_type_id,
    te.str_name as type_id,
    tu.str_nid as nid,
    tu.dtm_expiration_date_id as exp_date_id,
    tu.str_email as email,
    tu.str_pnumber_1 as pnumber_1,
    tu.str_pnumber_2 as pnumber_2,
    tu.str_psw as psw,
    tu.bool_isvisitor as isvisitor,
    tu.bool_active as isactive,
    tf.int_id_company as cod_entity,
    tc.str_cname as entity,
    tf.int_id_department as cod_department,
    dep.str_name as department,
    tf.int_id_role as cod_role,
    rol.str_name as user_role,
    NULL,
    NULL,
    0 as istemp,
    tf.dtm_registry,
    tf.str_tokken as tokken,
    NULL,
    NULL,
    NULL,
    NULL,
    tb.bool_isblocked as is_blocked,
    tb.int_id_who as int_id_who_blocked,
    tb.str_why as blocked_why,
    tb.dtm_when as blocked_when
FROM tbl_fdetail tf
         INNER JOIN tbl_user tu
                    ON tf.int_id_user = tu.int_id
         INNER JOIN tbl_extra te
                    ON te.int_id = tu.int_id_type_id
         LEFT OUTER JOIN tbl_company tc
                         ON tf.int_id_company = tc.int_id
         LEFT OUTER JOIN tbl_blocked tb
                         ON tf.int_id_user = tb.int_id_user
         LEFT OUTER JOIN tbl_extra dep
                         ON tf.int_id_department = dep.int_id
         LEFT OUTER JOIN tbl_extra rol
                         ON tf.int_id_role = rol.int_id
    go

---

alter PROCEDURE proc_start_ps_user_1
    @p_fname varchar(100),
    @p_lname varchar(100),
    @p_pnumber_1 varchar(15),
    @p_nid varchar(50),
    @p_id_type_id int,
    @p_path text = NULL
    AS
BEGIN
    DECLARE @cod_user int;
    --Insert the user to tbl_user
INSERT INTO tbl_user
(str_fname, str_lname, str_psw ,str_pnumber_1, str_nid, bool_active, bool_isvisitor, int_id_type_id)
VALUES
(@p_fname, @p_lname, 'ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f', @p_pnumber_1, @p_nid, 1, 1, @p_id_type_id);

-- Insert visitor detail to tbl_vdetail
INSERT INTO tbl_vdetail
(int_id_user, dtm_registry, bool_istemp)
SELECT
    int_id, CURRENT_TIMESTAMP, 1 FROM tbl_user
WHERE (str_fname = @p_fname AND str_lname = @p_lname AND str_pnumber_1 = @p_pnumber_1);

IF(@p_path IS NOT NULL)
BEGIN
            -- Insert the path to tbl_path
INSERT INTO tbl_path
(int_id, str_table, str_path)
SELECT
    int_id, 'tbl_user', @p_path FROM tbl_user
WHERE (str_fname = @p_fname AND str_lname = @p_lname AND str_pnumber_1 = @p_pnumber_1);
END

    -- Get the last inserted user
SELECT TOP(1) @cod_user = int_id FROM tbl_user
WHERE (str_fname = @p_fname AND str_lname = @p_lname AND str_pnumber_1 = @p_pnumber_1);

RETURN @cod_user;
END;
go


-- 14 DEZ 2021
ALTER PROCEDURE proc_edit_visit(
    @p_id int,
    @p_id_x int,
    @p_id_gk int = NULL,
    @p_id_svisit int = NULL,
    @p_dtm datetimeoffset = NULL,
    @p_id_obs int = NULL,
    @p_isin bit = NULL,
    @p_id_gate int = NULL,
    @p_is_active bit = NULL
    )
    AS
BEGIN
    DECLARE @has_regist datetimeoffset;
        
    -- Check if it alredy went inside once
SELECT @has_regist = dtm FROM tbl_visit WHERE int_id_svisit = @p_id_svisit ;

SELECT @has_regist;

IF(@has_regist IS NOT NULL)
BEGIN
EXEC proc_set_visit_started @p_id_svisit;
END
    
    
    -- Update the table visit 
UPDATE tbl_visit
SET
    int_id_gk=@p_id_gk,
    int_id_svisit=@p_id_svisit,
    dtm=@p_dtm,
    int_id_obs=@p_id_obs,
    bool_isin=@p_isin,
    int_id_gate=@p_id_gate,
    bool_is_active=@p_is_active
WHERE int_id = @p_id and int_id_x = @p_id_x

    INSERT INTO tbl_log_visit
(int_id,int_id_gk, int_id_obs, dtm, bool_isin, int_id_gate, bool_is_active )
VALUES
    (@p_id, @p_id_gk, @p_id_obs, @p_dtm, @p_isin, @p_id_gate, @p_is_active)
END ;


--- 28 DEZ 2021
ALTER PROCEDURE proc_edit_visit(
    @p_id int,
    @p_id_x int,
    @p_id_gk int = NULL,
    @p_id_svisit int = NULL,
    @p_dtm datetimeoffset = NULL,
    @p_id_obs int = NULL,
    @p_isin bit = NULL,
    @p_id_gate int = NULL,
    @p_is_active bit = NULL
    )
    AS
BEGIN
    DECLARE @has_regist datetimeoffset;
        DECLARE @is_ps bit;
        
    -- Check if it alredy went inside once
SELECT @has_regist = dtm FROM tbl_visit WHERE int_id_svisit = @p_id_svisit ;

-- If the visitor alredy went inside, start the visit
IF(@has_regist IS NOT NULL)
BEGIN
EXEC proc_set_visit_started @p_id_svisit;
END
    
    --Check if its visitor going out
    IF(@p_isin = 0)
BEGIN
            --Check if its public service 
SELECT @is_ps = bool_is_ps FROM tbl_svisit WHERE int_id = 1145

    IF(@is_ps IS NOT NULL)
BEGIN
EXEC proc_set_visit_done @p_id_svisit;
END

END
   
    
    -- Update the table visit 
UPDATE tbl_visit
SET
    int_id_gk=@p_id_gk,
    int_id_svisit=@p_id_svisit,
    dtm=@p_dtm,
    int_id_obs=@p_id_obs,
    bool_isin=@p_isin,
    int_id_gate=@p_id_gate,
    bool_is_active=@p_is_active
WHERE int_id = @p_id and int_id_x = @p_id_x

    INSERT INTO tbl_log_visit
(int_id,int_id_gk, int_id_obs, dtm, bool_isin, int_id_gate, bool_is_active )
VALUES
    (@p_id, @p_id_gk, @p_id_obs, @p_dtm, @p_isin, @p_id_gate, @p_is_active)
END ;



-- 31 DEZ 2021


-- UPDATE FOR ENABLE AND DISABLING NOTIFICATION 
-- tbl_fdetail
CREATE TABLE teste_db.dbo.tbl_fdetail (
	int_id_user int NOT NULL,
	int_id_role int NOT NULL,
	int_id_company int NULL,
	int_id_department int NULL,
	dtm_registry datetimeoffset NOT NULL,
	str_tokken varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	bool_notification bit DEFAULT 1 NULL,
	CONSTRAINT tbl_udetail_PK PRIMARY KEY (int_id_user),
	CONSTRAINT tbl_fdetail_FK FOREIGN KEY (int_id_department) REFERENCES teste_db.dbo.tbl_extra(int_id),
	CONSTRAINT tbl_fdetail_FK_1 FOREIGN KEY (int_id_company) REFERENCES teste_db.dbo.tbl_company(int_id),
	CONSTRAINT tbl_fdetail_FK_2 FOREIGN KEY (int_id_role) REFERENCES teste_db.dbo.tbl_extra(int_id),
	CONSTRAINT tbl_udetail_FK FOREIGN KEY (int_id_user) REFERENCES teste_db.dbo.tbl_user(int_id) ON UPDATE CASCADE
) GO


-- view_user_funcionario
ALTER VIEW view_user_funcionario AS
SELECT 
cod,
fname,
lname,
uname,
nationality,
cod_type_id,
type_id,
nid,
exp_date_id,
email,
pnumber_1,
pnumber_2,
psw,
tf.int_id_company as cod_company,
tc.str_cname as company,
tf.int_id_department as cod_department,
dep.str_name as department,
tf.int_id_role as cod_role,
rol.str_name as user_role,
tf.dtm_registry,
tf.str_tokken as tokken,
isvisitor,
isactive,
tf.bool_notification as isnotification
FROM 
tbl_fdetail tf 

INNER JOIN view_user vu 
ON tf.int_id_user = vu.cod 

LEFT OUTER JOIN tbl_company tc
ON tf.int_id_company = tc.int_id 

LEFT OUTER JOIN tbl_extra dep
ON tf.int_id_department = dep.int_id 

LEFT OUTER JOIN tbl_extra rol
ON tf.int_id_role = rol.int_id  GO

-- proc_get_selected_by_email_or_pnumber_and_psw_users
ALTER PROCEDURE proc_get_selected_by_email_or_pnumber_and_psw_users(
	@p_isvisitor bit,
	@p_email_pnumber varchar(250),
	@p_psw varchar(250) = NULL
	)
AS
BEGIN
	IF (@p_isvisitor = 1)
		BEGIN
			SELECT
			cod, fname, lname, uname, nationality, cod_type_id, 
			type_id, nid, exp_date_id, email, pnumber_1, 
			pnumber_2, psw, isvisitor, isactive, cod_entity, 
			entity, cod_provenance, provenance, istemp, 
			dtm_registry, tokken, is_lite_induction, is_blocked,
			dtm_lite_induction, ia_deep_induction, dtm_deep_induction
			FROM view_user_visitor 
			WHERE 
			(email = @p_email_pnumber OR pnumber_1 = @p_email_pnumber OR uname = @p_email_pnumber) AND psw = @p_psw
		END
	ELSE
		BEGIN 
			SELECT 
			cod, fname, lname, uname, nationality, cod_type_id, 
			type_id, nid, exp_date_id, email, pnumber_1, 
			pnumber_2, psw, isvisitor, isactive, cod_company, 
			company, cod_department, department, cod_role, 
			user_role, dtm_registry, tokken, isnotification 
			FROM view_user_funcionario 
			WHERE 
			(email = @p_email_pnumber OR pnumber_1 = @p_email_pnumber OR uname = @p_email_pnumber) AND psw = @p_psw
		END 
END GO

-- view user details
ALTER VIEW view_user_detail AS 
        SELECT 
            tu.int_id as cod,
            tu.str_fname as fname,
            tu.str_lname as lname,
            tu.str_uname as uname,
            tu.str_nationality as nationality,
            tu.int_id_type_id as cod_type_id,
            te.str_name as type_id,
            tu.str_nid as nid,
            tu.dtm_expiration_date_id as exp_date_id,
            tu.str_email as email,
            tu.str_pnumber_1 as pnumber_1,
            tu.str_pnumber_2 as pnumber_2,
            tu.str_psw as psw,
            tu.bool_isvisitor as isvisitor,
            tu.bool_active as isactive,
            tv.int_id_entity as cod_entity,
            tc.str_cname as entity,
            NULL as cod_department,
            NULL as department,
            NULL as cod_role,
            NULL as user_role,
            tv.int_id_provenance as cod_provenance,
            prov.str_name as provenance,
            tv.bool_istemp as istemp,
            tv.dtm_registry,
            tv.str_tokken as tokken,
            tv.bool_lite_induction as is_lite_induction, 
            tv.dtm_lite_induction,
            tv.bool_deep_induction as ia_deep_induction,
            tv.dtm_deep_induction,
            tb.bool_isblocked as is_blocked,
            tb.int_id_who as int_id_who_blocked,
            tb.str_why as blocked_why,
            tb.dtm_when as blocked_when
        FROM tbl_vdetail tv 
        INNER JOIN tbl_user tu 
        ON tv.int_id_user = tu.int_id
        INNER JOIN tbl_extra te
        ON te.int_id = tu.int_id_type_id
        LEFT OUTER JOIN tbl_extra prov
        ON tv.int_id_provenance = prov.int_id 
        LEFT OUTER JOIN tbl_company tc
        ON tv.int_id_entity = tc.int_id 
        LEFT OUTER JOIN tbl_blocked tb 
        ON tv.int_id_user = tb.int_id_user

        UNION ALL 

        SELECT 
            tu.int_id as cod,
            tu.str_fname as fname,
            tu.str_lname as lname,
            tu.str_uname as uname,
            tu.str_nationality as nationality,
            tu.int_id_type_id as cod_type_id,
            te.str_name as type_id,
            tu.str_nid as nid,
            tu.dtm_expiration_date_id as exp_date_id,
            tu.str_email as email,
            tu.str_pnumber_1 as pnumber_1,
            tu.str_pnumber_2 as pnumber_2,
            tu.str_psw as psw,
            tu.bool_isvisitor as isvisitor,
            tu.bool_active as isactive,
            tf.int_id_company as cod_entity,
            tc.str_cname as entity,
            tf.int_id_department as cod_department,
            dep.str_name as department,
            tf.int_id_role as cod_role,
            rol.str_name as user_role,
            tf.bool_notification as isnotification,
            NULL,
            0 as istemp,
            tf.dtm_registry,
            tf.str_tokken as tokken,
            NULL, 
            NULL,
            NULL,
            NULL,
            tb.bool_isblocked as is_blocked,
            tb.int_id_who as int_id_who_blocked,
            tb.str_why as blocked_why,
            tb.dtm_when as blocked_when
        FROM tbl_fdetail tf
        INNER JOIN tbl_user tu
        ON tf.int_id_user = tu.int_id 
        INNER JOIN tbl_extra te
        ON te.int_id = tu.int_id_type_id
        LEFT OUTER JOIN tbl_company tc
        ON tf.int_id_company = tc.int_id 
        LEFT OUTER JOIN tbl_blocked tb 
        ON tf.int_id_user = tb.int_id_user 
        LEFT OUTER JOIN tbl_extra dep
        ON tf.int_id_department = dep.int_id 
        LEFT OUTER JOIN tbl_extra rol
        ON tf.int_id_role = rol.int_id GO

--- view user
ALTER VIEW view_user AS
SELECT 
tu.int_id as cod,
tu.str_fname as fname,
tu.str_lname as lname,
tu.str_uname as uname,
tu.str_nationality as nationality,
tu.int_id_type_id as cod_type_id,
te.str_name as type_id,
tu.str_nid as nid,
tu.dtm_expiration_date_id as exp_date_id,
tu.str_email as email,
tu.str_pnumber_1 as pnumber_1,
tu.str_pnumber_2 as pnumber_2,
tu.str_psw as psw,
tu.bool_isvisitor as isvisitor,
tu.bool_active as isactive,
tf.bool_notification as isnotification
FROM 
tbl_user tu
INNER JOIN tbl_extra te
ON te.int_id = tu.int_id_type_id
LEFT JOIN tbl_fdetail tf
ON tu.int_id = tf.int_id_user 

--- edit user details
ALTER PROCEDURE proc_edit_user_detail(
	@int_id_user int, 
	@p_isvisitor bit ,
	@p_id_role int = NULL,
	@p_id_company int = NULL,
	@p_id_department int = NULL,
	@p_registry datetimeoffset = NULL,
	@p_tokken varchar(50) = NULL,
	@p_id_entity int = NULL,
	@p_id_provenance int = NULL,
	@p_istemp bit = NULL,
	@p_isnotification bit = NULL
)
AS
BEGIN
	IF (@p_isvisitor = 0)
	BEGIN
		UPDATE tbl_fdetail
		SET int_id_role=@p_id_role,
		int_id_company=@p_id_company,
		int_id_department=@p_id_department,
		dtm_registry=@p_registry,
		str_tokken=@p_tokken,
		bool_notification=@p_isnotification
		WHERE int_id_user=@int_id_user
	END
	ELSE
	BEGIN
		UPDATE tbl_vdetail
		SET int_id_entity=@p_id_entity,
		int_id_provenance=@p_id_provenance,
		dtm_registry=@p_registry,
		str_tokken=@p_tokken,
		bool_istemp=@p_istemp
		WHERE int_id_user=@int_id_user
	END
END  GO


--- 09/01/2022
--- proc_add_svisit
ALTER PROCEDURE proc_add_svisit(
	@p_id_host int,
	@p_id_department int,
	@p_id_company int,
	@p_date_time datetimeoffset,
	@p_detail text,
	@p_submited datetimeoffset,
	@p_rule bit,
	@p_period int,
	@p_id_rpp int,
	@p_code varchar(6),
	@p_multi_entry bit,
	@p_access bit,
	@p_tocompany bit,
	@p_fast bit,
	@p_id_gate int,
	@p_internal bit,
	@p_id_subcompany int = NULL
)
AS
BEGIN
	INSERT INTO tbl_svisit 
	(int_id_host, 
	int_id_department, 
	int_id_company, 
	dtm_date_time, 
	str_detail, 
	dtm_submited, 
	bool_rule, 
	int_period, 
	int_id_rrp,
	str_code, 
	bool_multi_entry, 
	bool_access, 
	bool_tocompany, 
	bool_fast, 
	int_id_gate,
	bool_internal,
	int_id_sub_company)
	VALUES
	(@p_id_host,
	@p_id_department,
	@p_id_company,
	@p_date_time,
	@p_detail,
	@p_submited,
	@p_rule,
	@p_period,
	@p_id_rpp,
	@p_code,
	@p_multi_entry,
	@p_access,
	@p_tocompany,
	@p_fast,
	@p_id_gate,
	@p_internal,
	@p_id_subcompany)
END  GO


---proc_add_svisit_visitors

ALTER PROCEDURE proc_add_svisit_visitors
	@p_id_host int,
	@p_id_department int,
	@p_id_company int,
	@p_date_time datetimeoffset,
	@p_detail text,
	@p_submited datetimeoffset,
	@p_rule bit,
	@p_period int,
	@p_id_rpp int,
	@p_code varchar(6),
	@p_multi_entry bit,
	@p_access bit,
	@p_tocompany bit,
	@p_fast bit,
	@p_id_gate int,
	@p_internal bit,
	@p_id_subcompany int = NULL
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @cod_svisit int,
				@type_user int;
		BEGIN
			EXEC proc_add_svisit @p_id_host, @p_id_department, @p_id_company, @p_date_time, @p_detail, @p_submited, @p_rule, @p_period, @p_id_rpp, @p_code, @p_multi_entry, @p_access, @p_tocompany, @p_fast, @p_id_gate, @p_internal, @p_id_subcompany;
			SET @cod_svisit = ( SELECT IDENT_CURRENT('tbl_svisit'));
			EXEC proc_add_ivstate @cod_svisit;
			SET @type_user = ( SELECT int_id_role FROM tbl_fdetail tf WHERE tf.int_id_user = @p_id_host )
			IF (@type_user = 52) 
				BEGIN 
					EXEC proc_edit_vstate_state_htc @p_int=0, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
				END
			ELSE IF (@type_user = 53)
				BEGIN
					EXEC proc_edit_vstate_state_htc @p_int=0, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
					EXEC proc_edit_vstate_state_htc @p_int=1, @p_id_svisit=@cod_svisit, @p_id_htc=@p_id_host, @p_bool_htc=1, @p_dtm_htc=@p_date_time;
				END
			SELECT 1 AS status, @cod_svisit AS svisit, NULL AS error_num, NULL AS error_msg ;
			COMMIT TRANSACTION;
		END
	END TRY
	BEGIN CATCH  
 		-- RETURN ERROR
		SELECT 0 AS status, NULL AS svisit, ERROR_NUMBER() AS error_num, ERROR_MESSAGE() AS error_msg;
		--ROOLBACK
		ROLLBACK TRANSACTION;
	END CATCH  
END


 GO


--- proc_get_svisit_vstates

ALTER PROCEDURE proc_get_svisit_vstates (
	@p_dtm_start datetimeoffset,
	@p_dtm_end datetimeoffset,
	@p_id_host int = NULL,
	@p_id_comp int = NULL,
	@p_id_entity int = NULL,
	@p_id_x int = NULL,
	@p_id int = NULL)
AS
BEGIN
	IF (@p_id_host IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, sv_cod_rrp, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp, sv_sub_company, 
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE st_host_cod = @p_id_host AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id_comp IS NOT NULL )
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail, sv_cod_host,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp, sv_sub_company,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE sv_cod_comp = @p_id_comp AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end)) AND sv_is_ps = 0
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id_x IS NOT NULL ) 
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, sv_cod_rrp,
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp, sv_sub_company,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		WHERE ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end)) AND vsv.cod IN (SELECT vsv.cod 
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		WHERE tvi.int_id_x = @p_id_x)
		--AND sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
	ELSE IF (@p_id IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp, sv_sub_company,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		WHERE vsv.cod = @p_id AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
	END
	ELSE IF (@p_id_entity IS NOT NULL)
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_sub_company, sv_sub_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, 
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, sv_is_internal,
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp, sv_sub_company,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod 
		WHERE vuv.cod_entity = @p_id_entity AND ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
	END
	ELSE 
	BEGIN 
		SELECT 
			vsv.cod, sv_cod_comp, sv_company, sv_cod_dep, sv_department, sv_visit_date, sv_detail,
			sv_submited_date, sv_is_rule, sv_period, sv_code, sv_is_multi_entry, sv_is_access, 
			sv_is_company, sv_is_fast, sv_cod_gate, sv_gate, 
			st_host_cod, st_host_fname, st_host_lname, st_host_cod_comp, st_host_comp, 
			st_host_cod_dep, st_host_dep, st_host_is, st_host_is_cancel, st_host_dtm, 
			st_threa_cod, st_threa_fname, st_threa_lname, st_threa_cod_comp, st_threa_comp, sv_is_internal,
			st_threa_cod_dep, st_threa_dep, st_threa_is, st_threa_is_cancel, st_threa_dtm, 
			st_core_id, st_core_fname, st_core_lname, st_core_cop_comp, st_core_comp, 
			st_core_cod_dep, st_core_dep, st_core_is, st_core_is_cancel, st_core_dtm, 
			st_why, st_why_cencel, st_cod_why, st_selected_why, st_dtm_when_cancel, sv_cod_rrp, sv_rrp, sv_sub_company, tv.bool_isin  as x_is_in,
			vuv.cod as x_cod, vuv.fname as x_fname, vuv.lname as x_lname, vuv.entity as entity,
			vuv.nationality, vuv.type_id, vuv.nid, vuv.exp_date_id, vuv.email, vuv.pnumber_1, 
			vuv.pnumber_2, vuv.provenance, vuv.is_lite_induction, vuv.dtm_lite_induction, tvi.bool_induction as is_induction,
			vuv.ia_deep_induction, vuv.dtm_deep_induction, vsv.sv_is_done, sv_is_ps
		FROM view_svisit_vstates vsv
		INNER JOIN tbl_visitor tvi
		ON vsv.cod = tvi.int_id_svisit AND tvi.bool_is_active = 1
		INNER JOIN view_user_visitor vuv
		ON tvi.int_id_x = vuv.cod
		INNER JOIN tbl_visit tv
		ON vsv.cod = tv.int_id_svisit AND tv.int_id_x = vuv.cod
		WHERE ((vsv.sv_is_done = 0 AND vsv.sv_visit_date <= @p_dtm_end) OR (vsv.sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end))
		--sv_visit_date BETWEEN @p_dtm_start AND @p_dtm_end
	END
END GO
