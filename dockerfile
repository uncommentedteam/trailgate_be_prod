FROM node:14.11.0-alpine as build
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN mkdir userDocumentPhotos
CMD [ "node", "server.js" ]