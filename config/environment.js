const dotenv = require('dotenv');
dotenv.config();
module.exports = {
    port: process.env.SERVER_PORT,
    fe_server_address: process.env.FE_SERVER_ADDRESS,
    db_port: process.env.DATABASE_PORT,
    db_user: process.env.DATABASE_USER,
    db_password: process.env.DATABASE_PASSWORD,
    db_name: process.env.DATABASE_NAME,
    db_server: process.env.DATABASE_SERVER,
    twilio_accountSID: process.env.TWILIO_ACCOUNT_SID,
    twilio_auth_token: process.env.TWILIO_AUTH_TOKEN,
    twilio_sender_number: process.env.TWILIO_SENDER_NUMBER,
    twilio_service_sid: process.env.TWILIO_SERVICE_SID,
    nodemailer_service: process.env.EMAIL_SERVICE,
    nodemailer_user: process.env.EMAIL_USER,
    nodemailer_password: process.env.EMAIL_PASSWORD,
    email_env: process.env.EMAIL_ENV,
    support_email: process.env.SUPPORTEMAIL


} 